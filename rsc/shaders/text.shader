// For text rendering

###############|Vertex Shader|################
#version 330 core

// Attributes to Vertex Shader come from attributes in VAO
// Uniforms allow us the flexibility of passing other types of data to our shaders
layout (location = 0) in vec4 vertex;

// Per vertex outputs, sent to fragment shader
out vec2 passTexCoords;

uniform mat4 projection;

void main()
{
    
    // Output position of the vertex, in clip space
    // map [0..1000][0..800] to [-1..1][-1..1]
    // [0..1000][0..800] -> [-500..500][-400..400]
    //vec2 my_vertex_position = position - vec2( 500, 400);
    //my_vertex_position /= vec2( 500.0f, 400.0f );

    //gl_Position =  vec4( my_vertex_position, 0, 1 );
    gl_Position = projection * vec4(vertex.xy, 0.0, 1.0);

    // UV of the vertex. No special space for this one.
    passTexCoords = vertex.zw;
}


###############|Fragment Shader|################
#version 330 core

in vec2 passTexCoords;

out vec4 FragColor;

uniform sampler2D textureSampler;
uniform vec3 textColor;

void main()
{
    // Expecting characters in atlas to have some r value, and background to not
    vec4 sampled = vec4(1.0, 1.0, 1.0, texture(textureSampler, passTexCoords).r);
    FragColor = vec4(textColor, 1.0) * sampled;
}


