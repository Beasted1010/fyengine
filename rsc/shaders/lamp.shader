// For objects that serve as light sources

###############|Vertex Shader|################
#version 330 core

// Attributes to Vertex Shader come from attributes in VAO
// Uniforms allow us the flexibility of passing other types of data to our shaders
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textureCoords;

// Per vertex outputs, sent to fragment shader
out vec2 passTexCoords;

uniform mat4 transformationMatrix;
uniform mat4 modelToWorld; // Model matrix
uniform mat4 worldToCamera; // View matrix
uniform mat4 cameraToClip; // Projection matrix

void main()
{
    // Matrices are expected to be column-major order so that I can follow math notation
    gl_Position = cameraToClip * worldToCamera * modelToWorld * vec4(position, 1.0f);
    //gl_Position = vec4(position, 1.0f) * modelToWorld * worldToCamera * cameraToClip;
    //gl_Position = transformationMatrix * vec4(position, 1.0f);
    passTexCoords = textureCoords;
}


###############|Fragment Shader|################
#version 330 core

out vec4 FragColor;

uniform sampler2D textureSampler;

void main()
{
    // Define color of lamp's light (white in this case)
    FragColor = vec4(1.0f);
}



