// For non-light source objects

###############|Vertex Shader|################
#version 330 core

// Attributes to Vertex Shader come from attributes in VAO
// Uniforms allow us the flexibility of passing other types of data to our shaders
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 textureCoords;

// Per vertex outputs, sent to fragment shader
out vec2 passTexCoords;

uniform mat4 transformationMatrix;
uniform mat4 modelToWorld; // Model matrix
uniform mat4 worldToCamera; // View matrix
uniform mat4 cameraToClip; // Projection matrix

void main()
{
    // Matrices are expected to be column-major order so that I can follow math notation
    gl_Position = cameraToClip * worldToCamera * modelToWorld * vec4(position, 1.0f);
    //gl_Position = vec4(position.xy, 0, 1.0f);
    //gl_Position = vec4(position, 1.0f) * modelToWorld * worldToCamera * cameraToClip;
    //gl_Position = transformationMatrix * vec4(position, 1.0f);
    passTexCoords = textureCoords;
}


###############|Fragment Shader|################
#version 330 core

// Per vertex outputs, sent from vertex shader
// Fragment shader interpolates from these values to determine output value of each pixel
in vec2 passTexCoords;

out vec4 FragColor;

uniform sampler2D textureSampler1;

void main()
{
    // vec4 sampled = vec4(1.0, 1.0, 1.0, texture(textureSampler, passTexCoords).r);
    // FragColor = vec4(1.0, 1.0, 1.0, 1.0) * sampled;
    // FragColor = texture(textureSampler1, passTexCoords).r;

    vec4 sampled = vec4(1.0,1.0,1.0,texture(textureSampler1, passTexCoords).r);
    FragColor = vec4(1.0, 1.0f, 1.0f, 1.0f) * sampled;
}

