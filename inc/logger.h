


#ifndef LOGGER_H
#define LOGGER_H


/* TODO: Do I want? Sounds very bulky to use compile what is in the windows header, especially when nothing else is needed from it
#ifdef _WIN32
    #include <windows.h>
#else
    #include <unistd.h>
#endif*/
#include <unistd.h> // Has sleep?
#include <time.h> // Has some useful time stuff
#include <stdio.h>

#define SEPARATE printf("------------------------------------------\n") // TODO: Console width? Parameter?

#ifdef NO_LOGS
    #define TIME_STAMP 
    #define LOG_CRITICAL( ... ) 
    #define LOG_ERROR( ... ) 
    #define LOG_WARNING( ... ) 
    #define LOG_DEBUG( ... ) 
    #define LOG_INFO( ... ) 
    #define LOG_TRACE( ... ) 
    #define LOG_CHECKPOINT( ... ) 
    
    #define LOG_DATA( ... ) 
    #define LOG_STEP( step, ... ) 
#else
    #define TIME_STAMP // TODO
    #define LOG_CRITICAL( ... ) TIME_STAMP; printf("CRITICAL: "); printf( __VA_ARGS__ ); printf("\n");
    #define LOG_ERROR( ... ) TIME_STAMP; printf("ERROR: "); printf( __VA_ARGS__ ); printf("\n");
    #define LOG_WARNING( ... ) TIME_STAMP; printf("WARNING: "); printf( __VA_ARGS__ ); printf("\n");
    #define LOG_DEBUG( ... ) TIME_STAMP; printf("DEBUG: "); printf( __VA_ARGS__ ); printf("\n");
    #define LOG_INFO( ... ) TIME_STAMP; printf("INFO: "); printf( __VA_ARGS__ ); printf("\n");
    #define LOG_TRACE( ... ) TIME_STAMP; printf("TRACE "); printf( __VA_ARGS__ ); printf("\n"); // TODO: Info of where at and vars?
    #define LOG_CHECKPOINT( ... ) TIME_STAMP; printf("CHECKPOINT: "); printf( __VA_ARGS__ ); printf("\n"); // TODO: Impt thing completed? Print results?
    
    #define LOG_DATA( ... ) TIME_STAMP; printf("DATA: "); printf( __VA_ARGS__ ); printf("\n"); // TODO: Variable?
    #define LOG_STEP( step, ... ) TIME_STAMP; printf("STEP #%i: ",step); printf( __VA_ARGS__ ); printf("\n"); // TODO: Number?
#endif


#define PAUSE // TODO: scanf?
#define SLEEP( seconds ) sleep( seconds )

#define TIME_START
#define TIME_END

#define MEMORY_DUMP

enum LoggerLevels
{
    // Bit fields (GCC needed) to allow for flexible options for logging levels
    // E.g. INFO | ERROR -> Sets these two logging levels and a bit field of 0101
    LOGGER_LEVEL_INFO = 0b0001,
    LOGGER_LEVEL_WARNING = 0b0010,
    LOGGER_LEVEL_ERROR = 0b0100,
    LOGGER_LEVEL_CRITICAL = 0b1000,

    // Presets TODO: Good idea?
    LOGGER_LEVEL_DEFCON_1 = 0b1111, // todo
    LOGGER_LEVEL_DEFCON_2 = 0b0000,
    LOGGER_LEVEL_DEFCON_3 = 0b0000,
    LOGGER_LEVEL_DEFCON_5 = 0b0001
};

enum LoggerTypes
{
    LOGGER_TYPE_DATA,
    LOGGER_TYPE_STEP,
    LOGGER_TYPE_TRACE,
    LOGGER_TYPE_CHECKPOINT
};

struct Timer
{

};

struct Memory
{

};

struct Profiler
{
    struct Timer timer;
    struct Memory memory;
};

struct Debugger
{
    struct Profiler profiler;
};

struct Logger // TODO: Design
{
    char** criticals;
    char** errors;
    char** warnings;

    int numCriticals;
    int numErrors;
    int numWarnings;

    struct Debugger debugger;

    unsigned char storeLogsFlag: 1; // Flag, use 1 bit only TODO: Option of which logs to store? Bitfield?
    unsigned char useDebuggerFlag: 1; // Flag
};


void CreateLogger( struct Logger* loggerOut, unsigned char useLogsFlag, unsigned char useDebuggerFlag );
void FlushLogs( struct Logger* logger );
void DestroyLogger( struct Logger* logger );

// Print all errors currently in the logger
void PrintLogs( struct Logger* logger );

void StartTimer( struct Timer* timer );
void StopTimer( struct Timer* timer ); // TODO: And print results?
void StartProfiler( struct Profiler* profiler );
void StopProfiler( struct Profiler* profiler );


#endif // LOGGER_H




