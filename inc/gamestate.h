
#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "matrix.h"
#include "loader.h"

struct GameState
{
    struct Object* gameObjects;
    int numGameObjects;

    struct Object* lightObjects;
    int numLightObjects;

    struct Object* npcObjects;
    int numNpcObjects;
};

struct Object
{
    struct TexturedModel texturedModel;
    struct Vector3f position;
};

void InitializeGameState( struct GameState* state );
void FreeGameState( struct GameState* state );

struct Object CreateObject( struct Loader* loader, float* vertices, int numVertices, unsigned int* indices, int numIndices,
                            float* textureCoords, int numTextureCoords, const char* textureLocation, struct Vector3f position );

struct Object CreateObjectFromRawModel( struct Loader* loader, struct RawModel* rawModel,
                                        const char* textureLocation, struct Vector3f position );

void AddObject( struct Object object, struct Object** objects, int* numObjects );

#endif // GAMESTATE_H
