
#ifndef WINDOW_H
#define WINDOW_H

// Screen dimension settings
#define SCREEN_WIDTH 1000
#define SCREEN_HEIGHT 800


#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>

#include "camera.h"
#include "events.h"

// TODO: How do I want to handle this? glew.h has "GLFWAPI" but this assumes that I conditionally #include it, or else always defnd
#define GLFW
#define GLEW

struct WindowState
{
    GLFWwindow* window;

    short int running;

    float deltaTime; // Time between current frame and last
    float lastFrame; // Time of last frame

    struct Input input;
};

struct WindowState CreateWindowState();
void UpdateWindowState( struct WindowState* state );
void DestroyWindowState( struct WindowState* state );

GLFWwindow* GLFW_OpenGLWindowAndContextCreation();
void GLEW_OpenGLLoadFunctions();

void ProcessInput( struct WindowState* windowState, struct Overlay* overlays, unsigned int overlayCount, struct Camera* camera );


#endif // WINDOW_H
