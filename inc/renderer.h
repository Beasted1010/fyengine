
#ifndef TEXTURES_H
#define TEXTURES_H

#include "model.h"


void RenderPrepare();
void Render( unsigned int programID, struct TexturedModel* texturedModel );


#endif // TEXTURES_H
