
#ifndef FONT_H
#define FONT_H

#include "loader.h"

#define DEBUG_FONT

struct Character
{
    // NOTE: Height and advance are currently not used. I am using cell height instead. And the width for advance.
    unsigned int textureID;
    unsigned int width, height; // In pixels
    unsigned int bearingX, bearingY; // In pixels (using as offset)
    // unsigned int advance; // In 1/64th pixels -> CBFG doesn't seem to use this, and it is basically just "base width" anyways
};

struct Font2D
{
    struct Loader loader;
    
    unsigned int VAO, VBO;

    struct Character* characters;
    unsigned int numCharacters;

    unsigned int imageWidth;
    unsigned int imageHeight;

    // Cells are the sections of the font image that contains a glyph (character)
    unsigned int cellWidth;
    unsigned int cellHeight;

    unsigned int startChar;

    char* fontName;

    unsigned int fontHeight;
    unsigned int fontWidth; // 0 is default

    unsigned int globalWidthOffset;
    unsigned int globalXOffset;
    unsigned int globalYOffset;

    unsigned int bold;
    unsigned int italic;
    unsigned int antiAlias;
};

static inline int GetTextQuadWidth( struct Font2D* font, const char* text, float scale )
{
    int text_quad_width = 0;
    for( unsigned int i = 0; text[i] != '\0'; i++ )
    {
        text_quad_width += font->characters[text[i] - font->startChar].width * scale;
    }

    return text_quad_width;
}

void CreateHUDFont2D( const char* fontTextureLocation, const char* fontDataLocation, struct Font2D* fontOut );
void CreateWorldFont2D( const char* fontTextureLocation, const char* fontDataLocation, struct Font2D* fontOut );
void DestroyFont2D( struct Font2D* font );

void PrintHUDText2D( const char* text, int x, int y, float scale, struct RGB color,
                     struct Font2D* font, struct ShaderProgram* shaderProgram );
void PrintText2DInWorld( const char* text, float scale, struct RGB color, struct Font2D* font, 
                         struct ShaderProgram* shaderProgram );


#endif // FONT_H

