
#ifndef CAMERA_H
#define CAMERA_H

#include "matrix.h"


#define MAX_YAW
#define MIN_YAW

#define MAX_ROLL
#define MIN_ROLL

#define MAX_PITCH 89.0f
#define MIN_PITCH -89.0f


struct Camera
{
    struct Vector3f position;
    struct Vector3f target;
    struct Vector3f up;

    struct Vector3f front;
    struct Vector3f right;
    struct Vector3f worldUp;

    // Euler Angles
    float yaw;
    float roll;
    float pitch;

    float zoom;

    unsigned int enabled;
};


void CreateCamera( struct Camera* camera, struct Vector3f position, struct Vector3f target );
void PlaceCamera( struct Camera* camera, struct Vector3f position, struct Vector3f target );
void LockCamera( struct Camera* camera, struct Vector3f position, struct Vector3f target );
void OrientateCamera( struct Camera* camera, float yaw, float roll, float pitch );
void UpdateCamera( struct Vector3f position, struct Vector3f target );
void DisableCamera( struct Camera* camera );
void DestroyCamera( struct Camera* camera );



#endif // CAMERA_H
