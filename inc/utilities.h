
#ifndef UTILITIES_H
#define UTILITIES_H

#include <stdio.h>

int PowerInt( int base, int exponent );

int StringCompare( const char* str1, const char* str2 );
int StringContains( const char* str, const char* toCompare );
void StringReplaceN( const char* source, char* destination, int characterCountToReplace );
size_t StringLength( const char* str );

int IsDigit( const char c );
int CharToInt( const char c );
int StringToInt( const char* str, int strLen );
float StringToFloat( const char* str, int strLen );

int SplitOn( const char* str, int strLen, const char splitOn, char*** out );

int ReadLine( FILE* fp, char splitOn, char*** out );


#endif // UTILITIES_H

