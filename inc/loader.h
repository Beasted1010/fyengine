
#ifndef VERTICES_H
#define VERTICES_H

#include "model.h"

struct Loader
{
    unsigned int* vaos;
    unsigned int numVAOs;

    unsigned int* vbos;
    unsigned int numVBOs;

    unsigned int* ebos;
    unsigned int numEBOs;

    unsigned int* textures;
    unsigned int numTextures;
};

struct Loader CreateLoader();
void AddVAOToLoader( struct Loader* loader, unsigned int vaoID );
void AddVBOToLoader( struct Loader* loader, unsigned int vboID );
void AddEBOToLoader( struct Loader* loader, unsigned int eboID );
void AddTextureToLoader( struct Loader* loader, unsigned int textureID );
void DestroyLoader( struct Loader* loader );

unsigned int CreateVAO( struct Loader* loader );
struct RawModel LoadToVAO( struct Loader* loader, float* vertices, unsigned int numVertices, 
                                                  unsigned int* indices, unsigned int numIndices,
                                                  float* textureCoords, unsigned int numTexCoords );

//unsigned int CreateVAO();
//static void StoreDataInAttributeList( unsigned int attributeNumber, float* data, unsigned int numElements );
//void UnbindVAO();

//unsigned char* LoadTextureData( const char* fileLocation, int* width, int* height );
unsigned int LoadTexture( struct Loader* loader, const char* fileLocation );

struct RawModel LoadOBJModel( struct Loader* loader, const char* fileLocation );






#endif // VERTICES_H
