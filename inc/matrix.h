
#ifndef MATRIX_H
#define MATRIX_H

// Row-major order


// TODO: Is returning a structure of a multidimensional float array inefficient? 
//          Are the individual elements copied? Or just the address

#define PI 3.14159
#define RADIANS( degrees ) degrees * ( PI / 180.0f )

enum VectorComponents
{
    M_X,
    M_Y,
    M_Z,
    M_W
};

enum MatrixColumns
{
    M_0,
    M_1,
    M_2,
    M_3
};

struct __attribute__((packed)) Vector2f
{
    float components[2];
};

struct __attribute__((packed)) Vector3f
{
    float components[3];
};

struct __attribute__((packed)) Vector4f
{
    float components[4];
};

struct __attribute__((packed)) Matrix4f
{
    float elements[4][4];
};

void PrintVector3f( struct Vector3f* vector );
void PrintMatrix4f( struct Matrix4f* matrix );

struct Matrix4f ComposeVector3sInMatrix4f( struct Vector3f* col1, struct Vector3f* col2, 
                                           struct Vector3f* col3, struct Vector3f* col4 );

void TransposeMatrix4f( struct Matrix4f* matrix );
void RowMajorToColumnMajorMatrix4f( struct Matrix4f* matrix );

float LengthOfVector3f( const struct Vector3f* vector );
float DotVector3f( struct Vector3f* current, struct Vector3f* second );
struct Vector3f CrossVector3f( struct Vector3f* current, struct Vector3f* second );
struct Vector3f NormalizeVector3f( const struct Vector3f* vector );

struct Vector3f SubtractVector3f( const struct Vector3f* vector1, const struct Vector3f* vector2 );
struct Vector3f ScalarMultiplyVector3f( struct Vector3f* vector, float scalar );
struct Vector3f AddVector3f( struct Vector3f* vector1, struct Vector3f* vector2 );

void SetIdentityMatrix4f( struct Matrix4f* matrix );
void SetZeroMatrix4f( struct Matrix4f* matrix );
void ScalarMultiplyMatrix4f( struct Matrix4f* matrix, float scalar );
void ScalarTransformMatrix4f( struct Matrix4f* matrix, float scalar );
struct Matrix4f MultiplyMatrix4fMatrix4f( const struct Matrix4f* matrix1, const struct Matrix4f* matrix2 );

struct Matrix4f CreateTransformationMatrix4f( struct Vector3f* translation, 
                                              float rx_deg, float ry_deg, float rz_deg, 
                                              float scale );

struct Matrix4f CreateViewMatrix4f( struct Vector3f* eyePosition, struct Vector3f* target, struct Vector3f* up );

struct Matrix4f CreatePerspectiveMatrix4f( float fieldOfView, float aspectRatio, float nearPlaneDist, float farPlaneDist );
struct Matrix4f CreateOrthographicMatrix4f( float left, float right, float bottom, float top, float near, float far );


#endif // MATRIX_H
