
#ifndef MODEL_H
#define MODEL_H

struct RawModel
{
    unsigned int vaoID;
    unsigned int vertexCount;
};


// A complete model is made up of textured coordinates (mapping to a spot on the texture s,t,r or u,v, ...) 
//      and vertex coordinates (location on screen x,y,z,w). 
struct TexturedModel
{
    unsigned int textureID;
    struct RawModel rawModel;
};


static inline struct TexturedModel CreateTexturedModel( struct RawModel model, unsigned int textureID )
{
    return (struct TexturedModel) { textureID, model };
}


#endif // MODEL_H

