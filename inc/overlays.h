
#ifndef OVERLAYS_H
#define OVERLAYS_H

#include "common.h"
#include "shaders.h"
#include "loader.h"
#include "font.h"




enum WidgetType
{
    wtNONE,
    wtPUSH_BUTTON,
    wtCHECKBOX,
    wtSLIDER,
    wtTEXTBOX,
    WIDGET_COUNT
};

enum ScreenComponentState
{
    scsNONE,
    scsHOVERED,
    scsPRESSED,
    scsSELECTED,
    scsDEACTIVATED,
    WIDGET_STATE_COUNT
};

enum ScreenComponentTitleBoundary
{
    sctbINSIDE_BOUNDARY,
    sctbOUTSIDE_BOUNDARY
};

enum ScreenComponentTitleEdge
{
    scteCENTER,
    scteLEFT_EDGE,
    scteRIGHT_EDGE,
    scteTOP_EDGE,
    scteBOTTOM_EDGE
};

struct ScreenData
{
    int startX, startY;
    int width, height;

    // TODO: How to determine if item is selected? Have a "selectColor" or some way to invert?
    //       Perhaps a "SelectWidget" function that will take care of this color toggle and state update?
    struct RGB color;

    ScreenComponentState state;
    float argValue;

    struct Font2D font;
    const char* tagName;
    struct RGB textColor;

    struct Loader loader;
    unsigned int vaoID, vboID;
    unsigned int textureID; // TODO
};

struct PushButton
{
    struct ScreenData screenData;
};

struct Checkbox
{
    struct ScreenData screenData;
    int checked;
};

struct Slider
{
    struct ScreenData railData;
    struct ScreenData sliderData;
};

struct Textbox
{
    struct ScreenData screenData;

    struct Font2D font;
    const char* text;

    struct RGB textColor;
};

struct Widget
{
    int type;

    union
    {
        struct PushButton pushButton;
        struct Checkbox checkbox;
        struct Slider slider;
        struct Textbox textbox;
    };
};

struct Overlay
{
    struct ScreenData screenData;

    struct Widget* widgets;
    int widgetCount;

    int pushButtonCount;
    int checkboxCount;
    int sliderCount;
    int textboxCount;
};

static inline struct ScreenData GetScreenDataFromWidget( struct Widget* widget )
{
    switch( widget->type )
    {
        case wtPUSH_BUTTON:
        {
            return widget->pushButton.screenData;
        } break;

        case wtCHECKBOX:
        {
            return widget->checkbox.screenData;
        } break;

        case wtSLIDER:
        {
            // TODO: Handle differently because slider has sliderData and railData
            //return widget->slider.screenData;
        } break;

        case wtTEXTBOX:
        {
            return widget->textbox.screenData;
        } break;
    }

    LOG_ERROR("Widget has an unkown type of '%i'! Returning {}", widget->type);
    return {};
}


struct Overlay CreateOverlay( int x, int y, int width, int height, const char* tagName, struct RGB color );
void AddWidgetToOverlay( void* widget, int type, struct Overlay* overlay );
void DrawOverlay( struct Overlay* overlay, struct ShaderProgram* overlayShaderProgram,
                  struct ShaderProgram* widgetShaderProgram, struct ShaderProgram* textShaderProgram );
void DrawOverlays( struct Overlay* overlays, int overlay_count, struct ShaderProgram* overlayShaderProgram,
                   struct ShaderProgram* widgetShaderProgram, struct ShaderProgram* textShaderProgram );
void PlaceWidgetTitle( struct Widget* widget, enum ScreenComponentTitleEdge edge, enum ScreenComponentTitleBoundary boundary,
                       struct ShaderProgram* textShaderProgram );
void SetOverlayState( struct Overlay* overlay, ScreenComponentState state );
void SetWidgetState( struct Widget* widget, ScreenComponentState state );
void SetWidgetColor( struct Widget* widget, struct RGB color );
void DestroyOverlays( struct Overlay* overlays, int overlayCount );
void DestroyOverlay( struct Overlay* overlay );

void CreateAndAddPushButtonToOverlay( int relativeX, int relativeY, int width, int height, struct RGB color, 
                                      const char* tagName, struct Overlay* overlay );
void CreateAndAddCheckboxToOverlay( int relativeX, int relativeY, int width, int height, struct RGB color, 
                                    const char* tagName, struct Overlay* overlay );
void CreateAndAddSliderToOverlay( int relativeRailX, int relativeRailY, int railWidth, int railHeight, struct RGB railColor,
                                  int sliderWidth, int sliderHeight, struct RGB sliderColor, const char* tagName,
                                  int minValue, int maxValue, int defaultValue, struct Overlay* overlay );
void CreateAndAddTextboxToOverlay( int relativeX, int relativeY, int width, int height, const char* text, struct RGB color,
                                   const char* tagName, struct Overlay* overlay );

struct PushButton CreatePushButton( int x, int y, int width, int height, const char* tagName, struct RGB color );
struct Checkbox CreateCheckbox( int x, int y, int width, int height, const char* tagName, struct RGB color );
struct Slider CreateSlider( int railX, int railY, int railWidth, int railHeight, struct RGB railColor,
                            int sliderWidth, int sliderHeight, struct RGB sliderColor, const char* tagName,
                            int minValue, int maxValue, int defaultValue );
struct Textbox CreateTextbox( int x, int y, int width, int height, const char* text, const char* tagName, struct RGB color );




#endif // OVERLAYS_H

