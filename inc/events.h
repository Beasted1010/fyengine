
#ifndef EVENTS_H
#define EVENTS_H

#include "overlays.h"


#define MOUSE_SENSITIVITY 0.05f

typedef void (*OVERLAY_CALLBACK)( void* );
typedef void (*WIDGET_CALLBACK)( void* );


enum EventType
{
    etNONE = 0,

    etMOUSE_MOVE_EVENT = (1 << 0 ),
    etMOUSE_CLICK_EVENT = (1 << 1),
    etKEY_PRESS_EVENT = (1 << 2),

    EVENT_TYPE_COUNT = 3
};

enum KeyInputType
{
    kitNONE,

    kitKEY_ESCAPE, kitKEY_CONTROL, 
    kitKEY_LEFT_ALT, kitKEY_RIGHT_ALT,
    kitKEY_LEFT_SHIFT, kitKEY_RIGHT_SHIFT,

    kitKEY_0, kitKEY_1, kitKEY_2, kitKEY_3
};

enum PressInputType
{
    pitNONE = 0,
    pitPRESS,
    pitRELEASE
};

enum MouseButtonInput
{
    mbiNONE = 0,
    mbiMOUSE_BUTTON_LEFT,
    mbiMOUSE_BUTTON_RIGHT
};

struct KeyPressInput
{
    KeyInputType key;
    unsigned int repeat;
};

struct MouseInput
{
    float sensitivity;

    float xPos, yPos;
    float lastXPos, lastYPos;

    MouseButtonInput button;
};

struct Input
{
    struct MouseInput mouse;
    struct KeyPressInput keyboard;
    PressInputType type;
};

struct CallbackFunctions
{
    OVERLAY_CALLBACK overlayCallbacks[EVENT_TYPE_COUNT];
};



static inline void RegisterOverlayCallback( EventType type, OVERLAY_CALLBACK callback, struct CallbackFunctions* callbackFunctions )
{
    callbackFunctions->overlayCallbacks[type] = callback;
}

static inline int MouseInOverlay( int x, int y, struct Overlay* overlay )
{
    return ( (x >= overlay->screenData.startX && x <= overlay->screenData.startX + overlay->screenData.width) &&
             (y >= overlay->screenData.startY && y <= overlay->screenData.startY + overlay->screenData.height) );
}

static inline int MouseInWidget( struct MouseInput* mouseInput, struct Widget* widget )
{
    struct ScreenData widget_data = GetScreenDataFromWidget( widget );

    return ( (mouseInput->xPos >= widget_data.startX && mouseInput->xPos <= widget_data.startX + widget_data.width) &&
             (mouseInput->yPos >= widget_data.startY && mouseInput->yPos <= widget_data.startY + widget_data.height) );
}



void RegisterOverlayCallback( EventType type, OVERLAY_CALLBACK callback, struct CallbackFunctions* registeredCallbacks );
void HandleOverlayEvent( struct Overlay* overlay, EventType type, struct Input* input );

void OnMouseMove( void* data );
void OnMouseClick( void* data );







#endif // EVENTS_H

