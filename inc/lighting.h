
#ifndef LIGHTING_H
#define LIGHTING_H

#include "matrix.h"
#include "model.h"

struct LightSource
{
    struct Vector3f position;
    struct Vector3f color;
    struct TexturedModel texturedModel;
};

void CreateLightSource( struct Vector3f position, struct Vector3f color, struct LightSource* outLightSource );





#endif // LIGHTING_H
