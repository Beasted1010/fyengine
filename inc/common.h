
#ifndef COMMON_H
#define COMMON_H

#include "logger.h"

#define BIT(x) 1 << x

struct RGB
{
    unsigned char red;
    unsigned char green;
    unsigned char blue;
};


void ValidateObject( void* object, const char* name );


#endif // COMMON_H


