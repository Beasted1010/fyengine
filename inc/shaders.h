
#ifndef SHADERS_H
#define SHADERS_H

#define SHADER_FILE "rsc/shaders/shaders.shader"
#define VERTEX_FILE
#define FRAGMENT_FILE

//#define DEBUG_SHADERS

enum ShaderCategories
{
    SHADER_CATEGORY_NONE = -1,
    SHADER_CATEGORY_COLORS,
    SHADER_CATEGORY_LAMP,
    SHADER_CATEGORY_TEXT,
    NUMBER_OF_SHADER_CATEGORIES
};

enum ShaderType
{
    SHADER_TYPE_NONE = -1,
    SHADER_TYPE_VERTEX = 0,
    SHADER_TYPE_FRAGMENT = 1
};

struct ShaderProgramSource
{
    char* vertexShaderSource;
    char* fragmentShaderSource;
};

struct ShaderProgram
{
    unsigned int id;
    unsigned int vertexShaderID, fragmentShaderID;

    // Locations are supposed to be ints (glGetUniformLocation returns an int, -1 if not found)
    int modelToWorldMatrixLocation;
    int worldToCameraMatrixLocation;
    int cameraToClipMatrixLocation;

    int transformationMatrixLocation;
};


struct ShaderProgram CreateShaderProgram( const char* shaderFileLocation );
void DestroyShaderProgram( struct ShaderProgram* shaderProgram );

void UseProgram( unsigned int shaderProgram );
void StopProgram();

void FreeShaderProgramSource( struct ShaderProgramSource* shaderProgramSource );

void LoadColor( unsigned int* shaderID, const char* uniformName, struct RGB* color );

void LoadModelToWorldMatrix( int location, struct Matrix4f* matrix );
void LoadWorldToCameraMatrix( int location, struct Matrix4f* matrix );
void LoadCameraToClipMatrix( int location, struct Matrix4f* matrix );

void LoadMatrices( struct ShaderProgram* shaderProgram, struct Matrix4f* modelToWorld,  
                   struct Matrix4f* worldToCamera, struct Matrix4f* cameraToClip );

void LoadScreenProjectionMatrix( unsigned int shaderProgramID, float screenWidth, float screenHeight );

void LoadTransformationMatrix( int location, struct Matrix4f* matrix );

#endif // SHADERS_H
