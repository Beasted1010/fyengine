
#ifndef ENGINE_H
#define ENGINE_H

#include "common.h"
#include "logger.h"
#include "utilities.h"

#include "window.h"

#include "loader.h"
#include "font.h"

#include "model.h"
#include "lighting.h"
#include "camera.h"
#include "shaders.h"
#include "renderer.h"

#include "matrix.h"

// Relative shader locations as found in the directory tree
#define COLORS_SHADER_LOCATION "rsc/shaders/color.shader"
#define LAMP_SHADER_LOCATION "rsc/shaders/lamp.shader"
#define TEXT_SHADER_LOCATION "rsc/shaders/text.shader"

#define FONT_TEXTURE_LOCATION "rsc/fonts/font.bmp"
#define FONT_DATA_LOCATION "rsc/fonts/font_data.txt"

// TODO: This is hard coded into the engine. Perhaps this should be defined by the user... Make more flexible. Perhaps just keep these as defaults and let user override with a separate function?
#define INITIAL_CAMERA_LOCATION { 0, 0, 3.0f }
#define INITIAL_CAMERA_TARGET { 0, 0, 0 }


struct FYEngine
{
    struct WindowState windowState;

    struct ShaderProgram shaderPrograms[NUMBER_OF_SHADER_CATEGORIES];

    struct Loader loader;
    struct Font2D font;

    struct Camera camera;
};


void CreateEngine( struct FYEngine* engine );
void DestroyEngine( struct FYEngine* engine );



#endif // ENGINE_H

