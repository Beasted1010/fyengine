
#include <stdio.h>

#include "common.h"

// NOTE: I am defnining this here. This is the class definition of stb_image (single header library)
//       It must be defined once since it includes the code of the class. It must also be included in a .c file
//          This is because the code is the code of a class definition and must be defined only once
//       I am also including the actual stb_image library (which seems to be required) to add to the compilation
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>


void ValidateObject( void* object, const char* name )
{
    if( !object )
    {
        LOG_ERROR( "Failed to allocate memory for %s!\n", name );
        // TODO: Handle error
    }
}
