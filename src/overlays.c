
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>

#include <stdlib.h>

#include "overlays.h"


void SetWidgetState( struct Widget* widget, ScreenComponentState state )
{
    switch( widget->type )
    {
        case wtPUSH_BUTTON:
        {
            widget->pushButton.screenData.state = state;
        } break;

        case wtCHECKBOX:
        {
            widget->checkbox.screenData.state = state;
        } break;

        case wtSLIDER:
        {
            // TODO: A slider widget has 2 screen data states, I need to think through how to handle this
            //widget->slider.railData.state = state;
            //widget->slider.sliderData.state = state;
        } break;

        case wtTEXTBOX:
        {
            widget->textbox.screenData.state = state;
        } break;
    }
}

void SetWidgetColor( struct Widget* widget, struct RGB color )
{
    switch( widget->type )
    {
        case wtPUSH_BUTTON:
        {
            widget->pushButton.screenData.color = color;
        } break;

        case wtCHECKBOX:
        {
            widget->checkbox.screenData.color = color;
        } break;

        case wtSLIDER:
        {
            // TODO: A slider widget has 2 screen data states, I need to think through how to handle this
            //widget->slider.railData.state = state;
            //widget->slider.sliderData.state = state;
        } break;

        case wtTEXTBOX:
        {
            widget->textbox.screenData.color = color;
        } break;
    }
}

void SetOverlayState( struct Overlay* overlay, ScreenComponentState state )
{
    overlay->screenData.state = state;
}

static inline void CreateAndDescribeQuadVertexArrayData( struct Loader* loader, float* vertices, const int valueCount, 
                                                         const int componentCount, unsigned int* vaoID, unsigned int* vboID )
{
    glGenVertexArrays( 1, vaoID );
    glGenBuffers( 1, vboID );
    glBindVertexArray( *vaoID );
    glBindBuffer( GL_ARRAY_BUFFER, *vboID );
    glBufferData( GL_ARRAY_BUFFER, sizeof(float) * valueCount, vertices, GL_STATIC_DRAW );
    glVertexAttribPointer( 0, componentCount, GL_FLOAT, GL_FALSE, 0, 0 );
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindVertexArray( 0 );

    AddVAOToLoader( loader, *vaoID );
    AddVBOToLoader( loader, *vboID );
}

static inline void CreateTexturedQuad( int x, int y, int width, int height, 
                                       unsigned int* vaoID, unsigned int* vboID, struct Loader* loader )
{
    const int vertex_count = 6;
    const int component_count = 4;
    const int value_count = vertex_count * component_count;
    float vertices[value_count] = 
    {
        // First Triangle -> positions and texture coordinates
        (float) x + width, (float) y + height, 1.0f, 0.0f,
        (float) x, (float) y + height, 0.0f, 0.0f,
        (float) x, (float) y, 0.0f, 1.0f,

        // Second Triangle
        (float) x + width, (float) y + height, 1.0f, 0.0f,
        (float) x, (float) y, 0.0f, 1.0f,
        (float) x + width, (float) y, 1.0f, 1.0f
    };

    CreateAndDescribeQuadVertexArrayData( loader, vertices, value_count, component_count, vaoID, vboID );
}

static inline void FillScreenData( struct ScreenData* screenData, int x, int y, int width, int height, 
                                   const char* tagName, struct RGB* color )
{
    screenData->startX = x;
    screenData->startY = y;
    screenData->width = width;
    screenData->height = height;

    CreateHUDFont2D( "rsc/fonts/font.bmp", "rsc/fonts/font_data.txt", &screenData->font );
    screenData->tagName = tagName;

    screenData->color.red = color->red;
    screenData->color.green = color->green;
    screenData->color.blue = color->blue;

    screenData->loader = CreateLoader();

    screenData->state = scsNONE;

    CreateTexturedQuad( screenData->startX, screenData->startY, screenData->width, screenData->height, 
                        &screenData->vaoID, &screenData->vboID, &screenData->loader );
}

struct Overlay CreateOverlay( int x, int y, int width, int height, const char* tagName, struct RGB color )
{
    struct Overlay overlay;

    FillScreenData( &overlay.screenData, x, y, width, height, tagName, &color );
    SetOverlayState( &overlay, scsDEACTIVATED );

    overlay.widgets = 0;
    overlay.widgetCount = 0;

    overlay.pushButtonCount = 0;
    overlay.checkboxCount = 0;
    overlay.sliderCount = 0;
    overlay.textboxCount = 0;

    return overlay;
}

void AddWidgetToOverlay( void* widget, int type, struct Overlay* overlay )
{
    overlay->widgets = (struct Widget*) realloc( overlay->widgets, sizeof(struct Widget) * (overlay->widgetCount + 1) );
    ValidateObject( overlay->widgets, "overlay->widgets" );

    overlay->widgets[overlay->widgetCount].type = type;
    switch( type )
    {
        case wtPUSH_BUTTON:
        {
            overlay->widgets[overlay->widgetCount].pushButton = *( (struct PushButton*) widget );
            overlay->pushButtonCount++;
        } break;

        case wtCHECKBOX:
        {
            overlay->widgets[overlay->widgetCount].checkbox = *( (struct Checkbox*) widget );
            overlay->checkboxCount++;
        } break;

        case wtSLIDER:
        {
            overlay->widgets[overlay->widgetCount].slider = *( (struct Slider*) widget );
            overlay->sliderCount++;
        } break;

        case wtTEXTBOX:
        {
            overlay->widgets[overlay->widgetCount].textbox = *( (struct Textbox*) widget );
            overlay->textboxCount++;
        } break;
        
        default:
        {
            LOG_ERROR("Widget has no known type! Type given value: %i", type);
        }
    }

    overlay->widgetCount++;
}

static void DrawQuad( unsigned int* vaoID )
{
    glBindVertexArray( *vaoID );
    glEnableVertexAttribArray( 0 );
    glDrawArrays( GL_TRIANGLES, 0, 6 );
    glDisableVertexAttribArray( 0 );
    glBindVertexArray( 0 );
}

static void DrawOverlayElement( struct ScreenData* screenData, struct ShaderProgram* shaderProgram, const char* colorUniformName )
{
    UseProgram( shaderProgram->id );
    glDisable( GL_DEPTH_TEST );

    LoadColor( &shaderProgram->id, colorUniformName, &screenData->color );
    DrawQuad( &screenData->vaoID );

    glEnable( GL_DEPTH_TEST );
    StopProgram();
}

void PlaceWidgetTitle( struct Widget* widget, enum ScreenComponentTitleEdge edge, enum ScreenComponentTitleBoundary boundary,
                       struct ShaderProgram* textShaderProgram )
{
    struct ScreenData screenData = GetScreenDataFromWidget( widget );

    int text_quad_width = GetTextQuadWidth( &screenData.font, screenData.tagName, 1.0f );

    // Component title starts in the center by default
    int lower_left_x = screenData.startX + ( (screenData.width / 2) - (text_quad_width / 2) );
    int lower_left_y = screenData.startY + ( (screenData.height / 2) - (screenData.font.cellHeight) );

    if( scteLEFT_EDGE || scteRIGHT_EDGE )
    {

    }


    screenData.textColor = { 100, 0, 0 };
    PrintHUDText2D( screenData.tagName, lower_left_x, lower_left_y,
                    1.0f, screenData.textColor, &screenData.font, textShaderProgram );
}

void DrawOverlay( struct Overlay* overlay, struct ShaderProgram* overlayShaderProgram,
                  struct ShaderProgram* widgetShaderProgram, struct ShaderProgram* textShaderProgram )
{
    if( overlay->screenData.state == scsDEACTIVATED )
        return;

    DrawOverlayElement( &overlay->screenData, overlayShaderProgram, "overlayColor" );

    for( int i = 0; i < overlay->widgetCount; i++ )
    {
        // TODO: Instead just do GetScreenData and do the same drawing stuff for each? But sliders are different. And returning structure is heavy
        switch( overlay->widgets[i].type )
        {
            case wtPUSH_BUTTON:
            {
                DrawOverlayElement( &overlay->widgets[i].pushButton.screenData, widgetShaderProgram, "widgetColor" );

                PlaceWidgetTitle( &overlay->widgets[i], scteCENTER, sctbINSIDE_BOUNDARY, textShaderProgram );
            } break;

            case wtCHECKBOX:
            {
                DrawOverlayElement( &overlay->widgets[i].checkbox.screenData, widgetShaderProgram, "widgetColor" );
            } break;

            case wtSLIDER:
            {
                DrawOverlayElement( &overlay->widgets[i].slider.railData, widgetShaderProgram, "widgetColor" );
                DrawOverlayElement( &overlay->widgets[i].slider.sliderData, widgetShaderProgram, "widgetColor" );
            } break;

            case wtTEXTBOX:
            {
                DrawOverlayElement( &overlay->widgets[i].textbox.screenData, widgetShaderProgram, "widgetColor" );

                PrintHUDText2D( overlay->widgets[i].textbox.text, overlay->widgets[i].textbox.screenData.startX, overlay->widgets[i].textbox.screenData.startY,
                                1.0f, overlay->widgets[i].textbox.textColor, &overlay->widgets[i].textbox.font, textShaderProgram );
            } break;
            
            default:
            {
                LOG_ERROR("Widget has no known type! Type given value: %i", overlay->widgets[i].type);
            }
        } 
        /*glEnable( GL_DEPTH_TEST );
        StopProgram();*/
    }
}

void DrawOverlays( struct Overlay* overlays, int overlay_count, struct ShaderProgram* overlayShaderProgram,
                   struct ShaderProgram* widgetShaderProgram, struct ShaderProgram* textShaderProgram )
{
    for( int i = 0; i < overlay_count; i++ )
    {
        DrawOverlay( &overlays[i], overlayShaderProgram, widgetShaderProgram, textShaderProgram );
    }
}

// TODO: Destroy fontsssss!
void DestroyOverlay( struct Overlay* overlay )
{
    DestroyLoader( &overlay->screenData.loader );

    overlay->screenData.state = scsDEACTIVATED;

    free( overlay->widgets );
    overlay->widgets = 0;

    overlay->widgetCount = 0;

    overlay->pushButtonCount = 0;
    overlay->checkboxCount = 0;
    overlay->sliderCount = 0;
    overlay->textboxCount = 0;
}

void DestroyOverlays( struct Overlay* overlays, int overlayCount )
{
    for( int i = 0; i < overlayCount; i++ )
    {
        DestroyOverlay( &overlays[i] );
    }
}

struct PushButton CreatePushButton( int x, int y, int width, int height, const char* tagName, struct RGB color )
{
    struct PushButton push_button;

    FillScreenData( &push_button.screenData, x, y, width, height, tagName, &color );

    return push_button;
}

void CreateAndAddPushButtonToOverlay( int relativeX, int relativeY, int width, int height, struct RGB color, 
                                      const char* tagName, struct Overlay* overlay )
{
    struct PushButton button = CreatePushButton( relativeX + overlay->screenData.startX, relativeY + overlay->screenData.startY, 
                                                 width, height, tagName, color );

    AddWidgetToOverlay( &button, wtPUSH_BUTTON, overlay );
}

struct Checkbox CreateCheckbox( int x, int y, int width, int height, const char* tagName, struct RGB color )
{
    struct Checkbox checkbox;

    FillScreenData( &checkbox.screenData, x, y, width, height, tagName, &color );

    return checkbox;
}

void CreateAndAddCheckboxToOverlay( int relativeX, int relativeY, int width, int height, struct RGB color, 
                                    const char* tagName, struct Overlay* overlay )
{
    struct Checkbox checkbox = CreateCheckbox( relativeX + overlay->screenData.startX, relativeY + overlay->screenData.startY, 
                                               width, height, tagName, color );

    AddWidgetToOverlay( &checkbox, wtCHECKBOX, overlay );
}

// TODO: Continue with this idea, but perhaps add some "argument" variable to the screenData, because "sliderData" should know its value
//       "argument" could be an enum for push button, checkbox, textfield?, etc. and something like slider's value for 
//          sliderData and rail's max for railData?          DESIGN. Also, limit all these arguments?
struct Slider CreateSlider( int railX, int railY, int railWidth, int railHeight, struct RGB railColor,
                            int sliderWidth, int sliderHeight, struct RGB sliderColor, const char* tagName,
                            int minValue, int maxValue, int defaultValue )
{
    struct Slider slider;

    FillScreenData( &slider.railData, railX, railY, railWidth, railHeight, tagName, &sliderColor );
    // TODO: Hard coding some offset for slider and such. Think through use of this slider better. Basing it on railX...
    FillScreenData( &slider.sliderData, railX, railY - 5, sliderWidth, sliderHeight, tagName, &railColor );

    return slider;
}

void CreateAndAddSliderToOverlay( int relativeRailX, int relativeRailY, int railWidth, int railHeight, struct RGB railColor,
                                  int sliderWidth, int sliderHeight, struct RGB sliderColor, const char* tagName,
                                  int minValue, int maxValue, int defaultValue, struct Overlay* overlay )
{
    struct Slider slider = CreateSlider( relativeRailX + overlay->screenData.startX, relativeRailY + overlay->screenData.startY, 
                                         railWidth, railHeight, railColor, sliderWidth, sliderHeight, sliderColor, tagName,
                                         minValue, maxValue, defaultValue );

    AddWidgetToOverlay( &slider, wtSLIDER, overlay );
}

struct Textbox CreateTextbox( int x, int y, int width, int height, const char* text, const char* tagName, struct RGB color )
{
    struct Textbox textbox;
    
    textbox.text = text;
    textbox.textColor = (struct RGB) { 255, 255, 255 };

    CreateHUDFont2D( "rsc/fonts/font.bmp", "rsc/fonts/font_data.txt", &textbox.font );

    FillScreenData( &textbox.screenData, x, y, width, height, tagName, &color );

    return textbox;
}

void CreateAndAddTextboxToOverlay( int relativeX, int relativeY, int width, int height, const char* text, struct RGB color,
                                   const char* tagName, struct Overlay* overlay )
{
    struct Textbox textbox = CreateTextbox( relativeX + overlay->screenData.startX, relativeY + overlay->screenData.startY, 
                                            width, height, text, tagName, color );

    AddWidgetToOverlay( &textbox, wtTEXTBOX, overlay );
}




