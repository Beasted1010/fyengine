
#include "stdlib.h"
#include "gamestate.h"


void InitializeGameState( struct GameState* state )
{
    state->gameObjects = 0;
    state->numGameObjects = 0;

    state->lightObjects = 0;
    state->numLightObjects = 0;

    state->npcObjects = 0;
    state->numNpcObjects = 0;
}

void FreeGameState( struct GameState* state )
{
    free( state->gameObjects ); state->gameObjects = 0; state->numGameObjects = 0;
    free( state->lightObjects ); state->lightObjects = 0; state->numLightObjects = 0;
    free( state->npcObjects ); state->npcObjects = 0; state->numNpcObjects = 0;
}

struct Object CreateObject( struct Loader* loader, float* vertices, int numVertices, unsigned int* indices, int numIndices,
                            float* textureCoords, int numTextureCoords, const char* textureLocation, struct Vector3f position )
{
    struct Object object;

    struct RawModel raw_model = LoadToVAO( loader, vertices, numVertices,
                                           indices, numIndices,
                                           textureCoords, numTextureCoords );

    unsigned int texture = LoadTexture( loader, textureLocation );
    object.texturedModel = CreateTexturedModel( raw_model, texture );

    object.position = position;

    return object;
}

struct Object CreateObjectFromRawModel( struct Loader* loader, struct RawModel* rawModel,
                                        const char* textureLocation, struct Vector3f position )
{
    struct Object object;

    unsigned int texture = LoadTexture( loader, textureLocation );
    object.texturedModel = CreateTexturedModel( *rawModel, texture );

    object.position = position;

    return object;
}

// TODO: Rethink the parameters, a pointer to numObjects but no pointer to Object? This is only called a few times too...
void AddObject( struct Object object, struct Object** objects, int* numObjects )
{
    (*objects) = (struct Object*) realloc( (*objects), sizeof(struct Object) * ((*numObjects) + 1) );
    (*objects)[(*numObjects)++] = object;
}




