
#include <stdio.h>

#include <GL/glew.h> // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GL/gl.h>

#include "renderer.h"

void RenderPrepare()
{
    glClearColor( 0.2,0.3,0.3,1 );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void Render( unsigned int programID, struct TexturedModel* texturedModel )
{
    struct RawModel model = texturedModel->rawModel;

    glUseProgram( programID );
    glBindVertexArray( model.vaoID );

    // TODO: I don't think this and disable are needed here, just surrounding the glVertexAtribPointer function thing (I think)
    //       I believe it's purpose is supposed to be to "open the gates" for data to flow into our attribute list
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );

    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, texturedModel->textureID );

    // TODO: It was recommended to use UNSIGNED_SHORT instead of int (performance boost) -> Will need change to index type
    glDrawElements( GL_TRIANGLES, model.vertexCount, GL_UNSIGNED_INT, (void*)0 );
    //glDrawArrays( GL_TRIANGLES, 0, model.vertexCount ); // Params = mode/first(starting index in arrays)/count( # indices to rdr)

    glBindTexture( GL_TEXTURE_2D, 0 );

    glDisableVertexAttribArray( 0 );
    glDisableVertexAttribArray( 1 );

    glBindVertexArray( 0 );
    glUseProgram( 0 );
}



