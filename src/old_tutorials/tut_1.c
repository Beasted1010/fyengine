

// Objectives:
//             1. Get window on screen - 9/22
//             2. Initialize OpenGL
//                  a. Create context for OpenGL - 9/23
//                      - Done with glfwCreateWindow function (otherwise Windows API has a wglCreateContext function)
//             3. 
//
//
// TODO:
//             - Modularize the creation of shaders -> "CreateShader" function and "CompileShader" function (called by CreateShader)
//                  - This includes being able to write GLSL code in a .txt file and reading that file (as a string), to serve as our shader code
//             - 



#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
// Handles window creation and events (minimal library, no rendering functions) -> And cross platform!
#include "GL/glew.h" // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h> // Located in glfw WIN64 library -> NOTE: the architecture (e.g. WIN64) is what your app is targeting
#include <GL/gl.h> // Located in MinGW include folder

#define SCREEN_HEIGHT 600
#define SCREEN_WIDTH 800


void FramebufferSizeCallback( GLFWwindow* window, int width, int height );
unsigned int CompileShader( const char* shaderSource, uint8_t isVertexShader );
unsigned int CreateShaders( const char* vertexShaderSource, const char* fragmentShaderSource );
struct ShaderProgramSource ReadShaderFile( const char* fileLocation );
void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource );
void ProcessInput( GLFWwindow* window );

enum ShaderType
{
    NONE = -1, VERTEX = 0, FRAGMENT = 1
};

struct ShaderProgramSource
{
    char* vertexShaderSource;
    char* fragmentShaderSource;
};

/* SHADERS (GLSL)
 * GLSL is very similar to C. The basic types of C are supported. A main function exists as well.
 * GLSL features 2 container types, vectors and matrices.
 * Vectors are a 1,2,3 or 4 component container for any basic types. They have the following forms:
 *      vecn (default vector of n floats), bvecn (n booleans), ivecn (n integers), uvecn (n unsigned ints), dvecn (n doubles)
 *      These vectors have fields: .x, .y, .z, .w
 *          Can also use .r,.g,.b,.a for colors or .s,.t,.p,.q for textures, which access the same members
 *          The vector type allows for interesting things: CODE-> vec2 someVec; vec4 difVec = someVec.xyxx; vec3 new = difVec.zyw;
 *          Can slo use in vector constructor calls: CODE-> vec2 vect = vec2(0.5,0.7); vect4 result=vec4(vect, 0.0,0.0);
 * GLSL has keywords "in" and "out" to handle input and output of a shader (which output of one shader is input to the next)
 *      Whenever an output variable matches with an input variable of the next shader stage they're passed along
 * This means the vertex shader would need some sort of input or else is useless, but it's on the GPU, we don't even call it
 *      The input to the vertex shader is actually straight from the vertex data
 *      To define how vertex data is organized, we specify input variables with "location" metadata,
 *          this allows us to configure the vertex attributes on the CPU. -> E.g. "layout (location = 0)"
 *          The vertex shader requires an extra layout specification for its inputs so that we can link it with the vertex data
 *          You COULD omit the layout part and query for attribute locations with "glGetAttribLocation", but not as clear
 * The fragment shader MUST have a vec4 color output variable, or else OpenGL will render your object black (or white)
 * If you want to send data from one shader to the next, you must declare an output of sending shader and a similar input
 *      in receiving shader. When the TYPES and NAMES are equal on BOTH SIDES, OpenGL will link those variables together
 *          This occurs while linking a program object
 * Vertex attributes and UNIFORMS are how you can send data from our application on CPU to the shaders on the GPU
 *      Uniforms are different from vertex attributes in that they are GLOBAL, meaning they are visible to all shaders within
 *          a shader program object. Their value ALSO persists between shaders, a change of the value remains from shader to shader
 *          THE UNIFORM VARIABLES ARE SET IN YOUR APPLICATION CODE, THIS IS HOW YOU CAN COMMUNICATE TO THE SHADER
 *              You add data into the uniform variable either in your own source code on in the GLSL code
 *                  The idea is usually to communicate (from your code on CPU) to shader code on GPU, so is typically in your code
 *                  This setting needs to be done via glGetUniformLocation to get index/locatio nof uniform attribtue in shader
 *                      So this setting must be done after compiling and linking the shader program, and then use that program
 *                      You can obtain the location of the uniform before "glUseProgram", but setting must be done AFTER
 *                          This is because the glUniform4f (the uniform setting function for vec4), sets for active program
 *                      Uniforms are great for attributes whose values should change each iteration of game loop (e.g. POSITION)
 *          Note that if a uniform variable isn't used anywhere in your GLSL code, the compiler will silently remove that variable
 *              This causes several frustrating errors
 *      Declare uniforms with the "uniform" keyword followed by a type and a name (like ins and outs)
 * 
 * .
 */

/* NOTE: Now reaidng shader code form file
// The source code for the vertex shader in OpenGL Shading Language (GLSL) -> In string form
// Each shader begins with a declaration of its version, 330 = 3.3 and we specify the use of core profile functionality
// Next declare all input vertex attributes in vertex shader with the "in" keyword
// Then we do processing on the input data
// The below shader source does not do any processing (we assume we retrieve normalized coordinates, not a realistic assumption)
const char* vertexShaderSource = "#version 330 core\n" // -> This source code must be dynamically compiled at runtime
    // Set location of input variable to 0. GLSL has vector datatype that contains 1-4 floats based on postfix digit (e.g. vec3)
    // Each value in the GLSL vector aPos can be retrieved via aPos.x, .y, .z (and .w if was vec4)
    "layout (location = 0) in vec3 aPos;\n" // We only care about position data, so only needsingle vertex attribute
    "out vec4 vertexColor;\n" // Used to demonstrate passing output of one shader to input of next
    "uniform vec4 ourColor;\n" // Used to demonstrate the uniform variables, this color could just be in the fragment shader code
    "void main()\n"
    "{\n"
        // Assign the position data predefined gl_Position variable (a vec4, so cast) to set the output of the vertex shader
        // This simply assigns our values to gl_Position after first casting them to a vec4
    //"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n" // Using the 4th dimension for perspective division
    "   gl_Position = vec4(aPos, 1.0);\n" // Using the neat property of vector types in GLSL
    //"   vertexColor = vec4(0.5, 0.0, 0.0, 1.0);\n" // Set output variable to be dark red
    "   vertexColor = ourColor;\n" // Demonstrating the use of a uniform variable
    "}\0";

// Similar to the above, written in GLSL, delcaration is for version 3.3 using core profile functionality
// We declare our output vector, this example only needs an output, and it defines the final color output (we calculate ourselves)
const char* fragmentShaderSource = "#version 330 core\n"
    "out vec4 FragColor;\n" // 4 component vector OUTput -> Red, Green, Blue, Alpha, declare output variable with out keyword
    "in vec4 vertexColor;\n" // Linked to vertexShaderSource out variable, used to demonstrate shader communication
    "void main()\n"
    "{\n"
        // This example uses an Orange color. 1.0 for alpha -> completely opaque
    "   FragColor = vertexColor;\n"//vec4(1.0f, 0.5f, 0.2f, 1.0f);\n" // RGBA values in range from 0.0 to 1.0f
    "}\n";
*/

int main( int argc, char** argv )
{
    if( !glfwInit() )
    {
        printf("Failed to initialize GLFW!\n");
        return -1;
    }

    // Set window hints BEFORE window and context creation so that you can influence their creation (as closely as possible)
    // Examples include: resizable, visible, focused, floating, maximized, etc.
    // Format of these hints WindowHint(option_to_configure, value_of_option)
    // These two lines translate to version "3.3" support. 
    // This version (3.3+) tells glfw explicitly that we want the core-profile (modern OpenGL, a subset of OpenGL)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // Specify client API version that created context must be compatible with
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // This is the version after the .
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // A window object encapsulates both a top-level window AND an OpenGL (or OpenGL ES) context.
    // Creates an OpenGL context as well as a window. glfwDestroyWindow or glfwTerminate destroys this context.
    // A window and a context are inseparably linked -> So the window object serves as the context as well.
    // NOTE: Vulkan does not have a context and the Vulkan instance is created via the Vulkan API itself.
    //          If using Vulkan to render a window, disable context creation by setting GLFW_CLIENT_API hint to GLFW_NO_API
    GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL /*monitor*/, NULL /*share*/);
    if( !window )
    {
        printf("Failed to create a window!\n");
        glfwTerminate();
        return -1;
    }

    // Successful creation of window does not change which context is current.
    // To use the newly created context, you must make it current. You can use the share parameter to share objects with another.
    // Only a single context may be current at a time.
    glfwMakeContextCurrent(window); // Creates the valid OpenGL context

    // NOTE: Only after context is set, NOW we can start calling OpenGL functions
    // This handles loading all of the OpenGL functions for us
    if( glewInit() != GLEW_OK )
    {
        printf("Failed to initialize glew!\n");
    }

    // We can make many of our own callback functions (e.g. process joystick input changes)
    // These callback functions need to be registered after creating the window and before the game loop
    // Tell GLFW that we want to call our framebuffer size function on every window resize. This function registers it
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    printf("GL Version: %s\n", glGetString(GL_VERSION));

    // Array of continuous memory with 9 floats that represent the vertices of our triangle (3 vertices)
    // OpenGL is a 3D graphics library so it requires 3D coordinates (X, Y, Z)
    /* This way doesn't scale well, if we had two triangles (forming a rectangle) here, we would have 6 vertices and 2 that overlap
     *      So, we instead will use Element Buffer Objects so that we only need to specify our unique vertices,
     *          we then tell OpenGL which order we'd like to draw them.
    float vertices[] = {
        -0.5f, -0.5f, 0.0f,
         0.5f, -0.5f, 0.0f,
         0.0f,  0.5f, 0.0f
    };
    */

    // Using indices allows us to need only 4 verticies instead of 6 (if were to draw a rectangle using 2 triangles)
    float vertices[] = {
        // positions        // colors
         0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom right
         -0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // bottom left
         0.0f,  0.5f, 0.0f, 0.0f, 0.0f, 1.0f// top
    };
         /*0.5f,  0.5f, 0.0f, // top right
         0.5f, -0.5f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f, // top left*/
    unsigned int indices[] = { // Starting from 0!
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };
    //

    // NOTE: Command pattern in OpenGL of creating a buffer object
    // VBO = Vertex Buffer Object -> Manages the GPU memory used by vertex shader -> Can store many vertices in GPU memory
    // OpenGL has many types of buffer objects, buffer objects are a type of object, each generated by a function of the form
    //      glGen* where * is the type of object (plural) all with same signature glGen*(GLsizei n, GLuint *objects)
    unsigned int VBO; // -> The unique ID corresponding to the specific buffer
    glGenBuffers(1, &VBO); // Creates and assigns an ID to this buffer (buffer variable has the ID) -> Generates ID
    // GL_ARRAY_BUFFER is the buffer type of a vertex buffer object (VBO)
    // NOTE: We are able to bind to several buffers at once, as long as they are of different buffer types
    glBindBuffer(GL_ARRAY_BUFFER, VBO); // Select the buffer, bind to GL_ARRAY_BUFFER to treat as an array
    // After binding, any buffer calls made (on GL_ARRAY_BUFFER target) will be used to configure the correctly bound buffer (VBO)
    // Now we can call "glBufferData" to copy the vertex data into the buffer's memory
    // This function is specifically targeted to copy user defined data into the currently bound buffer
    // glBufferData(type_of_buffer_to_copy_data_to, size_of_data_to_pass_in_bytes, actual_data_we_want_to_send, how_GPU_manage)
    // Last parameter can be of several values:
    //      GL_STATIC_DRAW: Data likely to not change at all or rarely
    //      GL_DYNAMIC_DRAW: Data likely to change a lot -> This & below ensures data placed in memory allowing for faster writes
    //      GL_STREAM_DRAW: Data that will change every time it is drawn
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW); // Create and initialize the data store

    // Now create and bind the Element Buffer Object
    unsigned int EBO;
    glGenBuffers(1, &EBO);

    // Like the VBO, we bind the EBO and copy the indices into the buffer with glBufferData
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW); // Copy indices into buffer

    /*
    // REGION: Handle vertex shader

    // At this point the vertex data is within memory on graphics card and managed by a vertex buffer object named VBO
    unsigned int vertexShader; // Create a vector shader object (which is referenced by an ID) to store vertex shader
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    // glShaderSource(shader_obj_to_compile, #_strings_as_source_code, actual_source_code, NULL)
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL); // Attach the shader source to the shader object
    glCompileShader(vertexShader); // Compile the vertex shader source code

    // Ensure compiling of the shader was successful
    { // Scoped to rid local variables
        int success;
        char infoLog[512];
        glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success); // Check compilation status of our shader (vertexShader)
        if( !success )
        {
            glGetShaderInfoLog(vertexShader, 512, NULL, infoLog); // Retrieve the error message
            printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED -> %s\n", infoLog); // Print the error message
        }
    }
    // Vertex shader now good to go
    */

    // REGION: Handle shaders
    struct ShaderProgramSource shaderProgramSource = ReadShaderFile( "rsc/shaders/shaders_tut.shader" );
    unsigned int shaderProgram = CreateShaders( shaderProgramSource.vertexShaderSource, shaderProgramSource.fragmentShaderSource );
    /*
    // REGION: Handle fragment shader
    // Process for compiling fragment shader is similar to the vertex shader, but now use GL_FRAGMMENT_SHADER
    unsigned int fragmentShader;
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER); // Create the fragment shader object
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL); // Attach fragment shader source to fragment shader object
    glCompileShader(fragmentShader); // Compile the fragment shader code

    // Ensure compiling of the shader was successful
    { // Scoped to rid local variables
        int success;
        char infoLog[512];
        glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success); // Check compilation status of our shader (fragmentShader)
        if( !success )
        {
            glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog); // Retrieve the error message
            printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED -> %s\n", infoLog); // Print the error message
        }
    }
    // Fragment shader now good to go

    // REGION: Linking shaders
    // Now we must create a shader program object which contains all the linked combination of multiple shaders
    // To use the above compiled shaders, we must link them to a shader program object
    // This program will then be activated when rendering objects.
    // The activated shader program's shaders will be used when we issue render calls
    // Linking shaders into a program ultimately links the outputs of each shader into the inputs of the next shader
    //      You will get linking errors if your outputs and inputs do not match
    
    // The program object
    unsigned int shaderProgram;
    shaderProgram = glCreateProgram(); // Creates program and returns ID reference to newly created program object
    // Now attach the newly created shaders to the program object and then link with glLinkProgram
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    // Check if linking shader program failed
    {
        int success;
        char infoLog[512];
        glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
        if(!success)
        {
            glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
            printf("ERROR::PROGRAM::SHADERS::LINKING_FAILED -> %s\n", infoLog);
        }
    }
    // You activiate the program object by calling ->   glUseProgram(shaderProgram);
    //          Every shader and rendering call after glUseProgram will now use this program object (and thus the shaders)
    // We can now delete the shader objects once they are linked into the program
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    */

    // At this point we have...
    //      * Sent the input vertex data to the GPU
    //      * Instructed GPU how it should process the vertex data within a vertex and fragment shader
    // We now need to tell OpenGL how to interpret the vertex data that is now in memory and how it should connect the vertex
    //  data to the vertex shader's attributes
    // Our vertex buffer data is formatted as...
    //      VERTEX 1            VERTEX 2            VERTEX 3
    //      X   Y   Z           X   Y   Z           X   Y   Z       -> 3 attributes per vertex, all happen to be positions
    //  Each value is a float (32 bits, 4 bytes), starting at byte 0 consecutively ordered as above from byte 0 to byte 36
    //  The stride is 12 bytes (4 * 3 = 12 bytes for a single vertex), offset = 0 (consecutive)
    // Tell OpenGL how to interpret our vertex data (per vertex attribute) using glVertexAttribPointer()
    // First arg: Index of generic vertex attrib to modify
    //              Since our vertex shader specified "layout (location = 0)", this sets location of vertex attribute to 0
    //                  This is the location of the position vertex attribute in the vertex shader with "layout (location = 0)"
    //              Since we want to pass in data to this vertex attribute, pass in 0 here
    // Second arg: Size -> Number of components per generic vertex attribute (must be 1,2,3,4 for the 4 possible vectors)
    //               The vertex attribute (aPos) is a "vec3", so we pass in 3
    // Third arg: Type of data -> The type of data for each of our vertex attributes (each component in array)
    // Fourth arg: Specify if we want data normalized: 
    //             If passing in integer values (int) and this is GL_TRUE, it will normalize to 0 (or -1 if signed) to 1
    //             We already have our data normalized, so we pass in GL_FALSE
    // Fifth arg: Stride -> Byte offset between consecutive generic vertex attributes
    //            We pass in "3 * sizeof(float)" since the next set of position data is 3 floats away
    //                  Since the array is tightly packed (consecutively placed), we could pass in 0 and OpenGL will figure it out
    //            If we had more vertex attributes (not just position?) we would need to carefuly define the spacing between each
    // Sixth arg: Pointer -> Points to where position data begins in the buffer (GL_ARRAY_BUFFER), represents the offset
    //            We have our position data at the start of the data array, so this value is just 0
    // This call takes its data from the VBO that is currently bound to GL_ARRAY_BUFFER (which we bound above, and still is)
    //  Since this is still bounded, glVertexAttribPointer has associated vertex attribute 0 with our VBO vertex data
    // I believe we need one call to this for each attribute in the VBO (each vertex attribute needs a separate call) (e.g. pos)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0); // Tell OpenGL how to interpret the vertex data
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float))); // Tell OpenGL how to interpret the vertex data
    // Each "in" variable for the Vertex shader source code is a vertex attribute. There is a limit to this number
    //  We can find this limit (limited by the hardware) by querying GL_MAX_VERTEX_ATTRIBS. OpenGL guarantees at least 16
    {
        int num_attributes;
        glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &num_attributes);
        printf("Max number of vertex attributes supported: %i\n", num_attributes);
    }

    // Now OpenGL knows how it should interpret the vertex data.
    // We now should enable the vertex attribute with glEnableVertexAttribArray, giving it the vertex attribute location
    // We need to do this since vertex attributes are disabled by default
    // glEnableVertexAttribArray(0); -> Not done since we don't want to enable it yet, just keeping for sake of learning


    // Handle the Vertex Array Object creation, this way we can save all of our state configurations in a single object
    //      This prevents the nightmare mentioned below of drawing an object in OpenGl "the bad way"
    unsigned int VAO;
    glGenVertexArrays(1, &VAO);



    // This tells OpenGL to draw its primitives in wireframe mode
    // First arg: Here we tell it to apply to the front and back triangles
    // Second arg: Tell function to draw them as lines
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    // To return back to the default mode...
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    while( !glfwWindowShouldClose(window) )
    {
        // REGION: Handle input...
        ProcessInput(window);

        // REGION: Rendering commands...
        glClearColor(0.2,0.3,0.3,1); // Specify the color to clear the screen to -> State setting function
        glClear(GL_COLOR_BUFFER_BIT); // Clear the screen to the specified color -> State using function

        // These 5 lines of code allow us to draw a triangle, but with OpenGL Version 1.1.
        /*glBegin(GL_TRIANGLES);
        glVertex2f(-0.5f, -0.5f);
        glVertex2f(0.0f, 0.5f);
        glVertex2f(0.5f, -0.5f);
        glEnd();*/

        //glDrawArrays(GL_TRIANGLES, 0, 3);

        /* Example drawing of an object in OpenGL, the bad way -> This is tedious for just a single object with 3 vertex attributes
        
        // 0. Copy our vertices array in a buffer for OpenGL to use
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        // 1. Then set the vertex attributes pointers
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(0); // Enable the vertex attributes since they are disabled by default
        // 2. Use our shader program we want to render an object
        glUseProgram(shaderProgram);
        // 3. Now draw the object
        someOpenGLFunctionThatDrawsOutTriangle();

         * The better approach is storing all these state configurations into an object 
         *      and simply bind that object to restore its state.
         * This can be accomplished with a VAO, which can be bounded like a VBO, and any subsequent vertex attribute calls
         *      from taht point on will be stored inside the VAO.
         *      With a VAO, we only need to make the calls to configure vertex attribute pointers once
         *          Whenever we want to draw the object, we just bind the corresponding VAO
         *      Drawing different vertex data and attribute configurations is as easy as binding a different VAO
         *      All of the state is stored inside of the VAO
         * Core OpenGL actually REQUIRES we use VAO so it knows what to do with our vertex inputs
         *      If we fail to bind a VAO, OpenGL will likely refuse to draw anything
         * A VAO stores the following
         *      Calls to glEnableVertexAttribArray or glDisableVertexAttribArray
         *      Vertex attribute configurations via glVertexAttribPointer
         *          - The stride and other configuration details (type, etc.) for the vertex attributes in the VBO
         *      Vertex buffer objects associated with vertex attributes by calls to glVertexAttribPointer
         *          - The pointer to each of our attributes in the VBO
        */

        // The good way to draw an object in OpenGL
        //  Uses a vAO that stores our vertex attribute configuration and which VBO to use
        // NOTE: When you have multiple objects that you want to draw, 
        //          you would first generate/configure all the VAOs (and thus the required VBO and attribute pointers)
        //          and store those for later use
        //          The moment you want to draw one of our objects, 
        //              we take the corresponding VAO, bind it, then draw the object and unbind the VAO again

        // ..:: Initialization code (done once, unless your object frequently changes)) ::.. -> I am just keeping all together
        // 1. Bind Vertex Array Object
        glBindVertexArray(VAO);
        // 2. Copy our vertices array in a buffer for OpenGL to use
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        // 3. Copy our index array in a element buffer for OpenGL to use
        // The use of Element Buffer Arrays will require that we use glDrawElements instead of glDrawArrays
        //      This indicates that we want to render the traingles from an index buffer
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW); // Copy indices into buffer

        // 4. Then set our vertex attributes pointers
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        // ..:: Drawing code (in render loop) ::.. Must happen in the render loop

        // Setting the uniform variable for the shader source code, add data to the uniform
        // Using this uniform variable allows us to update the color (or POSITION!) each iteration of the game loop
        // First we need to find the index/location of the uniform attribute in our shader, once obtained, 
        //  we can then update its value
        float greenValue = 0.8;
        int vertexColorLocation = glGetUniformLocation( shaderProgram, "ourColor"); // Get location of uniform
        // glGetUniformLocation will return -1 if the location could not be found
        glUseProgram( shaderProgram );
        // GLSL is a C library at its core, so no function overloading. OpenGL defines new functions for each type required.
        // So the function requires a specific postfix for the type of the uniform you want to set (e.g. glUniform4f)
        // A few possible postfixes: f (function expects a float as its value), i (int), 3f (3 floats), fv (float vector/array)
        glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f); // Set the uniform value

        // Draw the object
        //glUseProgram(shaderProgram); -> Using above, since adding uniform attribute example
        glBindVertexArray(VAO);
        // someOpenGLFunctionThatDrawsOurTraingle(); -> such as glDrawArrays(GL_TRIANGLES, 0, 3);
        // the glDrawArrays functions allows us to draw primitives 
        //      using the currently active shader, the previously defined vertex attribute configuration
        //      and the VBO's vertex data (indirectly bound via the VAO)
        // glDrawArrays( primitive_type_to_draw, starting_index_of_vertex_array_to_draw, number_vertices_to_draw)
        //          number_vertices_to_draw = 3, since we have only 1 triangle to draw, but could be like 6 if we had 2 triangles
        //glDrawArrays(GL_TRIANGLES, 0, 3); -> We now use the indices to draw so we can specify fewer vertices
        
        // glDrawElements(mode_to_draw_in, num_elements_to_draw_or_vertices, type_of_indices, offset_in_EBO)
        // This function takes its indices from teh EBO currently bound to the GL_ELEMENT_ARRAY_BUFFER target
        //      This means we have to bind the corresponding EBO each time we watn to render an object with indices
        //      But the Vertex Array Object also keeps track of element buffer object bindings
        //      The EBO currently bound while a VAO is bound, is stored as the VAO's EBO.
        //          Bindng to a VAO automatically binds its EBO
        //      NOTE: VAO stores teh glBindBuffer calls when the target is GL_ELEMENT_ARRAY_BUFFER
        //              This also means it sotres its unbind calls -> make sure you don't unbind element array buffer
        //                  before unbinding your VAO, otherwise it doesn't have an EBO configured
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0); // Unbind the vertex array, not necessary since only 1, but good practice and helps with organization




        

        // REGION: Check and call events and swap the buffers
        // Check if any events are triggered, update window state, call corresponding functions (which we set via callback methods)
        glfwPollEvents();

        // Swaps the color buffer (contains color values for each pixel in GLFW's window) used to draw in during this iteration
        //  and show it as output to the screen
        // Essentially, this will take the canvas we have been painting to this iteration and put it on the screen for user
        // NOTE: Double buffers are used to remove artifacts that occur during rendering
        //          The output image is not drawn in an instant, it is drawn pixel by pixel (usually left to right, top to bottom)
        //          This may cause artifacts to occur during rendering with a single buffer
        //          A double buffer eliminates this issue by having a "back" and a "front" buffer
        //          A back buffer is what we draw to, the front buffer is the current buffer shown to the user
        //          When we are ready to render to the screen, the back buffer is swapped with the front buffer (now draw over old)
        glfwSwapBuffers(window);
    }

    // Deallocate all resources
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    glfwTerminate(); // Clean/delete all of GLFW's resources that were allocated
    return 0;
}


// This is needed because we need to update the viewport for OpenGL (the rendering area) every time the screen is resized
void FramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    // Tell OpenGL the size of the rendering window (specifies how we want to display data and coords w.r.t. the window)
    // Specifying a smaller window than GLFWs dimensions, allows us to display other elements outside of the OpenGL viewport
    // NOTE: The processed coordinates in OpenGL are between -1 and 1, so the below maps (-1 to 1) to (0,WIDTH) and (0,HEIGHT)
    // This function also handles transforming the Normalized Device Coordinates to screen-space coordinates
    // Normalized Device Coordinates (NDC) have origin in the center and positive y-axis in upwards direction
    glViewport(0,0, width, height); // This specifies where all the OpenGL rendering is to be displayed

}

// A function to handle input so that it is organized
void ProcessInput(GLFWwindow* window)
{
    // Handle the event that the escape key is pressed -> Close the window
    if( glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS )
    {
        glfwSetWindowShouldClose(window, 1); // Second argument is 1 indicating true
    }
}


unsigned int CompileShader( const char* shaderSource, uint8_t isVertexShader )
{
    // Create the shader
    unsigned int shader = glCreateShader( isVertexShader ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER );
    glShaderSource( shader, 1, &shaderSource, NULL );

    // Compile the source
    glCompileShader(shader);

    int success;
    glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
    if( !success )
    {
        char infoLog[512];
        glGetShaderInfoLog( shader, 512, NULL, infoLog );
        printf("ERROR::SHADER::%s::COMPILATION_FAILED -> %s\n", isVertexShader ? "VERTEX" : "FRAGMENT", infoLog);
    }

    return shader;
}

unsigned int CreateShaders( const char* vertexShaderSource, const char* fragmentShaderSource )
{
    unsigned int shader_program;

    unsigned int vertex_shader = CompileShader( vertexShaderSource, 1 );
    unsigned int fragment_shader = CompileShader( fragmentShaderSource, 0 );
    

    // Create the program, attach shaders, and then link the shaders to the single program
    shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);

    int success;
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if( !success )
    {
        char infoLog[512];
        glGetProgramInfoLog(shader_program, 512, NULL, infoLog);
        printf("ERROR::PROGRAM::SHADERS::LINKING_FAILED -> %s\n", infoLog);
    }

    return shader_program;
}

int StringContains( const char* str, int strLen, const char* fragStr, int fragLen )
{
    for( int i = 0; i < strLen; i++ )
    {
        if( str[i] == fragStr[0] )
        {
            // str[i] == fragStr[0] so move to the next character for comparison
            int strLoc = i+1;
            // Loop through str and fragStr and compare each character
            int fragLoc = 1;
            for( ; fragLoc < fragLen && strLoc < strLen; fragLoc++, strLoc++ )
            {
                // If they differ at any spot, we do not have a match, keep going
                if( str[strLoc] != fragStr[fragLoc] )
                {
                    break;
                }
            }

            // If we ended with the fragment string location being the same as the fragment string length, then we have a match
            if( fragLoc == fragLen )
            {
                return 1;
            }
        }
    }
    return 0;
}

struct ShaderProgramSource ReadShaderFile( const char* fileLocation )
{
    int buffer = 255;
    enum ShaderType type = NONE;

    FILE* fp = fopen( fileLocation, "r" );

    // Skip any leading white space
    while( isspace( fgetc(fp) ) ) ;

    char line[buffer];

    // The shader source buffer that will ultimately be returned in a structure
    char* shaderSource[2] = { (char*) malloc(sizeof(char)), (char*) malloc(sizeof(char)) };
    shaderSource[0][0] = '\0';
    shaderSource[0][1] = '\0';

    while( !feof(fp) )
    {
        // Read in a line of text
        fgets( line, buffer, fp );

        if( StringContains( line, buffer, "#|", 2 ) )
        {
            const char* vertexShaderStr = "|Vertex Shader|";
            const char* fragmentShaderStr = "|Fragment Shader|";

            if( StringContains( line, buffer, vertexShaderStr, strlen(vertexShaderStr) ) )
            {
                type = VERTEX;
            }
            else if( StringContains( line, buffer, fragmentShaderStr, strlen(fragmentShaderStr) ) )
            {
                type = FRAGMENT;
            }
        }
        else
        {
            if( type >= 0 )
            {
                // Add on the line to the shader source. Include length of string to have null terminating character
                shaderSource[type] = (char*) realloc( shaderSource[type], 
                                                      sizeof(char) * (strlen(shaderSource[type]) + strlen(line) + 0) );
                strcat( shaderSource[type], line );
            }
            else
            {
                printf("The shader type was unable to be determined! Source file read: %s\n", fileLocation);
            }
        }
    }

    // Capture the return value before freeing the shaderSource buffer memeory
    struct ShaderProgramSource shaderProgramSource = { shaderSource[VERTEX], shaderSource[FRAGMENT] };

    free( shaderSource[VERTEX] );
    free( shaderSource[FRAGMENT] );

    return shaderProgramSource;
}

void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource )
{
    printf("\n------------VERTEX SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->vertexShaderSource);

    printf("\n------------FRAGMENT SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->fragmentShaderSource);
}





