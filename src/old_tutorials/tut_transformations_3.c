
//
// TODO:
//             - Remove OpenGL types (e.g. GLuint) with corresponding C type -> Essentially use what is typedef'd as
//             - 
//

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
// Handles window creation and events (minimal library, no rendering functions) -> And cross platform!
#include "GL/glew.h" // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h> // Located in glfw WIN64 library -> NOTE: the architecture (e.g. WIN64) is what your app is targeting
#include <GL/gl.h> // Located in MinGW include folder

#define SCREEN_HEIGHT 600
#define SCREEN_WIDTH 800


void FramebufferSizeCallback( GLFWwindow* window, int width, int height );
unsigned int CompileShader( const char* shaderSource, uint8_t isVertexShader );
unsigned int CreateShaders( const char* vertexShaderSource, const char* fragmentShaderSource );
struct ShaderProgramSource ReadShaderFile( const char* fileLocation );
void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource );
void ProcessInput( GLFWwindow* window );

enum ShaderType
{
    NONE = -1, VERTEX = 0, FRAGMENT = 1
};

struct ShaderProgramSource
{
    char* vertexShaderSource;
    char* fragmentShaderSource;
};

int main( int argc, char** argv )
{
    if( !glfwInit() )
    {
        printf("Failed to initialize GLFW!\n");
        return -1;
    }

    // Configure the window for API version 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // Specify client API version that created context must be compatible with
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // This is the version after the .
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create the window and the OpenGL context
    GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL /*monitor*/, NULL /*share*/);
    if( !window )
    {
        printf("Failed to create a window!\n");
        glfwTerminate();
        return -1;
    }

    // Make the OpenGL context current, this completes the creation of the OpenGL context
    glfwMakeContextCurrent(window); // Creates the valid OpenGL context

    // Load all of the OpenGL functions
    if( glewInit() != GLEW_OK )
    {
        printf("Failed to initialize glew!\n");
    }

    // Set the frambuffer size callback which will handle setting the viewport size
    //      This is the part of the screen that OpenGL can draw to
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    printf("GL Version: %s\n", glGetString(GL_VERSION));

    struct ShaderProgramSource shaderProgramSource = ReadShaderFile( "rsc/shaders/shaders_tut.shader" );
    unsigned int shaderProgram = CreateShaders( shaderProgramSource.vertexShaderSource, shaderProgramSource.fragmentShaderSource );
    //PrintShaderSource( &shaderProgramSource );

    // Set up vertex data (and buffer(s)) and configure vertex attributes
    float vertices[] = {
        // positions          // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 1.0f,   // top right
         0.5f, -0.5f, 0.0f,   1.0f, 0.0f,   // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f,   // bottom left
        -0.5f,  0.5f, 0.0f,   0.0f, 1.0f    // top left 
    };
    int stride_length = 5 * sizeof(float);

    unsigned int indices[] = {
        0, 1, 3, // first triangle
        1, 2, 3 // second triangle
    };

    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    // Bind Vertex Array Object first, then bind and set vertex buffer(s), and configure vertex attribute(s)
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride_length, (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride_length, (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // REGION: Using glm to do vector, matrix, and transformation stuff
    
    // Define vector using glm's vec class
    glm::vec4 vec(1.0f, 0.0f, 0.0f, 1.0f);
    // Define a matrix using glm's built in mat class, mat4 is a 4x4 matrix in our case -> This is an identity matrix to start
    glm::mat4 trans;
    // Create a translation matrix, this involves passing in our identity matrix to glm::translate along with a translation vec
    // This makes trans the appropriate matrix associated with our desired translation (vec3(1,1,0)) -> func does multiplication
    trans = glm::translate(trans, glm::vec3(1.0f, 1.0f, 0.0f));
    // Multiply the vector by our translation matrix to perform the transformation
    //      Result should be (2,1,0) since a translation adds the components -> (1+1, 0+1, 0+0) = (2,1,0)
    vec = trans * vec;
    printf("%f %f %f\n", vec.x, vec.y, vec.z);

    // REGION Texture handling
    // You can specify to extract more than 0-1f from the image though, but how should OpenGL handle this? ... w/ GL_TEXTURE_WRAP
    // This means that the texture coordinates do not depend on resolution, they can be any floating point value
    /*glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

    // Retrieving the texture color using texture coordinates is called sampling
    //      Essentially takes image and cuts out your vertices

    // Set the texture filtering option for scaling (magnifying and minifying operations)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);*/

    // To use textures... First load them into your application, we will use stb_image.h from open source stb project
    int width, height, nrChannels;
    unsigned char* data = stbi_load("rsc/container.jpg", &width, &height, &nrChannels, 0);

    if( !data )
    {
        printf("Failed to load texture!\n");
    }

    // Textures (like any of our previous OpenGL objects) are referenced by an ID
    unsigned int texture1, texture2;
    glGenTextures(1, &texture1); // Generate 1 texture and store them in our unsigned int array given as second arg (here just 1)
    // Just like other objects, we need to bind it so any subsequent commands will configure the currently bound texture
    glBindTexture(GL_TEXTURE_2D, texture1);

    // Set texture wrapping parameters -> To GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Set the texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Texutres are bound, so now start generating the texture using our loaded image data
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    
    // glGenerateMipmap will automatically generate all mipmaps for the currently bound texture for us.
    glGenerateMipmap(GL_TEXTURE_2D);

    // Loading another texture ...
    // Free the image memory of the previous
    stbi_image_free(data);

    glGenTextures(1, &texture2);
    glBindTexture(GL_TEXTURE_2D, texture2);

    // Set texture wrapping parameters -> To GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Set the texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // We use stbi to flip the bit ordering for us since our image needs an origin at bottom left (:
    stbi_set_flip_vertically_on_load(1); // 1 = TRUE
    data = stbi_load("rsc/awesomeface.png", &width, &height, &nrChannels, 0);
    
    if( !data )
    {
        printf("Failed to load texture!\n");
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    // First activate the shader program so we can get the uniform location of the textures...
    glUseProgram(shaderProgram);
    // Tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
    glUniform1i( glGetUniformLocation(shaderProgram, "texture1"), 0);
    glUniform1i( glGetUniformLocation(shaderProgram, "texture2"), 1);

    stbi_image_free(data);

    glUseProgram(shaderProgram);

    while( !glfwWindowShouldClose(window) )
    {
        ProcessInput( window );

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // Activate texture unit first before rendering texture. OpenGL has a minimum of 16 texture units for use
        // These texture units are defined in order, so you could get to GL_TEXTURE8 via "GL_TEXTURE0 + 8" (e.g. in a loop)
        glActiveTexture(GL_TEXTURE0); // texture unit TEXTURE0 is always by default activated, so if only 1 texture, no need
        // Bind our texture to the GL_TEXTURE_2D object
        glBindTexture(GL_TEXTURE_2D, texture1); // Bind texture to the CURRENTLY ACTIVE texture unit
        // Do the same for the second texture
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);


        // Let's scale and rotate the container object (the image loaded) from previous texture tutorial
        // The below two lines first scales the container by 0.5 on each axis, and then rotates the container 90 deg around z-axis
        // We rotate around z axis since the image is on the x/y plane (the screen)
        // NOTE: The axis we rotate around should be a UNIT VECTOR, so normalize it if it is not already a unit vector
        // NOTE: GLM expects its angles in radians, so use glm::radians(...) to convert
        // Passing the matrix to each of GLM's functions leads to GLM automatically multiplying the matrices together
        //      This results in a matrix that combines all the transformations
        glm::mat4 transform;
        // Despite the order of the operations below, we first rotate and then translate
        //      The actual transformation order should be read in reverse
        // These two commented out lines scale each axis by 0.5 and THEN rotate by 90 degrees
        //transform = glm::rotate(transform, glm::radians(90.0f), glm::vec3(0.0, 0.0, 1.0));
        //transform = glm::scale(transform, glm::vec3(0.5, 0.5, 0.5));  
        // These two lines first rotate by some amount depending on the time, and then translate (move) by some amount (x and y)
        // The translation part essentially puts it somewhere other than the center (here it is 0.5 along x axis, -.5 on y axis)
        transform = glm::translate(transform, glm::vec3(0.5f, -0.5f, 0.0f));
        transform = glm::rotate(transform, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));
        glUseProgram(shaderProgram);
        // We now want to get this translation matrix (which wil scale by 0.5 on each axis and rotate by 90 degrees) to our
        //      shaders. So use a 4x4 matrix (mat4) uniform variable and multiply the position vector by the matrix uniform
        unsigned int transformLoc =  glGetUniformLocation( shaderProgram, "transform" ); // Get location of uniform variable
        // 1st arg: the uniform's location
        // 2nd arg: # of matrices we'd like to send (here just the 1)
        // 3rd arg: Do we want to transpose our matrix? This is to support the OpenGL internal matrix layout of column-major ordering
        //          This is the default matrix layout in GLM so there is no need to transpose the matrices
        // 4th arg: The actual matrix data. GLM stores matrices in diff way than how OpenGL wants, so first transform with value_ptr
        glUniformMatrix4fv( transformLoc, 1, GL_FALSE, glm::value_ptr(transform)); // Send data to shaders
        
        // NOTE: The beauty of these matrices is that we can define an infinite amount of transformations and combien them all
        //          into a single matrix that can be reused as ofted as we like.
        //       Using the matrix as we are in the vertex shader allows us to not have to redefine the vertex data and saves us
        //          some processing time as well (since we don't have to resend our data to GPU all the time, which is very slow)

        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_TRIANGLES, 0, 3);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // Deallocate all resources
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    glfwTerminate(); // Clean/delete all of GLFW's resources that were allocated
    return 0;
}


// This is needed because we need to update the viewport for OpenGL (the rendering area) every time the screen is resized
void FramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0,0, width, height); // This specifies where all the OpenGL rendering is to be displayed

}

// REGION: HANDLE INPUT
// A function to handle input so that it is organized
void ProcessInput(GLFWwindow* window)
{
    // Handle the event that the escape key is pressed -> Close the window
    if( glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS )
    {
        glfwSetWindowShouldClose(window, 1); // Second argument is 1 indicating true
    }
}


// REGION: HANDLE SHADERS
// Can compile either a vertex shader or a fragment shader
unsigned int CompileShader( const char* shaderSource, uint8_t isVertexShader )
{
    // Create the shader
    unsigned int shader = glCreateShader( isVertexShader ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER );
    glShaderSource( shader, 1, &shaderSource, NULL );

    // Compile the source
    glCompileShader(shader);

    int success;
    glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
    if( !success )
    {
        char infoLog[512];
        glGetShaderInfoLog( shader, 512, NULL, infoLog );
        printf("ERROR::SHADER::%s::COMPILATION_FAILED -> %s\n", isVertexShader ? "VERTEX" : "FRAGMENT", infoLog);
    }

    return shader;
}

// Creates both the vertex shader and the fragment shader
unsigned int CreateShaders( const char* vertexShaderSource, const char* fragmentShaderSource )
{
    unsigned int shader_program;

    unsigned int vertex_shader = CompileShader( vertexShaderSource, 1 );
    unsigned int fragment_shader = CompileShader( fragmentShaderSource, 0 );
    

    // Create the program, attach shaders, and then link the shaders to the single program
    shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);

    int success;
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if( !success )
    {
        char infoLog[512];
        glGetProgramInfoLog(shader_program, 512, NULL, infoLog);
        printf("ERROR::PROGRAM::SHADERS::LINKING_FAILED -> %s\n", infoLog);
    }
    
    // Delete the shaders since they are now linked to the shader program (we don't need these references anymore)
    glDeleteShader( vertex_shader );
    glDeleteShader( fragment_shader );

    return shader_program;
}

int StringContains( const char* str, int strLen, const char* fragStr, int fragLen )
{
    for( int i = 0; i < strLen; i++ )
    {
        if( str[i] == fragStr[0] )
        {
            // str[i] == fragStr[0] so move to the next character for comparison
            int strLoc = i+1;
            // Loop through str and fragStr and compare each character
            int fragLoc = 1;
            for( ; fragLoc < fragLen && strLoc < strLen; fragLoc++, strLoc++ )
            {
                // If they differ at any spot, we do not have a match, keep going
                if( str[strLoc] != fragStr[fragLoc] )
                {
                    break;
                }
            }

            // If we ended with the fragment string location being the same as the fragment string length, then we have a match
            if( fragLoc == fragLen )
            {
                return 1;
            }
        }
    }
    return 0;
}

struct ShaderProgramSource ReadShaderFile( const char* fileLocation )
{
    int buffer = 255;
    enum ShaderType type = NONE;

    FILE* fp = fopen( fileLocation, "r" );

    // Skip any leading white space
    while( isspace( fgetc(fp) ) ) ;

    char line[buffer];

    // The shader source buffer that will ultimately be returned in a structure
    char* shaderSource[2] = { (char*) malloc(sizeof(char)), (char*) malloc(sizeof(char)) };
    shaderSource[0][0] = '\0';
    shaderSource[0][1] = '\0';

    while( !feof(fp) )
    {
        // Read in a line of text
        fgets( line, buffer, fp );

        if( StringContains( line, buffer, "#|", 2 ) )
        {
            const char* vertexShaderStr = "|Vertex Shader|";
            const char* fragmentShaderStr = "|Fragment Shader|";

            if( StringContains( line, buffer, vertexShaderStr, strlen(vertexShaderStr) ) )
            {
                type = VERTEX;
            }
            else if( StringContains( line, buffer, fragmentShaderStr, strlen(fragmentShaderStr) ) )
            {
                type = FRAGMENT;
            }
        }
        else
        {
            if( type >= 0 )
            {
                // Add on the line to the shader source. Include length of string to have null terminating character
                shaderSource[type] = (char*) realloc( shaderSource[type], 
                                                        sizeof(char) * (strlen(shaderSource[type]) + strlen(line) + 0) );
                strcat( shaderSource[type], line );
            }
            else
            {
                printf("The shader type was unable to be determined! Source file read: %s\n", fileLocation);
            }
        }
    }

    // Capture the return value before freeing the shaderSource buffer memeory
    struct ShaderProgramSource shaderProgramSource = { shaderSource[VERTEX], shaderSource[FRAGMENT] };

    free( shaderSource[VERTEX] );
    free( shaderSource[FRAGMENT] );

    return shaderProgramSource;
}

void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource )
{
    printf("\n------------VERTEX SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->vertexShaderSource);

    printf("\n------------FRAGMENT SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->fragmentShaderSource);
}





