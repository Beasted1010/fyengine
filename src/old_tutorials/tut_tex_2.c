
//
// TODO:
//             - Remove OpenGL types (e.g. GLuint) with corresponding C type -> Essentially use what is typedef'd as
//             - 
//

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
// Handles window creation and events (minimal library, no rendering functions) -> And cross platform!
#include "GL/glew.h" // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h> // Located in glfw WIN64 library -> NOTE: the architecture (e.g. WIN64) is what your app is targeting
#include <GL/gl.h> // Located in MinGW include folder

#define SCREEN_HEIGHT 600
#define SCREEN_WIDTH 800


void FramebufferSizeCallback( GLFWwindow* window, int width, int height );
unsigned int CompileShader( const char* shaderSource, uint8_t isVertexShader );
unsigned int CreateShaders( const char* vertexShaderSource, const char* fragmentShaderSource );
struct ShaderProgramSource ReadShaderFile( const char* fileLocation );
void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource );
void ProcessInput( GLFWwindow* window );

enum ShaderType
{
    NONE = -1, VERTEX = 0, FRAGMENT = 1
};

struct ShaderProgramSource
{
    char* vertexShaderSource;
    char* fragmentShaderSource;
};

int main( int argc, char** argv )
{
    if( !glfwInit() )
    {
        printf("Failed to initialize GLFW!\n");
        return -1;
    }

    // Configure the window for API version 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // Specify client API version that created context must be compatible with
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // This is the version after the .
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create the window and the OpenGL context
    GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL /*monitor*/, NULL /*share*/);
    if( !window )
    {
        printf("Failed to create a window!\n");
        glfwTerminate();
        return -1;
    }

    // Make the OpenGL context current, this completes the creation of the OpenGL context
    glfwMakeContextCurrent(window); // Creates the valid OpenGL context

    // Load all of the OpenGL functions
    if( glewInit() != GLEW_OK )
    {
        printf("Failed to initialize glew!\n");
    }

    // Set the frambuffer size callback which will handle setting the viewport size
    //      This is the part of the screen that OpenGL can draw to
    glfwSetFramebufferSizeCallback(window, FramebufferSizeCallback);

    printf("GL Version: %s\n", glGetString(GL_VERSION));

    struct ShaderProgramSource shaderProgramSource = ReadShaderFile( "rsc/shaders/shaders_tut.shader" );
    unsigned int shaderProgram = CreateShaders( shaderProgramSource.vertexShaderSource, shaderProgramSource.fragmentShaderSource );
    //PrintShaderSource( &shaderProgramSource );

    // Set up vertex data (and buffer(s)) and configure vertex attributes
    float vertices[] = {
        // positions          // colors           // texture coords
         0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // top right
         0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // bottom right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // bottom left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // top left 
    };
    int stride_length = 8 * sizeof(float);

    unsigned int indices[] = {
        0, 1, 3, // first triangle
        1, 2, 3 // second triangle
    };

    // REGION Texture handling
    // NOTE: Texture coordinates are s,t,r corresponding to x,y,z
    // Set the texture wrapping beahvior to mirrored_repeat for both s and t direction
    // This specifiies how OpenGL shall handle texture coordinates outside of the 0-1 range (which is the expected range)
    // In other words, since a texture's coordinates range from 0-1, but OpenGL allows for any float value, OpenGL will have to
    // know how to handle the case you specify a coordinate outside of the 0-1 range. the GL_TEXTURE_WRAP val determines this
    // Given any image, its coordinates range from 0-1f each.
    // You can specify to extract more than 0-1f from the image though, but how should OpenGL handle this? ... w/ GL_TEXTURE_WRAP
    // This means that the texture coordinates do not depend on resolution, they can be any floating point value
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

    // Retrieving the texture color using texture coordinates is called sampling
    //      Essentially takes image and cuts out your vertices

    // Since the texture coordinates are independent of the resolution and can be any floating point value, OpenGL has to 
    // figure out which texture pixel (AKA "texel") maps to which texture coordinates.
    // NOTE: When first loaded, a texture's individual pixels likely do not have a 1 pixel to 1 pixel mapping to your screen
    //       This means that a low resolution image may have 20 pixels of your screen for every 1 pixel of the image.
    //       THINK of how a "pixelated image looks", its individual pixels are visible, your screen has more pixels per pixel of
    //       the image. This means a texture COORDINATE is fundamentally different from a texture PIXEL
    //       A texture COORDINATE is the specific location within the image once translated to your screen's pixels
    //          This value can be within an actual texture's pixel.
    //       A texture PIXEL is one of those pixels belonging to the texture (which may have many of your screen's pixels)
    //       Due to this lack of texture coordinates mapping to texture resolution, OpenGL must determine what color to use
    //       This whole process is "TEXTURE FILTERING"
    // Texel = The pixel on the texture, this is the actual color/pixel to be rendered at some location (texture coordinate that
    //         This is the pixel that cooresponds to the texture's resolution (which may consist of many of your screen's pixels)
    // is being rendered). OpenGL has options for handling this "texture filtering" -> GL_NEAREST and GL_LINEAR
    // GL_NEAREST: Default filtering method. OpenGL selects texture PIXEL which center is closest to texture COORDINATE
    // GL_LINEAR: Takes an interpolated value from the texture COORDINATE's neighboring TEXELS (texture PIXELS) approx a color
    // Set the texture filtering option for scaling (magnifying and minifying operations)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Mipmaps on wikipedia: pre-calculated optimized sequence of images, 
    //      each of which is a progressively lower resolution representation of same image
    // mipmaps: An OpenGL concept used to make rendering high resolution textures at a far distant more efficient
    // A high resolution image at a far distant should be small than close up, and thus doesn't require so high resolution
    // A way to solve this is to determine threshold distances from viewer, once past determine the mipmap that suites the
    // distance to the object the best. Since the object is far away, a smaller resolution will not be noticable to the user
    // You could create each mipmapped texture yourself, but OpenGL offers a single call (glGenerateMinimaps) to do this
    // The result is essentially a collection of textures with varying resolutions (at varying distances) -> Layered in one
    // Filtering methods can be used on mipmaps as well to handle artifacts such as sharp edges between two minimap layers
    // These methods are the same NEAREST and LINEAR as mentioned above.
    // You would us glTexParmeter to set the filtering method. If you use mipmaps it would replace the above filter selection
    // The options include: GL_NEAREST_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR
    // These values represent a combination of filtering techniques for mipmap selection and sampling within the minimap
    // GL_NEAREST_MIPMAP_LINEAR -> Linearly interpolates between the two mipmaps that most closely match the size of a pixel
    //      and samples via nearest neighbor interpolation
    // e.g. glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR)
    //      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)

    // To use textures... First load them into your application, we will use stb_image.h from open source stb project
    int width, height, nrChannels;
    unsigned char* data = stbi_load("rsc/container.jpg", &width, &height, &nrChannels, 0);

    if( !data )
    {
        printf("Failed to load texture!\n");
    }

    // Textures (like any of our previous OpenGL objects) are referenced by an ID
    unsigned int texture1, texture2;
    glGenTextures(1, &texture1); // Generate 1 texture and store them in our unsigned int array given as second arg (here just 1)
    // Just like other objects, we need to bind it so any subsequent commands will configure the currently bound texture
    glBindTexture(GL_TEXTURE_2D, texture1);

    // Set texture wrapping parameters -> To GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Set the texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Texutres are bound, so now start generating the texture using our loaded image data
    // Arg 1: Specify which texture target to operate on. We can have 1D and 3D textures bound at same time and it will be fine
    // Arg 2: Specify mipmap level for which we want to create a texture for -> No desire to use mipmap, so use base level of 0
    // Arg 3: What format to store picture? Our image has only RGB values, so store as such.
    // Arg 4: The width of the resulting texture
    // Arg 5: The height of the resulting texture
    // Arg 6: Must be 0 (legacy stuff)
    // Arg 7: The format of the source image, loaded as RGB values -> Format of pixel data
    // Arg 8: The datatype of source image, stored as chars (bytes) -> The datatype of each pixel
    // Arg 9: The image data
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Now the currently bound texture object has the texture image attached to it
    // The texture only has the base level of the mipmap, to generate each corresponding layer we would need to repeatedly
    // call glTexImage2D incrementing the second argument only each time. That is the manual way.
    // glGenerateMipmap will automatically generate all mipmaps for the currently bound texture for us.
    glGenerateMipmap(GL_TEXTURE_2D);

    // A sampler is a particular GLSL type which represents a single texture of a particular texture type
    // It is essentially a blob of data (our texture) from which things can be sampled from?
    // NOTE: When dealing with the shader code, the vertex shader will now take in a tex coord and output it for fragment shader
    // E.g. layout (location = 2) in vec2 aTexCoord;         out vec2 texCoord;
    // The fragment shader will take this in and also have a uniform sampler2D type to store our texture (sampler2D is built in)
    // FragColor will then be the result of the "texture" function which samples corresponding color 
    // value using texture parameters we set earlier. The output of this fragment shader is the (filtered) color texture at
    // the (interpolated) texture coordinate
    // E.g. in vec2 texCoord;           uniform sampler2D ourTexture; FragColor = texture(ourTexture, texCoord);
    // Here we don't actually assign a value to the sampler2D variable with glUniform
    //      We could use glUniform1i to assign a LOCATION value to the texture sampler so we can set multiple textures at 
    //      once in a fragment shader. This specific location is more commonly known as a "texture unit"
    //      Texture unit = location value for texture sampler -> Default is 0 (the default active texture unit)
    //      If we want to use multiple textures (active texture is not the only texture) we will need to specify the location
    //      Not all graphics drivers assign a default texture unit, so you may get an error if you don't specify a location
    //      Texture unit -> Main purpose: To use more than 1 texture in our shaders
    //      When we assign texture units to the samplers, we can bind multiple textures at once, we just have to activate
    //      the corresponding texture unit first, like glBindTexture, we activate a texture with glActivateTexture.
    // This is all shown below in game loop

    // Now just bind the texture before calling the glDrawElements function (done in game loop when ready to use)


    // Loading another texture ...

    // Free the image memory of the previous
    stbi_image_free(data);

    glGenTextures(1, &texture2);
    glBindTexture(GL_TEXTURE_2D, texture2);

    // NOTE: Our image may be upside down originally! This is because OpenGL expects 0,0 to be lower left, but some images
    // (e.g. our smiley face texture from a png file) has 0,0 at the upper left
    // We use stbi to flip the ordering for us (:
    stbi_set_flip_vertically_on_load(1); // 1 = TRUE
    data = stbi_load("rsc/awesomeface.png", &width, &height, &nrChannels, 0);
    
    if( !data )
    {
        printf("Failed to load texture!\n");
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    // First activate the shader program so we can get the uniform location of the textures...
    glUseProgram(shaderProgram);
    // Tell opengl for each sampler to which texture unit it belongs to (only has to be done once)
    glUniform1i( glGetUniformLocation(shaderProgram, "texture1"), 0);
    glUniform1i( glGetUniformLocation(shaderProgram, "texture2"), 1);

    stbi_image_free(data);

    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    // Bind Vertex Array Object first, then bind and set vertex buffer(s), and configure vertex attribute(s)
    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride_length, (void*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride_length, (void*)(3 * sizeof(float)));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride_length, (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glUseProgram(shaderProgram);

    while( !glfwWindowShouldClose(window) )
    {
        ProcessInput( window );

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // We ultimately want to use both of our textures, so change the rendering a bit
        // We will use 2 texture units, and bind to each a corresponding texture

        // Activate texture unit first before rendering texture. OpenGL has a minimum of 16 texture units for use
        // These texture units are defined in order, so you could get to GL_TEXTURE8 via "GL_TEXTURE0 + 8" (e.g. in a loop)
        glActiveTexture(GL_TEXTURE0); // texture unit TEXTURE0 is always by default activated, so if only 1 texture, no need
        // Bind our texture to the GL_TEXTURE_2D object
        glBindTexture(GL_TEXTURE_2D, texture1); // Bind texture to the CURRENTLY ACTIVE texture unit
        // Do the same for the second texture
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_TRIANGLES, 0, 3);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // Deallocate all resources
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    glfwTerminate(); // Clean/delete all of GLFW's resources that were allocated
    return 0;
}


// This is needed because we need to update the viewport for OpenGL (the rendering area) every time the screen is resized
void FramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0,0, width, height); // This specifies where all the OpenGL rendering is to be displayed

}

// REGION: HANDLE INPUT
// A function to handle input so that it is organized
void ProcessInput(GLFWwindow* window)
{
    // Handle the event that the escape key is pressed -> Close the window
    if( glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS )
    {
        glfwSetWindowShouldClose(window, 1); // Second argument is 1 indicating true
    }
}


// REGION: HANDLE SHADERS
// Can compile either a vertex shader or a fragment shader
unsigned int CompileShader( const char* shaderSource, uint8_t isVertexShader )
{
    // Create the shader
    unsigned int shader = glCreateShader( isVertexShader ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER );
    glShaderSource( shader, 1, &shaderSource, NULL );

    // Compile the source
    glCompileShader(shader);

    int success;
    glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
    if( !success )
    {
        char infoLog[512];
        glGetShaderInfoLog( shader, 512, NULL, infoLog );
        printf("ERROR::SHADER::%s::COMPILATION_FAILED -> %s\n", isVertexShader ? "VERTEX" : "FRAGMENT", infoLog);
    }

    return shader;
}

// Creates both the vertex shader and the fragment shader
unsigned int CreateShaders( const char* vertexShaderSource, const char* fragmentShaderSource )
{
    unsigned int shader_program;

    unsigned int vertex_shader = CompileShader( vertexShaderSource, 1 );
    unsigned int fragment_shader = CompileShader( fragmentShaderSource, 0 );
    

    // Create the program, attach shaders, and then link the shaders to the single program
    shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);

    int success;
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if( !success )
    {
        char infoLog[512];
        glGetProgramInfoLog(shader_program, 512, NULL, infoLog);
        printf("ERROR::PROGRAM::SHADERS::LINKING_FAILED -> %s\n", infoLog);
    }
    
    // Delete the shaders since they are now linked to the shader program (we don't need these references anymore)
    glDeleteShader( vertex_shader );
    glDeleteShader( fragment_shader );

    return shader_program;
}

int StringContains( const char* str, int strLen, const char* fragStr, int fragLen )
{
    for( int i = 0; i < strLen; i++ )
    {
        if( str[i] == fragStr[0] )
        {
            // str[i] == fragStr[0] so move to the next character for comparison
            int strLoc = i+1;
            // Loop through str and fragStr and compare each character
            int fragLoc = 1;
            for( ; fragLoc < fragLen && strLoc < strLen; fragLoc++, strLoc++ )
            {
                // If they differ at any spot, we do not have a match, keep going
                if( str[strLoc] != fragStr[fragLoc] )
                {
                    break;
                }
            }

            // If we ended with the fragment string location being the same as the fragment string length, then we have a match
            if( fragLoc == fragLen )
            {
                return 1;
            }
        }
    }
    return 0;
}

struct ShaderProgramSource ReadShaderFile( const char* fileLocation )
{
    int buffer = 255;
    enum ShaderType type = NONE;

    FILE* fp = fopen( fileLocation, "r" );

    // Skip any leading white space
    while( isspace( fgetc(fp) ) ) ;

    char line[buffer];

    // The shader source buffer that will ultimately be returned in a structure
    char* shaderSource[2] = { malloc(sizeof(char)), malloc(sizeof(char)) };
    shaderSource[0][0] = '\0';
    shaderSource[0][1] = '\0';

    while( !feof(fp) )
    {
        // Read in a line of text
        fgets( line, buffer, fp );

        if( StringContains( line, buffer, "#|", 2 ) )
        {
            const char* vertexShaderStr = "|Vertex Shader|";
            const char* fragmentShaderStr = "|Fragment Shader|";

            if( StringContains( line, buffer, vertexShaderStr, strlen(vertexShaderStr) ) )
            {
                type = VERTEX;
            }
            else if( StringContains( line, buffer, fragmentShaderStr, strlen(fragmentShaderStr) ) )
            {
                type = FRAGMENT;
            }
        }
        else
        {
            if( type >= 0 )
            {
                // Add on the line to the shader source. Include length of string to have null terminating character
                shaderSource[type] = realloc( shaderSource[type], sizeof(char) * (strlen(shaderSource[type]) + strlen(line) + 0) );
                strcat( shaderSource[type], line );
            }
            else
            {
                printf("The shader type was unable to be determined! Source file read: %s\n", fileLocation);
            }
        }
    }

    // Capture the return value before freeing the shaderSource buffer memeory
    struct ShaderProgramSource shaderProgramSource = { shaderSource[VERTEX], shaderSource[FRAGMENT] };

    free( shaderSource[VERTEX] );
    free( shaderSource[FRAGMENT] );

    return shaderProgramSource;
}

void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource )
{
    printf("\n------------VERTEX SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->vertexShaderSource);

    printf("\n------------FRAGMENT SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->fragmentShaderSource);
}





