
#include "camera.h"
#include "common.h"

void CreateCamera( struct Camera* camera, struct Vector3f position, struct Vector3f target )
{
    LOG_CHECKPOINT("Creating Camera");

    camera->position = position;
    camera->target = target;

    camera->front = { 0, 0, -1.0f }; // Vector pointing in direction of the front of the camera (camera points down -Z)
    camera->worldUp = { 0, 1.0f, 0 };

    struct Vector3f temp = SubtractVector3f( &camera->position, &camera->target );
    struct Vector3f camera_direction = NormalizeVector3f( &temp );
    temp = CrossVector3f( &camera->worldUp, &camera_direction );
    camera->right = NormalizeVector3f( &temp );
    camera->up = CrossVector3f( &camera_direction, &camera->right );

    camera->yaw = -90.0f;
    camera->roll = 0.0f;
    camera->pitch = 0.0f;

    camera->zoom = 45.0f;

    camera->enabled = 1;

    LOG_CHECKPOINT("Camera Created");
}

void PlaceCamera( struct Camera* camera, struct Vector3f position, struct Vector3f target )
{
    camera->position = position;
    camera->target = target;
}

void LockCamera( struct Camera* camera, struct Vector3f position, struct Vector3f target )
{
    PlaceCamera( camera, position, target );
    DisableCamera( camera );
}

void OrientateCamera( struct Camera* camera, float yaw, float roll, float pitch )
{
    camera->yaw = yaw;
    camera->roll = roll;
    camera->pitch = pitch;
}

void DisableCamera( struct Camera* camera )
{
    camera->enabled = 0;
    //glfwSetInputMode( windowState->window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
}

void EnableCamera( struct Camera* camera )
{
    camera->enabled = 1;
    //glfwSetInputMode( windowState->window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
}

void DestroyCamera( struct Camera* camera )
{
    LOG_CHECKPOINT("Destroying Camera");

    camera->enabled = 0;

    LOG_CHECKPOINT("Camera Destroyed");
}

