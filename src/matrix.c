
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdio.h>
#include <math.h> // TODO: Implement own function (use own library)

#include "matrix.h"

static inline void PrintDashGap( int numDashes )
{
    for( int i = 0; i < numDashes; i++ )
    {
        printf("-");
    }
    printf("\n");
}

static inline void SwapFloatValues( float* val1, float* val2 )
{
    float temp = *val1;
    *val1 = *val2;
    *val2 = temp;
}

void PrintVector3f( struct Vector3f* vector )
{
    PrintDashGap( 20 );
    for( int i = 0; i < 3; i++ )
    {
        printf("%f\t", vector->components[i]);
    }
    printf("\n");
    PrintDashGap( 20 );
}

void PrintMatrix4f( struct Matrix4f* matrix )
{
    PrintDashGap( 20 );
    for( int i = 0; i < 4; i++ )
    {
        for( int j = 0; j < 4; j++ )
        {
            printf("%f\t", matrix->elements[i][j]);
        }
        printf("\n");
    }
    PrintDashGap( 20 );
}

struct Matrix4f ComposeVector3sInMatrix4f( struct Vector3f* col1, struct Vector3f* col2, 
                                           struct Vector3f* col3, struct Vector3f* col4 )
{
    struct Matrix4f result;
    SetIdentityMatrix4f( &result );

    if( col1 )
        for( int i = 0; i < 3; i++ )
            result.elements[i][0] = col1->components[i];

    if( col2 )
        for( int i = 0; i < 3; i++ )
            result.elements[i][1] = col2->components[i];

    if( col3 )
        for( int i = 0; i < 3; i++ )
            result.elements[i][2] = col3->components[i];

    if( col4 )
        for( int i = 0; i < 3; i++ )
            result.elements[i][3] = col4->components[i];
    
    return result;
}

void TransposeMatrix4f( struct Matrix4f* matrix )
{
    for( int i = 0; i < 4; i++ )
    {
        for( int j = i + 1; j < 4; j++ )
        {
            SwapFloatValues( &matrix->elements[i][j], &matrix->elements[j][i] );
        }
    }
}

void RowMajorToColumnMajorMatrix4f( struct Matrix4f* matrix )
{
    TransposeMatrix4f( matrix );
}

float LengthOfVector3f( const struct Vector3f* vector )
{
    return sqrtf( powf( vector->components[0], 2 ) + powf( vector->components[1], 2 ) + powf( vector->components[2], 2 ) );
}

float DotVector3f( struct Vector3f* current, struct Vector3f* second )
{
    float result = 0.0f;
    for( int i = 0; i < 3; i++ )
    {
        result += current->components[i] * second->components[i];
    }
    return result;
}

struct Vector3f CrossVector3f( struct Vector3f* current, struct Vector3f* second )
{
    struct Vector3f result;

    for( int i = 0; i < 3; i++ )
    {
        result.components[i] = current->components[ (i+1) % 3 ] * second->components[ (i+2) % 3 ] - 
                    current->components[ (i+2) % 3 ] * second->components[ (i+1) % 3 ];
    }

    return result;
}

struct Vector3f NormalizeVector3f( const struct Vector3f* vector )
{
    float vector_length = LengthOfVector3f( vector );
    return (struct Vector3f) { vector->components[0] / vector_length,
                               vector->components[1] / vector_length,
                               vector->components[2] / vector_length };
}

struct Vector3f SubtractVector3f( const struct Vector3f* vector1, const struct Vector3f* vector2 )
{
    struct Vector3f result;

    for( int i = 0; i < 3; i++ )
    {
        result.components[i] = vector1->components[i] - vector2->components[i];
    }

    return result;
}

struct Vector3f ScalarMultiplyVector3f( struct Vector3f* vector, float scalar )
{
    struct Vector3f result;

    for( int i = 0; i < 3; i++ )
    {
        result.components[i] = vector->components[i] * scalar;
    }

    return result;
}

struct Vector3f AddVector3f( struct Vector3f* vector1, struct Vector3f* vector2 )
{
    struct Vector3f result;

    for( int i = 0; i < 3; i++ )
    {
        result.components[i] = vector1->components[i] + vector2->components[i];
    }

    return result;
}

void SetIdentityMatrix4f( struct Matrix4f* matrix )
{
    // Setting row by row
    for( int i = 0; i < 4; i++ )
    {
        for( int j = 0; j < 4; j++ )
        {
            if( i == j )
            {
                matrix->elements[i][j] = 1.0f;
            }
            else
            {
                matrix->elements[i][j] = 0.0f;
            } 
        }
    }
}

void SetZeroMatrix4f( struct Matrix4f* matrix )
{
    // Setting row by row
    for( int i = 0; i < 4; i++ )
    {
        for( int j = 0; j < 4; j++ )
        {
            matrix->elements[i][j] = 0.0f;
        }
    }
}

void ScalarMultiplyMatrix4f( struct Matrix4f* matrix, float scalar )
{
    for( int i = 0; i < 4; i++ )
    {
        for( int j = 0; j < 4; j++ )
        {
            matrix->elements[i][j] *= scalar;
        }
    }
}

void ScalarTransformMatrix4f( struct Matrix4f* matrix, float scalar )
{
    // All of the diagonal, but not last column which handles translation (element 4 specifically indicates position/direction)
    for( int i = 0; i < 3; i++ )
    {
        matrix->elements[i][i] *= scalar;
    }
}

struct Matrix4f MultiplyMatrix4fMatrix4f( const struct Matrix4f* matrix1, const struct Matrix4f* matrix2 )
{
    struct Matrix4f result;

    for( int i = 0; i < 4; i++ )
    {
        for( int j = 0; j < 4; j++ )
        {
            result.elements[i][j] = 0;
            for( int k = 0; k < 4; k++ )
            {
                result.elements[i][j] += matrix1->elements[i][k] * matrix2->elements[k][j];
            }
        }
    }

    return result;
}

static struct Matrix4f CreateScaleMatrix4f( float scale )
{
    struct Matrix4f scale_matrix;
    SetIdentityMatrix4f( &scale_matrix );

    scale_matrix.elements[0][0] *= scale;
    scale_matrix.elements[1][1] *= scale;
    scale_matrix.elements[2][2] *= scale;

    return scale_matrix;
}

static inline struct Matrix4f CreateXAxis3DRotationMatrix4f( float rx )
{
    struct Matrix4f x_axis_rotation_matrix;
    SetIdentityMatrix4f( &x_axis_rotation_matrix );

    x_axis_rotation_matrix.elements[1][1] = cos( rx );
    x_axis_rotation_matrix.elements[1][2] = -sin( rx );
    x_axis_rotation_matrix.elements[2][1] = sin( rx );
    x_axis_rotation_matrix.elements[2][2] = cos( rx );

    return x_axis_rotation_matrix;
}

static inline struct Matrix4f CreateYAxis3DRotationMatrix4f( float ry )
{
    struct Matrix4f y_axis_rotation_matrix;
    SetIdentityMatrix4f( &y_axis_rotation_matrix );

    y_axis_rotation_matrix.elements[0][0] = cos( ry );
    y_axis_rotation_matrix.elements[0][2] = sin( ry );
    y_axis_rotation_matrix.elements[2][0] = -sin( ry );
    y_axis_rotation_matrix.elements[2][2] = cos( ry );

    return y_axis_rotation_matrix;
}

static inline struct Matrix4f CreateZAxis3DRotationMatrix4f( float rz )
{
    struct Matrix4f z_axis_rotation_matrix;
    SetIdentityMatrix4f( &z_axis_rotation_matrix );

    z_axis_rotation_matrix.elements[0][0] = cos( rz );
    z_axis_rotation_matrix.elements[0][1] = -sin( rz );
    z_axis_rotation_matrix.elements[1][0] = sin( rz );
    z_axis_rotation_matrix.elements[1][1] = cos( rz );

    return z_axis_rotation_matrix;
}

struct Matrix4f CreateRotationMatrix4f( float rx, float ry, float rz )
{
    struct Matrix4f rotation_matrix;
    SetIdentityMatrix4f( &rotation_matrix );

    const struct Matrix4f z_axis_rotation = CreateZAxis3DRotationMatrix4f( rz );
    const struct Matrix4f y_axis_rotation = CreateYAxis3DRotationMatrix4f( ry );
    const struct Matrix4f x_axis_rotation = CreateXAxis3DRotationMatrix4f( rx );
    struct Matrix4f temp = MultiplyMatrix4fMatrix4f( &y_axis_rotation, &x_axis_rotation );
    rotation_matrix = MultiplyMatrix4fMatrix4f( &z_axis_rotation, &temp );

    return rotation_matrix;
}

struct Matrix4f CreateTranslationMatrix4f( struct Vector3f* translation )
{
    struct Matrix4f translation_matrix;
    SetIdentityMatrix4f( &translation_matrix );

    for( int i = 0; i < 3; i++ )
        translation_matrix.elements[i][3] = translation->components[i];

    return translation_matrix;
}

struct Matrix4f CreateTransformationMatrix4f( struct Vector3f* translation, 
                                              float rx_deg, float ry_deg, float rz_deg,
                                              float scale )
{
    struct Matrix4f transformation_matrix;

    struct Matrix4f scale_matrix = CreateScaleMatrix4f( scale );
    struct Matrix4f rotation_matrix = CreateRotationMatrix4f( RADIANS( rx_deg ), RADIANS( ry_deg ), RADIANS( rz_deg ) );
    struct Matrix4f translation_matrix = CreateTranslationMatrix4f( translation );

    // TODO: OPTIMIZE, just work out the math component wise and set directly... I don't want the CPU doing matrix multiplication
    // Order should be (right to left): translation * rotation * scale
    struct Matrix4f temp = MultiplyMatrix4fMatrix4f( &rotation_matrix, &scale_matrix );
    transformation_matrix = MultiplyMatrix4fMatrix4f( &translation_matrix, &temp );

    return transformation_matrix;
}

struct Matrix4f CreateViewMatrix4f( struct Vector3f* eyePosition, struct Vector3f* eyeTarget, struct Vector3f* eyeUp )
{
    struct Matrix4f view_matrix;
    SetIdentityMatrix4f( &view_matrix );

    // NOTE: Forward/Left/Up are defined relative to target's position with "forward" directed towards camera
    // Forward vector is unit vector from the target position towards the eyePosition (camera)
    struct Vector3f forward = SubtractVector3f( eyePosition, eyeTarget ); // Z axis
    forward = NormalizeVector3f( &forward );

    // Left vector is unit vector that is cross of eyeUp and forward -> "Left" from target's POV, "Right" from camera's
    struct Vector3f left = CrossVector3f( eyeUp, &forward ); // X axis
    left = NormalizeVector3f( &left );

    // Up vector is unit vector that is cross of forward and left
    struct Vector3f up = CrossVector3f( &forward, &left ); // Y axis

    // Each of the first 3 columns of the view matrix is just the left/up/forward from the rotation matrix
    for( int i = 0; i < 3; i++ )
        view_matrix.elements[0][i] = left.components[i];

    for( int i = 0; i < 3; i++ )
        view_matrix.elements[1][i] = up.components[i];

    for( int i = 0; i < 3; i++ )
        view_matrix.elements[2][i] = forward.components[i];

    // Last column of view matrix is dot product between transposed rotation vectors into translation vector
    view_matrix.elements[0][3] = -DotVector3f( &left, eyePosition );
    view_matrix.elements[1][3] = -DotVector3f( &up, eyePosition );
    view_matrix.elements[2][3] = -DotVector3f( &forward, eyePosition );

    return view_matrix;
}

static struct Matrix4f CreateFrustrumMatrix4f( float left, float right, float bottom, float top, float near, float far )
{
    struct Matrix4f frustrum;
    SetZeroMatrix4f( &frustrum );

    frustrum.elements[0][0] = (2 * near) / (right - left);
    frustrum.elements[0][2] = (right + left) / (right - left);

    frustrum.elements[1][1] = (2 * near) / (top - bottom);
    frustrum.elements[1][2] = (top + bottom) / (top - bottom);

    frustrum.elements[2][2] = (far + near) / (near - far);
    frustrum.elements[2][3] = (2 * near * far) / (near - far);

    frustrum.elements[3][2] = -1;

    return frustrum;
}

static struct Matrix4f CreateOrthoFrustrumMatrix4f( float left, float right, float bottom, float top, float near, float far )
{
    struct Matrix4f frustrum;
    SetZeroMatrix4f( &frustrum );

    frustrum.elements[0][0] = 2 / (right - left);
    frustrum.elements[0][3] = (right + left) / (left - right);

    frustrum.elements[1][1] = 2 / (top - bottom);
    frustrum.elements[1][3] = (top + bottom) / (bottom - top);

    frustrum.elements[2][2] = 2 / (near - far);
    frustrum.elements[2][3] = (far + near) / (near - far);

    frustrum.elements[3][3] = 1;

    return frustrum;
}

struct Matrix4f CreatePerspectiveMatrix4f( float fieldOfView, float aspectRatio, float nearPlaneDist, float farPlaneDist )
{
    float height = nearPlaneDist * tan( RADIANS(fieldOfView) / 2 );
    float width = height * aspectRatio; // aspectRatio = width / height

    return CreateFrustrumMatrix4f( -width, width, -height, height, nearPlaneDist, farPlaneDist );
}

struct Matrix4f CreateOrthographicMatrix4f( float left, float right, float bottom, float top, float near, float far )
{
    return CreateOrthoFrustrumMatrix4f( left, right, bottom, top, near, far );
}









