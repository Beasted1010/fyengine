
#include "events.h"


void HandleWidgetEvent( struct Widget* widget, EventType type, struct Input* input )
{
    switch( type )
    {
        case etMOUSE_MOVE_EVENT:
        {
            SetWidgetState( widget, scsHOVERED );
        } break;

        case etMOUSE_CLICK_EVENT:
        {
            switch( input->type )
            {
                case pitPRESS:
                {
                    SetWidgetState( widget, scsPRESSED );
                } break;

                case pitRELEASE:
                {
                    // TODO: Currently this seems to happen too fast leaving the user having to set state to scsNONE after they handle it. Do I want?
                    //SetWidgetState( widget, scsNONE );
                } break;

                default:
                {
                    //SetWidgetState( widget, scsNONE );
                } break;
            }
        } break;

        case etKEY_PRESS_EVENT:
        {
        } break;

        default:
        {
            //SetWidgetState( widget, scsNONE );
            SetWidgetState( widget, scsHOVERED );
        } break;
    }
}

void HandleOverlayEvent( struct Overlay* overlay, EventType type, struct Input* input )
{
    for( int i = 0; i < overlay->widgetCount; i++ )
    {
        if( MouseInWidget( &input->mouse, &overlay->widgets[i] ) )
        {
            HandleWidgetEvent( &overlay->widgets[i], type, input );
        }
        else
        {
            //HandleWidgetEvent( &overlay->widgets[i], etNONE, input );
            SetWidgetState( &overlay->widgets[i], scsNONE );
        }
    }
}





// TODO: This would ultimatley be user defined, meaning not in any engine file but in a user's file
void OnMouseMove( void* data )
{
}

void OnMouseClick( void* data )
{
}

void OnKeyPress( void* data )
{
}




