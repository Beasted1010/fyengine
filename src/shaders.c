
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>

//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>

#include "shaders.h"
#include "matrix.h"
#include "common.h"

// Control the use of our shader program
void UseProgram( unsigned int shaderProgram )
{
    glUseProgram( shaderProgram );
}

void StopProgram()
{
    glUseProgram( 0 );
}

void DestroyShaderProgram( struct ShaderProgram* shaderProgram )
{
    StopProgram();
    glDetachShader( shaderProgram->id, shaderProgram->vertexShaderID );
    glDetachShader( shaderProgram->id, shaderProgram->fragmentShaderID );
    glDeleteShader( shaderProgram->vertexShaderID );
    glDeleteShader( shaderProgram->fragmentShaderID );
    glDeleteProgram( shaderProgram->id );
}

void FreeShaderProgramSource( struct ShaderProgramSource* shaderProgramSource )
{
    free( shaderProgramSource->vertexShaderSource );
    free( shaderProgramSource->fragmentShaderSource );
}

/*BindAttributeLocation( shaderProgram.id, 0, "position" );
BindAttributeLocation( shaderProgram.id, 1, "textureCoords" );*/
void BindAttributeLocation( unsigned int shaderProgramID, unsigned int genericVertexAttribIndexToBind, const char* attribName )
{
    // Binds a unifrom variable from our code to the shader code -> Allows us to pass information to shader
    // Essentially it pairs a generic vertex attribute (location of some attribute in shader) with a name
    glBindAttribLocation( shaderProgramID, genericVertexAttribIndexToBind, attribName );
}

void BindAttributes( unsigned int shaderProgramID )
{
    // TODO: Need to add normals at some point
    BindAttributeLocation( shaderProgramID, 0, "position" );
    BindAttributeLocation( shaderProgramID, 1, "textureCoords" );
}

void LoadFloat( int location, float value )
{
    glUniform1f( location, value );
}

void LoadInteger( int location, int value )
{
    glUniform1i( location, value );
}

void LoadBoolean( int location, unsigned short value )
{
    glUniform1i( location, value );
}

void LoadColor( unsigned int* shaderID, const char* uniformName, struct RGB* color )
{
    // Normalize values since that is what the vec3 uniform expects (values between 0 and 1)
    glUniform3f( glGetUniformLocation( *shaderID, uniformName ), 
                 (color->red / (float) UCHAR_MAX ),
                 (color->green / (float) UCHAR_MAX ),
                 (color->blue / (float) UCHAR_MAX ) );
}

void LoadVector3f( int location, struct Vector3f* vector )
{
    glUniform3f( location, vector->components[M_X], vector->components[M_Y], vector->components[M_Z] );
}

void LoadMatrix4f( int location, struct Matrix4f* matrix )
{
    // NOTE: Relying on a tightly packed matrix
    // NOTE: Relying on OpenGL to transpose so I may oblige by Linear Algebra matrix multiplication and row major array storage
    glUniformMatrix4fv( location, 1, GL_TRUE, (float*)matrix );
}

void LoadTransformationMatrix( int location, struct Matrix4f* matrix )
{
    LoadMatrix4f( location, matrix );
}

void LoadModelToWorldMatrix( int location, struct Matrix4f* matrix )
{
    LoadMatrix4f( location, matrix );
}

void LoadWorldToCameraMatrix( int location, struct Matrix4f* matrix )
{
    LoadMatrix4f( location, matrix );
}

void LoadCameraToClipMatrix( int location, struct Matrix4f* matrix )
{
    LoadMatrix4f( location, matrix );
}

void LoadMatrices( struct ShaderProgram* shaderProgram, struct Matrix4f* modelToWorld,  
                   struct Matrix4f* worldToCamera, struct Matrix4f* cameraToClip )
{
    UseProgram( shaderProgram->id );

    if( modelToWorld )
        LoadModelToWorldMatrix( shaderProgram->modelToWorldMatrixLocation, modelToWorld );

    if( worldToCamera )
        LoadWorldToCameraMatrix( shaderProgram->worldToCameraMatrixLocation, worldToCamera );

    if( cameraToClip )
        LoadCameraToClipMatrix( shaderProgram->cameraToClipMatrixLocation, cameraToClip );

    StopProgram();
}

void LoadScreenProjectionMatrix( unsigned int shaderProgramID, float screenWidth, float screenHeight )
{
    struct Matrix4f projection = CreateOrthographicMatrix4f( 0.0f, screenWidth, 
                                                             0.0f, screenHeight, 0.0f, 10.0f );

    UseProgram( shaderProgramID );
    glUniformMatrix4fv( glGetUniformLocation( shaderProgramID, "projection"), 1, GL_TRUE, (float*) &projection);
    UseProgram ( 0 );
}

void GetAllUniformLocations( struct ShaderProgram* shaderProgram )
{
    shaderProgram->modelToWorldMatrixLocation = glGetUniformLocation( shaderProgram->id, "modelToWorld" );
    shaderProgram->worldToCameraMatrixLocation = glGetUniformLocation( shaderProgram->id, "worldToCamera" );
    shaderProgram->cameraToClipMatrixLocation = glGetUniformLocation( shaderProgram->id, "cameraToClip" );

    shaderProgram->transformationMatrixLocation = glGetUniformLocation( shaderProgram->id, "transformationMatrix" );
}

// Can compile either a vertex shader or a fragment shader
static unsigned int CompileShader( const char* shaderSource, enum ShaderType type )
{
    // Create the shader
    unsigned int shader = glCreateShader( type == SHADER_TYPE_VERTEX ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER );
    glShaderSource( shader, 1, &shaderSource, NULL );

    // Compile the source
    // TODO: Currently There is a problem with the Fragment shader? (illegal character)
    // TODO This happens occasionally, one time "make" leads to it working mostly fine, another "make" and it frequently fails
    glCompileShader(shader);

    int success;
    glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
    if( !success )
    {
        char infoLog[512];
        glGetShaderInfoLog( shader, 512, NULL, infoLog );
        printf("ERROR::SHADER::%s::COMPILATION_FAILED -> %s\n", type == SHADER_TYPE_VERTEX ? "VERTEX" : "FRAGMENT", infoLog);
    }

    return shader;
}

static int StringContains( const char* str, int strLen, const char* fragStr, int fragLen )
{
    for( int i = 0; i < strLen; i++ )
    {
        if( str[i] == fragStr[0] )
        {
            // str[i] == fragStr[0] so move to the next character for comparison
            int strLoc = i+1;
            // Loop through str and fragStr and compare each character
            int fragLoc = 1;
            for( ; fragLoc < fragLen && strLoc < strLen; fragLoc++, strLoc++ )
            {
                // If they differ at any spot, we do not have a match, keep going
                if( str[strLoc] != fragStr[fragLoc] )
                {
                    break;
                }
            }

            // If we ended with the fragment string location being the same as the fragment string length, then we have a match
            if( fragLoc == fragLen )
            {
                return 1;
            }
        }
    }
    return 0;
}

// NOTE: This function allocates memory that it itself doesn't free. Currently FreeShaderProgramSource does so.
// NOTE: This is because it seems the shader program source needs to be in memory when the shaders are linked
static struct ShaderProgramSource ReadShaderFile( const char* fileLocation )
{
    const int buffer_size = 255;
    enum ShaderType type = SHADER_TYPE_NONE;

    FILE* fp = fopen( fileLocation, "r" );

    ValidateObject( fp, "File Pointer to Shader" );

    // Skip any leading white space
    while( isspace( fgetc(fp) ) ) ;

    char line[buffer_size];

    // The shader source buffer that will ultimately be returned in a structure
    char* shaderSource[2] = { (char*) malloc(sizeof(char)), (char*) malloc(sizeof(char)) };
    shaderSource[SHADER_TYPE_VERTEX][0] = '\0';
    shaderSource[SHADER_TYPE_FRAGMENT][0] = '\0';

    while( !feof(fp) )
    {
        // Read in a line of text
        // TODO: Use my new utility function to read a line? while( ReadLine(...) )
        fgets( line, buffer_size, fp );

        if( StringContains( line, buffer_size, "#|", 2 ) )
        {
            const char* vertexShaderStr = "|Vertex Shader|";
            const char* fragmentShaderStr = "|Fragment Shader|";

            if( StringContains( line, buffer_size, vertexShaderStr, strlen(vertexShaderStr) ) )
            {
                type = SHADER_TYPE_VERTEX;
            }
            else if( StringContains( line, buffer_size, fragmentShaderStr, strlen(fragmentShaderStr) ) )
            {
                type = SHADER_TYPE_FRAGMENT;
            }
        }
        else if( type != SHADER_TYPE_NONE )
        {
            if( type >= 0 )
            {
                // Add on the line to the shader source. Include length of string to have null terminating character
                shaderSource[type] = (char*) realloc( shaderSource[type],   // Current              // New    // \0
                                                      sizeof(char) * (strlen(shaderSource[type]) + strlen(line) + 1) );
                strcat( shaderSource[type], line );
            }
            else
            {
                printf("The shader type was unable to be determined! Source file read: %s\n", fileLocation);
            }
        }
    }

    struct ShaderProgramSource shaderProgramSource = { shaderSource[SHADER_TYPE_VERTEX], shaderSource[SHADER_TYPE_FRAGMENT] };

    return shaderProgramSource;
}

// Suppress compiler warning with inline for unused function, could also use __atribute__((unused)) before "static" for GCC
static inline void PrintShaderSource( struct ShaderProgramSource* shaderProgramSource )
{
    printf("\n------------VERTEX SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->vertexShaderSource);

    printf("\n------------FRAGMENT SOURCE------------\n\n");
    printf("%s\n", shaderProgramSource->fragmentShaderSource);
}

// Creates both the vertex shader and the fragment shader
struct ShaderProgram CreateShaderProgram( const char* shaderFileLocation )
{
    LOG_CHECKPOINT("Creating Shader Program from Source: %s", shaderFileLocation);

    struct ShaderProgram shader_program;

    struct ShaderProgramSource shader_program_source = ReadShaderFile( shaderFileLocation );

#ifdef DEBUG_SHADERS
    PrintShaderSource( &shader_program_source );
#endif

    shader_program.vertexShaderID = CompileShader( shader_program_source.vertexShaderSource, SHADER_TYPE_VERTEX );
    shader_program.fragmentShaderID = CompileShader( shader_program_source.fragmentShaderSource, SHADER_TYPE_FRAGMENT );
    
    // Create the program, attach shaders, and then link the shaders to the single program
    shader_program.id = glCreateProgram();
    glAttachShader( shader_program.id, shader_program.vertexShaderID );
    glAttachShader( shader_program.id, shader_program.fragmentShaderID );
    BindAttributes( shader_program.id );
    glLinkProgram( shader_program.id );

    int success;
    glGetProgramiv( shader_program.id, GL_LINK_STATUS, &success );
    if( !success )
    {
        char infoLog[512];
        glGetProgramInfoLog(shader_program.id, 512, NULL, infoLog);
        printf("ERROR::PROGRAM::SHADERS::LINKING_FAILED -> %s\n", infoLog);
    }
    
    // Delete the shaders since they are now linked to the shader program (we don't need these references anymore)
    glDeleteShader( shader_program.vertexShaderID );
    glDeleteShader( shader_program.fragmentShaderID );

    FreeShaderProgramSource( &shader_program_source );

    GetAllUniformLocations( &shader_program );

    LOG_CHECKPOINT("Shader Program Created");
    return shader_program;
}






