
#include <stdio.h>
#include <stdlib.h>

#include <glm/glm.hpp>

#include "window.h"
#include "math.h" // TODO: Do myself
#include "matrix.h"
#include "common.h"

#include "overlays.h"
#include "events.h"

static void FramebufferSizeCallback(GLFWwindow* window, int width, int height);
//void MouseCallback( GLFWwindow* window, double xPos, double yPos ){}
void ScrollCallback( GLFWwindow* window, double xOffset, double yOffset ){}

struct WindowState CreateWindowState()
{
    LOG_CHECKPOINT("Creating Window State");

    struct WindowState state;
    state.deltaTime = 0.0f;
    state.lastFrame = 0.0f;

#ifdef GLFW
    state.window = GLFW_OpenGLWindowAndContextCreation();

    // Set the frambuffer size callback which will handle setting the viewport size
    glfwSetFramebufferSizeCallback(state.window, FramebufferSizeCallback);

    // Set our cursor movement function as the callback for glfw to handle our cursor movements
    //glfwSetCursorPosCallback( state.window, MouseCallback );
    // TODO: Should this be here?
    glfwSetInputMode( state.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED ); // Disables cursor being shown and captures it in window

    // Set the scroll-wheel event handling callback function for glfw to know how to handle our scroll events
    //glfwSetScrollCallback( state.window, ScrollCallback );
#else
    // TODO: ERROR
#endif

#ifdef GLEW
    GLEW_OpenGLLoadFunctions();
#else
    // TODO: ERROR
#endif

    double x_pos, y_pos;
    glfwGetCursorPos( state.window, &x_pos, &y_pos );

    state.input.mouse.xPos = x_pos;
    state.input.mouse.yPos = y_pos;
    state.input.mouse.lastXPos = x_pos;
    state.input.mouse.lastYPos = y_pos;

    state.input.mouse.sensitivity = MOUSE_SENSITIVITY;

    state.input.mouse.button = mbiNONE;

    state.input.keyboard.key = kitNONE;
    state.input.keyboard.repeat = 0;

    state.running = 1;


    LOG_INFO("Using OpenGL VERSION: %s", glGetString(GL_VERSION));
    LOG_CHECKPOINT("Window State Created");
    return state;
}

void UpdateWindowState( struct WindowState* state )
{
    float current_frame = glfwGetTime();
    state->deltaTime = current_frame - state->lastFrame;
    state->lastFrame = current_frame;

    glfwSwapBuffers( state->window );
    glfwPollEvents();
 
    if( glfwWindowShouldClose( state->window ) )
    {
        state->running = 0;
    }
}

void DestroyWindowState( struct WindowState* state )
{

#ifdef GLFW
    glfwTerminate(); // Clean/delete all of GLFW's resources that were allocated
#else
    // TODO: ERROR
#endif

}

GLFWwindow* GLFW_OpenGLWindowAndContextCreation()
{
    // Configure the window for API version 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // Specify client API version that created context must be compatible with
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); // This is the version after the .
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Create the window and the OpenGL context
    GLFWwindow* window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Hello World", NULL /*monitor*/, NULL /*share*/);
    if( !window )
    {
        printf("Failed to create a window!\n");
        glfwTerminate();
        exit(1);
    }

    // Make the OpenGL context current, this completes the creation of the OpenGL context
    glfwMakeContextCurrent(window); // Creates the valid OpenGL context

    return window;
}

void GLEW_OpenGLLoadFunctions()
{
    // Load all of the OpenGL functions
    if( glewInit() != GLEW_OK )
    {
        printf("Failed to initialize glew!\n");
    }
}

// This is needed because we need to update the viewport for OpenGL (the rendering area) every time the screen is resized
static void FramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0,0, width, height); // This specifies where all the OpenGL rendering is to be displayed
}

// REGION: HANDLE INPUT
// A function to handle input so that it is organized
// TODO: Rename this to something like "Process FYEngine input" or something... 
//       I want user to be able to create own input function and handle their own events
void ProcessInput( struct WindowState* windowState, struct Overlay* overlays, unsigned int overlayCount, struct Camera* camera )
{
    // TODO: Handle different API inputs, e.g. glfw should be #ifdef to allow things like windows input handling instead
    //       Or do I just want to get rid of glfw's input handling all together when I move to Windows API? -> PROBABLY
    
    // TODO: I want to resume my camera location where it left off once it was disabled. Currently this jolts around.
    //       I can save the cursor position once disabled and then glfwSetCursorPos with it later
    if( glfwGetKey(windowState->window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS && 
        glfwGetKey(windowState->window, GLFW_KEY_C) == GLFW_PRESS )
    {
        camera->enabled = !camera->enabled;

        if( camera->enabled )
        {
            // Disables cursor being shown and captures it in window
            glfwSetInputMode( windowState->window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
        }
        else
        {
            // Returns cursor state to original (cursor visible and free)
            glfwSetInputMode( windowState->window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
        }
    }

    if( (glfwGetKey(windowState->window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS) &&
        (glfwGetKey(windowState->window, GLFW_KEY_ESCAPE) == GLFW_PRESS) )
    {
        glfwSetWindowShouldClose(windowState->window, 1); // Second argument is 1 indicating true
    }

    // REGION: Handle keyboard input
    // TODO: A separate function to handle camera movement?
    // Handle the event that the escape key is pressed -> Close the window
    if( camera->enabled )
    {
        float camera_speed = 2.5f * windowState->deltaTime;
        
        if( glfwGetKey(windowState->window, GLFW_KEY_W) == GLFW_PRESS )
        {
            // Move forward
            struct Vector3f temp = ScalarMultiplyVector3f( &camera->front, camera_speed );
            camera->position = AddVector3f( &camera->position, &temp );
        }
        if( glfwGetKey(windowState->window, GLFW_KEY_S) == GLFW_PRESS )
        {
            // Move backwards
            struct Vector3f temp = ScalarMultiplyVector3f( &camera->front, camera_speed );
            camera->position = SubtractVector3f( &camera->position, &temp );
        }
        if( glfwGetKey(windowState->window, GLFW_KEY_A) == GLFW_PRESS )
        {
            // Create a right vector (with cross) and move to the left along this
            struct Vector3f temp = CrossVector3f( &camera->front, &camera->up );
            temp = NormalizeVector3f( &temp );
            temp = ScalarMultiplyVector3f( &temp, camera_speed );
            camera->position = SubtractVector3f( &camera->position, &temp );
        }
        if( glfwGetKey(windowState->window, GLFW_KEY_D) == GLFW_PRESS )
        {
            // Create a right vector (with cross) and move to the right along this
            struct Vector3f temp = CrossVector3f( &camera->front, &camera->up );
            temp = NormalizeVector3f( &temp );
            temp = ScalarMultiplyVector3f( &temp, camera_speed );
            camera->position = AddVector3f( &camera->position, &temp );
        }
    }

    // REGION: Handle mouse movement
    double x_pos, y_pos;
    glfwGetCursorPos( windowState->window, &x_pos, &y_pos );
    windowState->input.mouse.xPos = (float) x_pos;
    windowState->input.mouse.yPos = (float) y_pos;

    // Correct coordinates to line up with OpenGL's bottom left origin (glfw's window coord's origin is top left)
    windowState->input.mouse.yPos = SCREEN_HEIGHT - windowState->input.mouse.yPos;

    static int old_state = GLFW_RELEASE;
    int new_state = glfwGetMouseButton( windowState->window, GLFW_MOUSE_BUTTON_LEFT );
    if( new_state == GLFW_RELEASE && old_state == GLFW_PRESS )
    {
        windowState->input.mouse.button = mbiMOUSE_BUTTON_LEFT;
        windowState->input.type = pitPRESS;
    }
    else
    {
        windowState->input.mouse.button = mbiNONE;
        windowState->input.type = pitNONE;
    }
    old_state = new_state;

    for( unsigned int i = 0; i < overlayCount; i++ )
    {
        if( overlays[i].screenData.state == scsDEACTIVATED )
        {
            continue;
        }

        //HandleOverlayEvent( &overlays[i], etMOUSE_CLICK_EVENT, &windowState->input );
        if( MouseInOverlay( windowState->input.mouse.xPos, windowState->input.mouse.yPos, &overlays[i] ) )
        {
            //if( windowState->input.mouse.xPos != windowState->input.mouse.lastXPos )
                HandleOverlayEvent( &overlays[i], etMOUSE_MOVE_EVENT, &windowState->input );

            //else if( windowState->input.mouse.button == mbiMOUSE_BUTTON_LEFT ) // TODO: Make this cleaner...
                HandleOverlayEvent( &overlays[i], etMOUSE_CLICK_EVENT, &windowState->input );
        }
        else
        {
            HandleOverlayEvent( &overlays[i], etNONE, &windowState->input );
        }
    }

    if( camera->enabled && (windowState->input.mouse.xPos != windowState->input.mouse.lastXPos || 
                            windowState->input.mouse.yPos != windowState->input.mouse.lastYPos) )
    {
        float x_offset = windowState->input.mouse.xPos - windowState->input.mouse.lastXPos;
        float y_offset = windowState->input.mouse.yPos - windowState->input.mouse.lastYPos;

        windowState->input.mouse.lastXPos = windowState->input.mouse.xPos;
        windowState->input.mouse.lastYPos = windowState->input.mouse.yPos;

        x_offset *= windowState->input.mouse.sensitivity;
        y_offset *= windowState->input.mouse.sensitivity;

        camera->yaw += x_offset;
        camera->pitch += y_offset;

        if( camera->pitch > MAX_PITCH ) // Look at sky extreme
            camera->pitch = MAX_PITCH;
        if( camera->pitch < MIN_PITCH ) // Look at feet extreme
            camera->pitch = MIN_PITCH;
        
        struct Vector3f camera_direction;
        camera_direction.components[M_X] = cos( RADIANS(camera->pitch) ) * cos( RADIANS(camera->yaw) );
        camera_direction.components[M_Y] = sin( RADIANS(camera->pitch) );
        camera_direction.components[M_Z] = cos( RADIANS(camera->pitch) ) * sin( RADIANS(camera->yaw) );
        camera->front = NormalizeVector3f( &camera_direction );
    }

    windowState->input.mouse.lastXPos = windowState->input.mouse.xPos;
    windowState->input.mouse.lastYPos = windowState->input.mouse.yPos;
}



