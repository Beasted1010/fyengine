// TODO: Pretty up this file -> Make more efficient, use _t types

#include <stdlib.h>

#include "utilities.h"
#include "common.h"

int PowerInt( int base, int exponent )
{
    int result = 1;

    while( 1 )
    {
        if( exponent & 1 )
            result *= base;

        exponent >>= 1;

        if( !exponent )
            break;

        base *= base;
    }

    return result;
}

int StringCompare( const char* str1, const char* str2 )
{
    int str1_len = StringLength( str1 );
    int str2_len = StringLength( str2 );
    int stop_len = str1_len > str2_len ? str1_len : str2_len;

    for( int i = 0; i < stop_len; i++ )
    {
        if( str1[i] != str2[i] )
            // Positive if stopping character in str1 > that of str2, negative otherwise
            return (str1[i] - str2[i]);
    }

    return 0;
}

int StringContains( const char* str, const char* toCompare )
{
    int goal_len = StringLength( toCompare );

    for( int i = 0; i < goal_len; i++ )
    {
        if( str[i] != toCompare[i] )
            return 0;
    }

    return 1;
}

void StringReplaceN( const char* source, char* destination, int characterCountToReplace )
{
    while( *source && characterCountToReplace-- )
    {
        *(destination++) = *(source++);
    }
    *destination = '\0';
}

size_t StringLength( const char* str )
{
    const char* s;
    // Starting with first character, while we are not on a null character, count.
    for( s = str; *s; s++ ) ;
    // The end point - start point is the length
    return (s - str); // Does not include NULL character, I think
}

inline int IsDigit( const char c )
{
    return ( ( (c - '0') >= 0 ) && ( ('9' - c) >= 0 ) );
}

int CharToInt( const char c )
{
    if( !IsDigit( c ) )
    {
        // Char is not a digit
    }

    int result = c - '0';

    return result;
}

int StringToInt( const char* str, int strLen )
{
    int result = 0;

    for( int i = strLen - 1; i >= 0; i-- )
    {
        result += (str[i] - '0') * PowerInt( 10, ( (strLen - 1) - i ) );
    }

    return result;
}

float StringIntToPureFloat( const char* str, int strLen )
{
    float result = (float) StringToInt( str, strLen );

    return ( result / PowerInt( 10, strLen ) );
}

float StringToFloat( const char* str, int strLen )
{
    float result = 0;
    char* num_accum = 0;
    int decimal_point_found = 0;
    int negate = 1;
    int num_digits_before_decimal_point = 0;
    int num_digits_after_decimal_point = 0;

    int i = 0;
    if( str[0] == '-' )
    {
        i++;
        negate = -1;
    }

    for( ; i < strLen; i++ )
    {
        if( str[i] == '.' )
        {
            if( num_accum )
            {
                result = (float) StringToInt( num_accum, num_digits_before_decimal_point );
                free( num_accum );
                num_accum = 0;
            }

            decimal_point_found = 1;
            continue;
        }
        else if( !IsDigit( str[i] ) )
        {
            LOG_WARNING("String (%s) contains characters other than expected float characters! Found %c\n", str, str[i]);
            continue;
        }

        if( !decimal_point_found )
        {
            num_accum = (char*) realloc( num_accum, sizeof(char) * (num_digits_before_decimal_point + 1) );
            ValidateObject( num_accum, "integer part num_accum" );
            num_accum[num_digits_before_decimal_point++] = str[i];
        }
        else
        {
            num_accum = (char*) realloc( num_accum, sizeof(char) * (num_digits_after_decimal_point + 1) );
            ValidateObject( num_accum, "decimal part num_accum" );
            num_accum[num_digits_after_decimal_point++] = str[i];
        }
    }

    result += StringIntToPureFloat( num_accum, num_digits_after_decimal_point );
    result *= negate;

    free( num_accum );
    num_accum = 0;

    return result;
}

int SplitOn( const char* str, int strLen, const char splitOn, char*** out )
{
    if( *out )
    {
        // TODO: ERROR, can't have memory allocated yet
    }

    int num_components = 0, str_len = 0;

    (*out) = (char**) realloc( *out, sizeof(char*) * (num_components + 1) );
    ValidateObject( (*out), "New Component for String");
    (*out)[num_components] = 0;

    for( int i = 0; i < strLen; i++ )
    {
        if( str[i] == splitOn )
        {
            (*out)[num_components] = (char*) realloc( (*out)[num_components], sizeof(char) * (str_len + 1) );
            ValidateObject( (*out)[num_components], "New Character for Component");
            (*out)[num_components][str_len] = '\0';

            str_len = 0;
            num_components++;

            (*out) = (char**) realloc( *out, sizeof(char*) * (num_components + 1) );
            ValidateObject( (*out), "New Component for String");
            (*out)[num_components] = 0;
            continue;
        }

        (*out)[num_components] = (char*) realloc( (*out)[num_components], sizeof(char) * (str_len + 1) );
        ValidateObject( (*out)[num_components], "New Character for Component");
        (*out)[num_components][str_len++] = str[i];
    }

    if( !((*out)[0]) )
    {
        free( *out );
        return 0;
    }

    (*out)[num_components] = (char*) realloc( (*out)[num_components], sizeof(char) * (str_len + 1) );
    ValidateObject( (*out)[num_components], "New Character for Line");
    (*out)[num_components][str_len] = '\0';

    return ++num_components;
}

int ReadLine( FILE* fp, char splitOn, char*** out )
{
    if( *out )
    {
        // TODO: ERROR, can't have memory allocated yet
    }

    int curr, num_components = 0, str_len = 0;
    char c;

    (*out) = (char**) realloc( *out, sizeof(char*) * (num_components + 1) );
    ValidateObject( (*out), "New Component for Line");
    (*out)[num_components] = 0;

    while( (curr = fgetc(fp)) != '\n' && curr != EOF )
    {
        c = (char) curr;

        if( c == splitOn && splitOn )
        {
            (*out)[num_components] = (char*) realloc( (*out)[num_components], sizeof(char) * (str_len + 1) );
            ValidateObject( (*out)[num_components], "New Character for Line");
            (*out)[num_components][str_len] = '\0';

            str_len = 0;
            num_components++;

            (*out) = (char**) realloc( *out, sizeof(char*) * (num_components + 1) );
            ValidateObject( (*out), "New Component for Line");
            (*out)[num_components] = 0;
            continue;
        }

        (*out)[num_components] = (char*) realloc( (*out)[num_components], sizeof(char) * (str_len + 1) );
        ValidateObject( (*out)[num_components], "New Character for Line");
        (*out)[num_components][str_len++] = c;
    }

    if( !((*out)[0]) )
    {
        free( *out );
        return 0;
    }

    (*out)[num_components] = (char*) realloc( (*out)[num_components], sizeof(char) * (str_len + 1) );
    ValidateObject( (*out)[num_components], "New Character for Line");
    (*out)[num_components][str_len] = '\0';

    return ++num_components;
}

