
#include <stdlib.h>
#include<stdio.h>

#include "logger.h"


static void CreateProfiler( struct Profiler* profilerOut )
{
}

static void DestroyProfiler( struct Profiler* profiler )
{
}

static void CreateDebugger( struct Debugger* debuggerOut )
{
    CreateProfiler( &debuggerOut->profiler );
}

static void DestroyDebugger( struct Debugger* debugger )
{
    DestroyProfiler( &debugger->profiler );
}

void CreateLogger( struct Logger* loggerOut, unsigned char storeLogsFlag, unsigned char useDebuggerFlag )
{
    loggerOut->storeLogsFlag = storeLogsFlag;
    loggerOut->useDebuggerFlag = useDebuggerFlag;

    loggerOut->criticals = 0;
    loggerOut->numCriticals = 0;

    loggerOut->errors = 0;
    loggerOut->numErrors = 0;

    loggerOut->warnings = 0;
    loggerOut->numWarnings = 0;

    if( loggerOut->useDebuggerFlag )
    {
        CreateDebugger( &loggerOut->debugger );
    }

}

void FlushLogs( struct Logger* logger )
{
    if( logger->storeLogsFlag )
    {
        for( int i = 0; i < logger->numCriticals; i++ )
        {
            free( logger->criticals[i] );
        }
        free( logger->criticals );

        for( int i = 0; i < logger->numErrors; i++ )
        {
            free( logger->errors[i] );
        }
        free( logger->errors );

        for( int i = 0; i < logger->numWarnings; i++ )
        {
            free( logger->warnings[i] );
        }
        free( logger->warnings );
    }
}

void DestroyLogger( struct Logger* logger )
{
    FlushLogs( logger );

    if( logger->useDebuggerFlag )
    {
        DestroyDebugger( &logger->debugger );
    }
}

void PrintLogs( struct Logger* logger )
{
    SEPARATE;
    for( int i = 0; i < logger->numCriticals; i++ )
    {
        LOG_CRITICAL("%s", logger->criticals[i]);
    }

    SEPARATE;
    for( int i = 0; i < logger->numErrors; i++ )
    {
        LOG_ERROR("%s", logger->errors[i]);
    }

    SEPARATE;
    for( int i = 0; i < logger->numWarnings; i++ )
    {
        LOG_WARNING("%s", logger->warnings[i]);
    }
}




