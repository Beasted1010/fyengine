
#include <GL/glew.h> // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GL/gl.h>

#include <stdio.h>
#include <stdlib.h>

#include "loader.h"
#include "model.h"
#include "utilities.h"
#include "common.h"

//#define STB_IMAGE_IMPLEMENTATION // Must be defined in only a SINGLE file (has class definition of this header only file)
//                                    We want to compile this code once, and once only
#include <stb_image.h>


struct Loader CreateLoader()
{
    LOG_CHECKPOINT("Creating Loader");

    LOG_CHECKPOINT("Loader Created");
    return (struct Loader) { NULL, 0, NULL, 0, NULL, 0, NULL, 0 };
}

void DestroyLoader( struct Loader* loader )
{
    glDeleteVertexArrays( loader->numVAOs, loader->vaos );
    glDeleteBuffers( loader->numVBOs, loader->vbos );
    glDeleteBuffers( loader->numEBOs, loader->ebos );
    glDeleteTextures( loader->numTextures, loader->textures );

    free( loader->vaos );
    free( loader->vbos );
    free( loader->ebos );
    free( loader->textures );

    loader->numVAOs = 0;
    loader->numVBOs = 0;
    loader->numTextures = 0;

    loader->vaos = NULL;
    loader->vbos = NULL;
    loader->ebos = NULL;
}

void AddVAOToLoader( struct Loader* loader, unsigned int vaoID )
{
    loader->vaos = (unsigned int*) realloc( loader->vaos, sizeof(unsigned int) * (loader->numVAOs + 1) );
    loader->vaos[loader->numVAOs++] = vaoID;
}

void AddVBOToLoader( struct Loader* loader, unsigned int vboID )
{
    loader->vbos = (unsigned int*) realloc( loader->vbos, sizeof(unsigned int) * (loader->numVBOs + 1) );
    loader->vbos[loader->numVBOs++] = vboID;
}

void AddEBOToLoader( struct Loader* loader, unsigned int eboID )
{
    loader->ebos = (unsigned int*) realloc( loader->ebos, sizeof(unsigned int) * (loader->numEBOs + 1) );
    loader->ebos[loader->numEBOs++] = eboID;
}

void AddTextureToLoader( struct Loader* loader, unsigned int textureID )
{
    loader->textures = (unsigned int*) realloc( loader->textures, sizeof(unsigned int) * (loader->numTextures + 1) );
    loader->textures[loader->numTextures++] = textureID;
}

struct RawModel CreateRawModel( unsigned int vaoID, unsigned int vertexCount )
{
    /*struct RawModel model;
    model.vaoID = vaoID;
    model.vertexCount = vertexCount;*/

    return (struct RawModel) { vaoID, vertexCount };
}

unsigned int CreateVAO( struct Loader* loader )
{
    unsigned int vaoID;
    glGenVertexArrays( 1, &vaoID );
    AddVAOToLoader( loader, vaoID );

    glBindVertexArray( vaoID );
    return vaoID;
}

// Requires binding first, so make only visible to this file
static void StoreDataInAttributeList( struct Loader* loader, unsigned int attributeNumber, unsigned int numComponents,
                                      float* data, unsigned int numElements )
{
    unsigned int vboID;
    glGenBuffers( 1, &vboID );
    glBindBuffer( GL_ARRAY_BUFFER, vboID );
    AddVBOToLoader( loader, vboID );

    // Creates and initialized a buffer object's data store -> Loads our data to the GPU
    // (
    //  Type of buffer to copy data to,
    //  Size in bytes of data object's new data store,
    //      data store = the memory we have in GPU?
    //  A pointer to data that is to be copied into the data store for initialization (our data),
    //  The expected usage pattern of data store (how will our data be used? how often?)
    // )
    glBufferData( GL_ARRAY_BUFFER, sizeof(float) * numElements, data, GL_STATIC_DRAW );

    // (
    //  index: Index of generic vertex attribute to be modified
    //      generic vertex attribute = Location specified in shader for some attribute (i.e. location = #, or what linker chose)
    //      Each vertex shader input variable is associated with an attribute index (set with location = # or chosen by linker)
    //          In either case you can find this location with glGetAttribLocation
    //  size: Specifies the number of components per generic vertex attribute (must be 1,2,3, or 4)
    //  type: Specifies the type of each component in the array (e.g. GL_FLOAT)
    //  normalize?: should the data be normalized?
    //  stride: the byte offset between consecutive generic vertex attributes -> 0 = have OpenGL figure it out (tightly packed)
    //  pointer: a pointer to the first generic vertex attribute in the array (essentially an offset to where this attribute is)
    //      i.e. which generic vertex attribute does this data correspond to in our vertex buffer object
    // )
    // We are using 0 for stride so that OpenGL figures it out.
    // We have 0 for pointer to start at beginning of data (since our data is at the front of our NEW VBO that we created)
    //glVertexAttribPointer( attributeNumber, numComponents, GL_FLOAT, GL_FALSE, 0, 0 );
    glVertexAttribPointer( attributeNumber, numComponents, GL_FLOAT, GL_FALSE, 0, 0 ); // TODO: Doing normalization for 3D Sudoku? Internally though?

    glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

static void BindIndicesBuffer( struct Loader* loader, unsigned int* indices, unsigned int numElements )
{
    unsigned int eboID;
    glGenBuffers( 1, &eboID );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, eboID );
    AddEBOToLoader( loader, eboID );

    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * numElements, indices, GL_STATIC_DRAW );

    // Don't unbind EBO while VAO is active!
}

static void UnbindVAO()
{
    glBindVertexArray(0);
}

// TODO: Wherever I call something "vertices" I typically mean "positions". I need to change this
struct RawModel LoadToVAO( struct Loader* loader, float* vertices, unsigned int numVertices, 
                                                  unsigned int* indices, unsigned int numIndices,
                                                  float* textureCoords, unsigned int numTexCoords )
{
    unsigned int vaoID = CreateVAO( loader );

    StoreDataInAttributeList( loader, 0, 3, vertices, numVertices ); // In attribute index 0
    StoreDataInAttributeList( loader, 1, 2, textureCoords, numTexCoords ); // In attribute index 1
    BindIndicesBuffer( loader, indices, numIndices );

    UnbindVAO();
    return (struct RawModel) { vaoID, numIndices }; // numIndices = number of unique vertices
}

unsigned int LoadTexture( struct Loader* loader, const char* fileLocation )
{
    unsigned int textureID;

    int width, height, numChannels;
    // NOTE: I am not flipping vertically on load to keep texture coordinates as expected (and speed this up)
    //       The read image is bottom down, so I will need to do 1 - yCoord when assigning to texture coord
    unsigned char* data = stbi_load( fileLocation, &width, &height, &numChannels, 3 ); // Wanting 3 channels

    if( !data )
    {
        // ERROR LOADING DATA
        printf("ERROR: Loading the texture data from file %s failed!", fileLocation);
    }

    glGenTextures( 1, &textureID );
    glBindTexture( GL_TEXTURE_2D, textureID );

    // NOTE: Setup texture settings here
    // TODO: DO I need? Apparently these are required ? Chrono youtube guy says so
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

    // Most GPUs seem to convert RGB to RGBA8 anyways (recommended to have explit 8 specified)
    // It is best for Windows machines to have BGRA as internal format, so future IMPROVEMENT
    // https://www.khronos.org/opengl/wiki/Common_Mistakes#Texture_upload_and_pixel_reads
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data );
    glGenerateMipmap( GL_TEXTURE_2D );

    stbi_image_free( data );

    AddTextureToLoader( loader, textureID );

    return textureID;
}

struct RawModel LoadOBJModel( struct Loader* loader, const char* fileLocation )
{
    struct RawModel result;

    FILE* fp = fopen( fileLocation, "r" );

    ValidateObject( fp, "OBJ file pointer" );

    char** line = 0;

    int num_components;

    // TODO: "vertices" as i use it here isn't really "vertices", but position. The position with normal and tex coord is vertex
    float** read_vertices = 0;
    int num_read_vertices = 0;

    float** read_normals = 0;
    int num_read_normals = 0;

    float** read_tex_coords = 0;
    int num_read_tex_coords = 0;

    float* vertices = 0;
    unsigned int num_vertex_floats = 0;

    float* tex_coords = 0;
    unsigned int num_tex_floats = 0;

    float* normals = 0;
    unsigned int num_normal_floats = 0;

    unsigned int* indices = 0;
    unsigned int num_index_ints = 0;

    while( ( num_components = ReadLine( fp, ' ', &line ) ) )
    {
        if( !StringCompare( line[0], "v" ) )
        {
            if( num_components != 4 )
            {
                LOG_WARNING("String for vertices does not contain 4 components!\n");
                continue;
            }

            read_vertices = (float**) realloc( read_vertices, sizeof(float*) * (num_read_vertices + 1) );
            read_vertices[num_read_vertices] = (float*) malloc( sizeof(float) * 3 );
            read_vertices[num_read_vertices][0] = StringToFloat( line[1], StringLength( line[1] ) );
            read_vertices[num_read_vertices][1] = StringToFloat( line[2], StringLength( line[2] ) );
            read_vertices[num_read_vertices][2] = StringToFloat( line[3], StringLength( line[3] ) );
            num_read_vertices++;
        }
        else if( !StringCompare( line[0], "vt" ) )
        {
            if( num_components != 3 )
            {
                LOG_WARNING("String for texture coordinates does not contain 3 components!\n");
                continue;
            }

            read_tex_coords = (float**) realloc( read_tex_coords, sizeof(float*) * (num_read_tex_coords + 1) );
            read_tex_coords[num_read_tex_coords] = (float*) malloc( sizeof(float) * 2 );
            read_tex_coords[num_read_tex_coords][0] = StringToFloat( line[1], StringLength( line[1] ) );
            read_tex_coords[num_read_tex_coords][1] = StringToFloat( line[2], StringLength( line[2] ) );
            num_read_tex_coords++;
        }
        else if( !StringCompare( line[0], "vn" ) )
        {
            if( num_components != 4 )
            {
                LOG_WARNING("String for normals does not contain 4 components!\n");
                continue;
            }

            read_normals = (float**) realloc( read_normals, sizeof(float*) * (num_read_normals + 1) );
            read_normals[num_read_normals] = (float*) malloc( sizeof(float) * 3 );
            read_normals[num_read_normals][0] = StringToFloat( line[1], StringLength( line[1] ) );
            read_normals[num_read_normals][1] = StringToFloat( line[2], StringLength( line[2] ) );
            read_normals[num_read_normals][2] = StringToFloat( line[3], StringLength( line[3] ) );
            num_read_normals++;
        }
        else if( !StringCompare( line[0], "f" ) )
        {
            if( num_components != 4 )
            {
                LOG_WARNING("String for normals does not contain 4 components!\n");
                continue;
            }

            char** str = 0;
            for( int i = 1; i < num_components; i++ )
            {
                int num_face_components = SplitOn( line[i], StringLength( line[i] ), '/', &str );

                if( num_face_components != 3 )
                {
                    // TODO: ERROR
                    LOG_ERROR("There are less than 3 face components for face vertices in '%s' OBJ file!", fileLocation);
                    continue;
                }

                for( int j = 0; j < num_face_components; j++ )
                {
                    if( StringCompare( str[j], "\0" ) ) // str[j] would just be "\0" if nothing in a spot of _/_/_
                    {
                        int index = StringToInt( str[j], StringLength( str[j] ) ) - 1; // OBJ file 1 based

                        if( j == 0 )
                        {
                            vertices = (float*) realloc( vertices, sizeof(float) * (num_vertex_floats + 3) );
                            vertices[num_vertex_floats++] = read_vertices[index][0];
                            vertices[num_vertex_floats++] = read_vertices[index][1];
                            vertices[num_vertex_floats++] = read_vertices[index][2];

                            // We are rearranging our vertices/texcoords/normals in order so now order of indices is trivial
                            indices = (unsigned int*) realloc( indices, sizeof(unsigned int) * (num_index_ints + 1) );
                            indices[num_index_ints] = num_index_ints; // Splitting up ++ due to undefined behavior (sequence point)
                            num_index_ints++;
                        }
                        else if( j == 1 )
                        {
                            tex_coords = (float*) realloc( tex_coords, sizeof(float) * (num_tex_floats + 2) );
                            tex_coords[num_tex_floats++] = read_tex_coords[index][0]; // u coord
                            // OpenGL starts texture reading top left, but stbi loads top down and I don't want to flip. So 1 - ...
                            tex_coords[num_tex_floats++] = 1 - read_tex_coords[index][1]; // v coord
                        }
                        else if( j == 2 )
                        {
                            normals = (float*) realloc( normals, sizeof(float) * (num_normal_floats + 3) );
                            normals[num_normal_floats++] = read_normals[index][0];
                            normals[num_normal_floats++] = read_normals[index][1];
                            normals[num_normal_floats++] = read_normals[index][2];
                        }
                        free( str[j] );
                    }
                }
            }
            free( str );
        }

        for( int i = 0; i < num_components; i++ )
        {
            free( line[i] );
        }
        free( line );
        line = 0;
    }
    
    for( int i = 0; i < num_read_vertices; i++ )
    {
        free( read_vertices[i] );
        read_vertices[i] = 0;
    }
    free( read_vertices );
    read_vertices = 0;

    for( int i = 0; i < num_read_tex_coords; i++ )
    {
        free( read_tex_coords[i] );
        read_tex_coords[i] = 0;
    }
    free( read_tex_coords );
    read_tex_coords = 0;

    for( int i = 0; i < num_read_normals; i++ )
    {
        free( read_normals[i] );
        read_normals[i] = 0;
    }
    free( read_normals );
    read_normals = 0;

    result = LoadToVAO( loader, vertices, num_vertex_floats,
                                indices, num_index_ints,
                                tex_coords, num_tex_floats );

    free( vertices ); vertices = 0;
    free( tex_coords ); tex_coords = 0;
    free( normals ); normals = 0;
    free( indices); indices = 0;
    
    fclose( fp );

    return result;
}




