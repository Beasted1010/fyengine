

// Handles window creation and events (minimal library, no rendering functions) -> And cross platform!
#include <GL/glew.h> // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h> // Located in glfw WIN64 library -> NOTE: the architecture (e.g. WIN64) is what your app is targeting
#include <GL/gl.h> // Located in MinGW include folder

#include "engine.h"

static void CreateShaders( struct ShaderProgram* shaderPrograms )
{
    LOG_CHECKPOINT("Creating Shaders");

    shaderPrograms[SHADER_CATEGORY_LAMP] = CreateShaderProgram( LAMP_SHADER_LOCATION );
    shaderPrograms[SHADER_CATEGORY_COLORS] = CreateShaderProgram( COLORS_SHADER_LOCATION );
    // TODO: CreateShaderProgram will populate uniforms that don't exist (i.e. the matrices!)
    shaderPrograms[SHADER_CATEGORY_TEXT] = CreateShaderProgram( TEXT_SHADER_LOCATION );

    LOG_CHECKPOINT("Shaders Created");
}

void DestroyShaders( struct ShaderProgram* shaderPrograms )
{
    for( int i = 0; i < NUMBER_OF_SHADER_CATEGORIES; i++ )
    {
        DestroyShaderProgram( &shaderPrograms[i] );
    }
}

// TODO: Make all these functions take in pointers to the objects (if they are) that are being set (e.g. loader = CreateLoader)
void CreateEngine( struct FYEngine* engine )
{
    LOG_CHECKPOINT("Initializing Engine");

    if( !glfwInit() )
    {
        LOG_CRITICAL("Failed to initialize GLFW!");
        // TODO: Handle error
    }

    // Create window and OpenGL context
    engine->windowState = CreateWindowState();
    engine->loader = CreateLoader();
    CreateShaders( engine->shaderPrograms );
    CreateHUDFont2D( FONT_TEXTURE_LOCATION, FONT_DATA_LOCATION, &engine->font );
    CreateCamera( &engine->camera, INITIAL_CAMERA_LOCATION, INITIAL_CAMERA_TARGET );

    LOG_CHECKPOINT("Engine Initialized");
}

void DestroyEngine( struct FYEngine* engine )
{
    LOG_CHECKPOINT("Destroying Engine");
    // TODO: Free camera stuff?

    DestroyCamera( &engine->camera );
    DestroyFont2D( &engine->font );
    DestroyShaders( engine->shaderPrograms );
    DestroyLoader( &engine->loader );
    DestroyWindowState( &engine->windowState );

    LOG_CHECKPOINT("Engine Destroyed");
}

