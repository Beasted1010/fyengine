
#include <GL/glew.h> // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GL/gl.h>

#include <stdlib.h>

#include "font.h"
#include "common.h"
#include "utilities.h"
#include "shaders.h"

//#define STB_IMAGE_IMPLEMENTATION // Must be defined in only a SINGLE file (has class definition of this header only file)
//                                    We want to compile this code once, and once only
#include <stb_image.h>

// TODO: Create own font data file creator
// TODO: Improvements: Generate a texture map that takes up less space -> Grayscale (1 byte per pixel) is all we need (ATM 3 byte)
//                     Y offset could be added as the bearingY, although I'm not sure if CBFG intends these to be synonymous
//                     Adjust the DEBUG statements (either delete or make more convenient to use, i.e. only show when I want)


static void LoadCBFGFontData2D( const char* fontDataLocation, struct Font2D* fontOut )
{
    LOG_CHECKPOINT("Loading CBFG Font Data from file %s", fontDataLocation);

    FILE* fp = fopen( fontDataLocation, "r" );
    ValidateObject( fp, "Font Data file pointer" );

    char** str = 0;

    fontOut->characters = 0;
    fontOut->numCharacters = 0;

    fontOut->fontName = 0;

    while( ReadLine( fp, ' ', &str ) )
    {
        if( !StringCompare( str[0], "Image" ) )
        {
            if( !StringCompare( str[1], "Width" ) )
            {
                // TODO: NEVER GOES HERE...
                fontOut->imageWidth = StringToInt( str[2], StringLength( str[2] ) );
            }
            else if( !StringCompare( str[1], "Height" ) )
            {
                fontOut->imageHeight = StringToInt( str[2], StringLength( str[2] ) );
            }
        }
        else if( !StringCompare( str[0], "Cell" ) )
        {
            if( !StringCompare( str[1], "Width" ) )
            {
                fontOut->cellWidth = StringToInt( str[2], StringLength( str[2] ) );
            }
            else if( !StringCompare( str[1], "Height" ) )
            {
                fontOut->cellHeight = StringToInt( str[2], StringLength( str[2] ) );
            }
        }
        else if( !StringCompare( str[0], "Start" ) )
        {
            if( !StringCompare( str[1], "Char" ) )
            {
                fontOut->startChar = StringToInt( str[2], StringLength( str[2] ) );
            }
        }
        else if( !StringCompare( str[0], "Font" ) )
        {
            if( !StringCompare( str[1], "Name" ) )
            {
                // TODO: Do I care about this? If so, then I WILL NEED TO HANDLE COPYING STRING (not just name=str[2]) !!!)
            }
            else if( !StringCompare( str[1], "Height" ) )
            {
                fontOut->fontHeight = StringToInt( str[2], StringLength( str[2] ) );
            }
            else if( !StringCompare( str[1], "Width" ) )
            {
                fontOut->fontWidth = StringToInt( str[2], StringLength( str[2] ) );
            }
        }
        else if( !StringCompare( str[0], "Char" ) )
        {
            unsigned int char_index = StringToInt( str[1], StringLength( str[1] ) );
            
            // I don't want to allocate memory for any character that we won't be using
            if( char_index < fontOut->startChar )
            {
                continue;
            }

            if( !StringCompare( str[2], "Base" ) )
            {
                if( !StringCompare( str[3], "Width" ) )
                {
                    // We encounter Base Width first, so allocate the memory here
                    fontOut->characters = (struct Character*) realloc( fontOut->characters, 
                                                                       sizeof(struct Character) * (fontOut->numCharacters + 1) );
                    fontOut->characters[fontOut->numCharacters++].width = StringToInt( str[4], StringLength( str[4] ) );
                }
            }
            else if( !StringCompare( str[2], "Width" ) )
            {
                if( !StringCompare( str[3], "Offset" ) )
                {
                    // NOTE: I think this is only for if the character doesn't start from the left edge of square, IDC atm
                }
            }
            // I don't need to do anything with bearing since this font creating tool seems to automatically position character
            //  in expected location
            else if( !StringCompare( str[2], "X" ) )
            {
                if( !StringCompare( str[3], "Offset" ) )
                {
                    // NOTE: I'm not entirely sure what this is for, but IDC at the moment. File has it as 0s
                    //       I'm assuming this is bearing X -> How far character is from origin (left corner of glyph rect)
                    fontOut->characters[char_index].bearingX = StringToInt( str[4], StringLength( str[4] ) );
                }
            }
            else if( !StringCompare( str[2], "Y" ) )
            {
                if( !StringCompare( str[3], "Offset" ) )
                {
                    // NOTE: I'm not entirely sure what this is for, but IDC at the moment. File has it as 0s
                    //       I'm assuming this is bearing Y -> Dist up it goes from origin (lower left cnr) NOT SURE though
                    fontOut->characters[char_index].bearingY = StringToInt( str[4], StringLength( str[4] ) );
                }
            }

        }
        else if( !StringCompare( str[0], "Global" ) )
        {
            // NOTE: These are all 0s in the font data file for CBFG
            if( !StringCompare( str[1], "Width" ) )
            {
                if( !StringCompare( str[2], "Offset" ) )
                {
                    fontOut->globalWidthOffset = StringToInt( str[3], StringLength( str[3] ) );
                }
            }
            else if( !StringCompare( str[1], "X" ) )
            {
                if( !StringCompare( str[2], "Offset" ) )
                {
                    fontOut->globalXOffset = StringToInt( str[3], StringLength( str[3] ) );
                }
            }
            else if( !StringCompare( str[1], "Y" ) )
            {
                if( !StringCompare( str[2], "Offset" ) )
                {
                    fontOut->globalYOffset = StringToInt( str[3], StringLength( str[3] ) );
                }
            }
        }
        // NOTE: Not sure how to use these last things yet, file currently has 400 for bold, 0 for italic, and 3 for antialias?
        else if( !StringCompare( str[0], "Bold" ) )
        {
            fontOut->bold = StringToInt( str[1], StringLength( str[1] ) );
        }
        else if( !StringCompare( str[0], "Italic" ) )
        {
            fontOut->italic = StringToInt( str[1], StringLength( str[1] ) );
        }
        else if( !StringCompare( str[0], "AntiAlias" ) )
        {
            fontOut->antiAlias = StringToInt( str[1], StringLength( str[1] ) );
        }
    }

    fclose( fp );

    LOG_INFO("Number of characters from '%s' font file: %i", fontDataLocation, fontOut->numCharacters);

    LOG_CHECKPOINT("CBFG Font Data Loaded");
}

static void ReleaseFontData2D( struct Font2D* font )
{
    free( font->characters );
    free( font->fontName );
}

void LoadFontGlyphTextures( const char* fontTextureLocation, struct Font2D* fontOut )
{
    LOG_CHECKPOINT("Loading Font Glyphs");

    // Load the data corresponding to the texture image
    int width, height, number_of_channels;
    // I am not flipping texture on load, so I am using texture coordinates that coorespond to a bottom down image
    unsigned char* data = stbi_load( fontTextureLocation, &width, &height, &number_of_channels, 0 );

    int number_of_bytes_in_glyph = ( (fontOut->cellWidth * fontOut->cellHeight) * number_of_channels );
    // A buffer to hold the data corresponding to which glyph's data we currently care about from the font texture
    unsigned char* glyph_data = (unsigned char*) malloc( sizeof(unsigned char) * number_of_bytes_in_glyph );

    unsigned int bytes_in_image_row = fontOut->imageWidth * number_of_channels;
    unsigned int bytes_in_cell_row = fontOut->cellWidth * number_of_channels;

    // Get texture IDs for all of our textures
    for( unsigned int i = 0; i < fontOut->numCharacters; i++ )
    {
        // Fill glyph_data with all the bytes that correspond to that glyph
        //                                                          ...which row I am on...               
        unsigned char* current_byte = data + ( ( (i / 16) * bytes_in_image_row ) * fontOut->cellHeight ) 
                                           + ( (i % 16) * bytes_in_cell_row ); //...which cell on that row
        for( unsigned int j = 0; j < fontOut->cellHeight; j++ ) // We have cellHeight rows of bytes_in_cell_row bytes
        {
            for( unsigned int k = 0; k < bytes_in_cell_row; k++ )
            {
                glyph_data[ (j * bytes_in_cell_row) + k ] = *(current_byte++);
            }
            current_byte += (bytes_in_image_row - bytes_in_cell_row);
        }

        unsigned int textureID;
        glGenTextures( 1, &textureID );
        glBindTexture( GL_TEXTURE_2D, textureID );

        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

        // The texture is the entire glyph's rectangle in atlas, NOTE: so this may be larger than desired
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, fontOut->cellWidth, fontOut->cellHeight, 
                      0, GL_RGB, GL_UNSIGNED_BYTE, glyph_data );

        AddTextureToLoader( &fontOut->loader, textureID );
        fontOut->characters[i].textureID = textureID;

    }
    free( glyph_data );


    glBindTexture( GL_TEXTURE_2D, 0 );
    LOG_CHECKPOINT("Font Glyphs Loaded");
}

// TODO: I really need to figure out this printing text to 3D world thing. For now I'm moving on.
void CreateWorldFont2D( const char* text, int x, int y, const char* fontTextureLocation, const char* fontDataLocation, struct Font2D* fontOut )
{
    LOG_CHECKPOINT("Creating World Font2D");

    LoadCBFGFontData2D( fontDataLocation, fontOut );
    fontOut->loader = CreateLoader();
    LoadFontGlyphTextures( fontTextureLocation, fontOut );

    // Define the total size of quad to render
    /*int quad_width = 0;
    for( unsigned int i = 0; text[i] != '\0'; i++ )
    {
        struct Character character = fontOut->characters[text[i] - fontOut->startChar];

        quad_width += character.width;
    }*/
    /*int quad_width = GetTextQuadWidth( text, scale ); // TODO: I need scale? Hmmm grr! 

    float vertices[12] = {
        // First triangle
        x + quad_width, y + fontOut->cellHeight,
        x, y + fontOut->cellHeight,
        x, y,

        // Second triangle
        x + quad_width, y + fontOut->cellHeight,
        x, y,
        x + quad_width, y,
    }

    float texture_coordinates[] ...*/

    glGenVertexArrays( 1, &fontOut->VAO );
    glBindVertexArray( fontOut->VAO );

    glGenBuffers( 1, &fontOut->VBO );
    glBindBuffer( GL_ARRAY_BUFFER, fontOut->VBO );


    /*glGenVertexArrays( 1, &fontOut->VAO );
    glBindVertexArray( fontOut->VAO );

    glGenBuffers( 1, &fontOut->VBO );
    glBindBuffer( GL_ARRAY_BUFFER, fontOut->VBO );
    fontOut->loader.vbos = (unsigned int*) realloc( fontOut->loader.vbos, sizeof(unsigned int) * (fontOut->loader.numVBOs + 1) );
    fontOut->loader.vbos[fontOut->loader.numVBOs++] = fontOut->VBO;
    glBufferData( GL_ARRAY_BUFFER, sizeof(float) * 6 * 3, 0, GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, 0 ); // TODO: Doing normalization for 3D Sudoku? Internally though?
    glBindBuffer( GL_ARRAY_BUFFER, 0 );

    const int number_of_texture_coordinates = 6 * 2;
    float texture_coordinates[number_of_texture_coordinates] = {
        0.0f, 0.0f,
        0.0f, 1.0f,
        1.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };

    unsigned int vboID;
    glGenBuffers( 1, &vboID );
    glBindBuffer( GL_ARRAY_BUFFER, vboID );
    fontOut->loader.vbos = (unsigned int*) realloc( fontOut->loader.vbos, sizeof(unsigned int) * (fontOut->loader.numVBOs + 1) );
    fontOut->loader.vbos[fontOut->loader.numVBOs++] = vboID;
    glBufferData( GL_ARRAY_BUFFER, sizeof(float) * number_of_texture_coordinates, texture_coordinates, GL_STATIC_DRAW );
    glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glBindBuffer( GL_ARRAY_BUFFER, 0 );

    glBindVertexArray( 0 );*/

    LOG_CHECKPOINT("World Font2D Created");
}

void CreateHUDFont2D( const char* fontTextureLocation, const char* fontDataLocation, struct Font2D* fontOut )
{
    LOG_CHECKPOINT("Creating Font2D Object");

    LoadCBFGFontData2D( fontDataLocation, fontOut );

    fontOut->loader = CreateLoader();

    LoadFontGlyphTextures( fontTextureLocation, fontOut );

    glGenVertexArrays( 1, &fontOut->VAO );
    glGenBuffers( 1, &fontOut->VBO );
    glBindVertexArray( fontOut->VAO );
    glBindBuffer( GL_ARRAY_BUFFER, fontOut->VBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof(float) * 6 * 4, 0, GL_DYNAMIC_DRAW );
    glEnableVertexAttribArray( 0 );
    glVertexAttribPointer( 0, 4, GL_FLOAT, GL_FALSE, 0, 0 );
    glBindBuffer( GL_ARRAY_BUFFER, 0 );
    glBindVertexArray( 0 );

    LOG_CHECKPOINT("Font2D Object Created");
}

void DestroyFont2D( struct Font2D* font )
{
    DestroyLoader( &font->loader );
    ReleaseFontData2D( font );
}


void CreateFont2DQuadVertices( int x, int y, float scale, struct Font2D* font, float* verticesOut )
{
    // TODO: Handle bearing of character
    float x_position = x ;//+ character.bearingX * scale;
    float y_position = y ;//- (character.height - character.bearingY) * scale; // TODO: Could move out side loop, probably

    // TODO: Appropriately handle character's width and height, character.height = 0 I think right now.
    /*float w = character.width * scale;
    float h = character.height * scale;*/
    float width = font->cellWidth * scale;
    float height = font->cellHeight * scale;

    int index = 0;
    // Lower left triangle
    // Positions
    verticesOut[index++] = x_position;
    verticesOut[index++] = y_position + height;
    // Texture cordinates
    verticesOut[index++] = 0.0f;
    verticesOut[index++] = 0.0f;

    verticesOut[index++] = x_position;
    verticesOut[index++] = y_position;
    verticesOut[index++] = 0.0f;
    verticesOut[index++] = 1.0f;

    verticesOut[index++] = x_position + width;
    verticesOut[index++] = y_position;
    verticesOut[index++] = 1.0f;
    verticesOut[index++] = 1.0f;

    // Upper right triangle
    verticesOut[index++] = x_position;
    verticesOut[index++] = y_position + height;
    verticesOut[index++] = 0.0f;
    verticesOut[index++] = 0.0f;

    verticesOut[index++] = x_position + width;
    verticesOut[index++] = y_position;
    verticesOut[index++] = 1.0f;
    verticesOut[index++] = 1.0f;

    verticesOut[index++] = x_position + width;
    verticesOut[index++] = y_position + height;
    verticesOut[index++] = 1.0f;
    verticesOut[index++] = 0.0f;

    if( index != 24 )
    {
        LOG_ERROR("Incorrect number of floats in vertex array of positions and texture coordinates!");
    }
}

void CreateFont2DWorldQuadVertices( int x, int y, float scale, struct Font2D* font, float* verticesOut )
{
    // TODO: Handle bearing of character
    float x_position = x ;//+ character.bearingX * scale;
    float y_position = y ;//- (character.height - character.bearingY) * scale; // TODO: Could move out side loop, probably

    // TODO: Appropriately handle character's width and height, character.height = 0 I think right now.
    /*float w = character.width * scale;
    float h = character.height * scale;*/
    float width = font->cellWidth * scale / 16;
    float height = font->cellHeight * scale / 16;

    int index = 0;
    // Lower left triangle
    // Positions
    verticesOut[index++] = x_position;
    verticesOut[index++] = y_position + height;
    verticesOut[index++] = 0;

    verticesOut[index++] = x_position;
    verticesOut[index++] = y_position;
    verticesOut[index++] = 0;

    verticesOut[index++] = x_position + width;
    verticesOut[index++] = y_position;
    verticesOut[index++] = 0;

    // Upper right triangle
    verticesOut[index++] = x_position;
    verticesOut[index++] = y_position + height;
    verticesOut[index++] = 0;

    verticesOut[index++] = x_position + width;
    verticesOut[index++] = y_position;
    verticesOut[index++] = 0;

    verticesOut[index++] = x_position + width;
    verticesOut[index++] = y_position + height;
    verticesOut[index++] = 0;

    /*if( index != 12 )
    {
        LOG_ERROR("Incorrect number of floats in vertex array of positions and texture coordinates!");
    }*/
}

void PrintHUDText2D( const char* text, int x, int y, float scale, struct RGB color,
                     struct Font2D* font, struct ShaderProgram* shaderProgram )
{
    glUseProgram( shaderProgram->id );
    LoadColor( &shaderProgram->id, "textColor", &color );
    glActiveTexture( GL_TEXTURE0 );
    glBindVertexArray( font->VAO );

    // TODO: I am disabling depth testing here, but this is no permanent solution-> It still requires I draw text last
    //       Find a better way. There is a more fundamental underlying problem, without disabling depth testing,
    //          the first letter is only rendered, but this seems ridiculous... All letters should be rendered the same
    glDisable( GL_DEPTH_TEST );
    for( unsigned int i = 0; text[i] != '\0'; i++ )
    {
        struct Character character = font->characters[text[i] - font->startChar];

        float vertices[24];
        CreateFont2DQuadVertices( x, y, scale, font, vertices );

        glBindTexture( GL_TEXTURE_2D, character.textureID );

        glBindBuffer( GL_ARRAY_BUFFER, font->VBO );
        glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glDrawArrays( GL_TRIANGLES, 0, 6 );

        // TODO: Need more accurate advance?
        //x += (character.advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64) -> From tutorial
        x += character.width * scale;
    }
    glEnable( GL_DEPTH_TEST );

    glBindVertexArray( 0 );
    glBindTexture( GL_TEXTURE_2D, 0 );

    StopProgram();
}

void PrintText2DInWorld( const char* text, float scale, struct RGB color, struct Font2D* font,
                         struct ShaderProgram* shaderProgram )
{
    glUseProgram( shaderProgram->id );
    // TODO: Add this to a new shader for world text? Currently colors shader does not have this uniform
    //glUniform3f( glGetUniformLocation( shaderProgram->id, "textColor" ), color.red, color.green, color.blue );
    glActiveTexture( GL_TEXTURE0 );
    glBindVertexArray( font->VAO );

    // Start on bottom left of quad
    int x = 0, y = 0;

    // TODO: I am disabling depth testing here, but this is no permanent solution-> It still requires I draw text last
    //       Find a better way. There is a more fundamental underlying problem, without disabling depth testing,
    //          the first letter is only rendered, but this seems ridiculous... All letters should be rendered the same
    glDisable( GL_DEPTH_TEST );
    for( unsigned int i = 0; text[i] != '\0'; i++ )
    {
        struct Character character = font->characters[text[i] - font->startChar];

        float vertices[18];
        CreateFont2DWorldQuadVertices( x, y, scale, font, vertices );

        glBindTexture( GL_TEXTURE_2D, character.textureID );

        glBindBuffer( GL_ARRAY_BUFFER, font->VBO );
        glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        glDrawArrays( GL_TRIANGLES, 0, 6 );

        // TODO: Need more accurate advance?
        //x += (character.advance >> 6) * scale; // Bitshift by 6 to get value in pixels (2^6 = 64) -> From tutorial
        x += character.width * scale;
    }
    glEnable( GL_DEPTH_TEST );

    glBindVertexArray( 0 );
    glBindTexture( GL_TEXTURE_2D, 0 );
}





