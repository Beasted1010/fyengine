
// 4 Big 3D Things: Shaders, Textures, Coordinate Systems and Transformations, Camera
//
// TODO:
//      - A normalization function to take screen coordinates (top right = 0,0) and convert to -1 to 1 range
//      - Remove OpenGL types (e.g. GLuint) with corresponding C type -> Essentially use what is typedef'd as
//      - Create own LookAt function (remove glm's LookAt function)
//      - Abstract Camera code as much as possible to own camera source/header files
//      - Abstract Shader code as much as possible to own shader source/header files
//
// NOTE:
//      - Performance Considerations
//          - glUseProgram is a moderate performance hit, I do this back to back frequently
//              - Source: https://stackoverflow.com/questions/23968364/webgl-is-the-gl-useprogram-an-expensive-call-to-make1
//          - I am glUniform loading ALL of my matrices for EVERY object EVERY frame... AND TRANSPOSING.... BAD
//              - This means I am shoveling data form CPU to GPU quite frequently... Which may be far away from eachother!
//              - Source: https://stackoverflow.com/questions/23725654/gluniform-vs-single-draw-call-performance
//              - I HAVE IMPLEMENTED A SOLUTION: I NOW ALLOW FOR NULL TO BE PASSED IN TO MATRICES SO THAT ONLY DESIRED 1s UPDATE
//          -
//
// NOTE:
//      - I require OpenGL to transpose my matrix for two reasons...
//          - So I can keep my matrices in row major order
//          - So I can use typical Linear Algebra notation of multiplying matrices (read left to right)
//          

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

// Handles window creation and events (minimal library, no rendering functions) -> And cross platform!
#include <GL/glew.h> // Needs to be included before gl.h (and consequently glfw3.h) (before any other OpenGL things)
#include <GLFW/glfw3.h> // Located in glfw WIN64 library -> NOTE: the architecture (e.g. WIN64) is what your app is targeting
#include <GL/gl.h> // Located in MinGW include folder

/*#include "shaders.h"
#include "window.h"
#include "loader.h"
#include "renderer.h"
#include "model.h"
#include "matrix.h"
#include "camera.h"
#include "utilities.h"
#include "font.h"*/
#include "overlays.h" // TODO: Move to engine and stuff
#include "events.h" // TODO: Add to engine and stuff
#include "engine.h"
#include "gamestate.h"



// TODO: Separate out into functions -> Should be like "SetupThisThing ... (other thigns) ... DestroyThisThing"
//       Problem -> Variables and such that we need access to later. Object orientation makes things easier since class encapsults

int main( int argc, char** argv )
{
    /*printf("HERE = %i\n", StringToInt( "123", 3 ) );
    printf("HERE = %f\n", StringToFloat( "123.12129", 6 ) );
    printf("HERE = %f\n", StringToFloat( "12341.1212", 10 ) );
    printf("HERE = %f\n", StringToFloat( "1241.991212", 11 ) );
    printf("HERE = %f\n", StringToFloat( "121.9991212", 11 ) );
    printf("HERE = %f\n", StringToFloat( "-121.999", 8 ) );*/
    //exit(0);

    /*if( !glfwInit() )
    {
        printf("Failed to initialize GLFW!\n");
        return -1;
    }

    // Create the window and the OpenGL context
    struct WindowState windowState = CreateWindowState();

    printf("GL Version: %s\n", glGetString(GL_VERSION));

    struct ShaderProgram lighting_shader_program = CreateShaderProgram( "rsc/shaders/colors.shader" );
    struct ShaderProgram lamp_shader_program = CreateShaderProgram( "rsc/shaders/lamp.shader" );
    // TODO: CreateShaderProgram will populate uniforms that don't exist (i.e. the matrices!)
    struct ShaderProgram text_shader_program = CreateShaderProgram( "rsc/shaders/text.shader" );*/
    struct FYEngine engine;
    CreateEngine( &engine );

    // TODO: Tidy this up, maybe have a "PrepareOpenGLRendering" or "EnableOpenGLOptions" function to do so.
    //       This is an optimization to not render anything not facing the camera (can be seen by moving into an object)
    // Enables for the font, we wnat to enable blending to ignore certain color values of our font
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); // Specify how OGL should blend alpha pixels
    glEnable( GL_BLEND ); // We want to blend, so enable it

    glEnable( GL_DEPTH_TEST ); // Ensure that near vertices overlap far vertices and not vice-versa

    glEnable( GL_CULL_FACE ); 
    glEnable( GL_BACK );

    // uncomment this to draw in wireframe polygons -> Good for debugging
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    struct GameState state; InitializeGameState( &state );


    
    /*UseProgram( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id );
    // TODO: Find better spot? Is this even needed? I think so, especially if I want multiple textures. See tex tutorial
    //       Here we are assigning a location to the texture sampler, this is important if we want to set multiple textures at
    //          once in our fragment shader. This location is called a "texture unit" -> See more in comments of tex tut file
    glUniform1i( glGetUniformLocation(engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, "textureSampler1"), 0);
    UseProgram( 0 );*/


    // Create objects
    //struct RawModel objModel = LoadOBJModel( &loader,"rsc/untitled.obj" );
    //struct RawModel objModel = LoadOBJModel( &loader,"rsc/cube.obj" );
    //struct RawModel objModel = LoadOBJModel( &loader,"rsc/square.obj" );
    struct RawModel objModel = LoadOBJModel( &engine.loader,"rsc/models/sceneTest.obj" );
    //struct Object shape = CreateObjectFromRawModel( &loader, &objModel, "rsc/container.jpg", {-3.0f, 0, -1.0f} );
    struct Object shape = CreateObjectFromRawModel( &engine.loader, &objModel, "rsc/textures/wood.png", {0,0,0} );

    struct RawModel boxModel = LoadOBJModel( &engine.loader,"rsc/models/cube.obj" );
    struct Object box = CreateObjectFromRawModel( &engine.loader, &boxModel, "rsc/textures/stallTexture.jpg", {5.0f,0,0} );

    struct RawModel lampModel = LoadOBJModel( &engine.loader,"rsc/models/cube.obj" );
    struct Object lamp = CreateObjectFromRawModel( &engine.loader, &lampModel, "rsc/textures/stallTexture.jpg", {3.0f,0,-1.0f} );

    AddObject( box, &state.gameObjects, &state.numGameObjects );
    AddObject( shape, &state.gameObjects, &state.numGameObjects );
    AddObject( lamp, &state.lightObjects, &state.numLightObjects );

    // Maybe the engine should define these? Or else these variables will be hanging around memory the whole execution time
    struct Matrix4f light_model_to_world = CreateTransformationMatrix4f( &lamp.position, 0, 0, 0, 1.0f );

    struct Matrix4f world_to_camera = CreateViewMatrix4f( &engine.camera.position, &engine.camera.target, &engine.camera.up );
    struct Matrix4f camera_to_clip = CreatePerspectiveMatrix4f( 45.0f, SCREEN_WIDTH / (float) SCREEN_HEIGHT, 0.1f, 100.0f );
    //struct Matrix4f camera_to_clip = CreateOrthographicMatrix4f( -10.0f, 10.0f, -10.0f, 10.0f, 0.0f, 100.0f );
    
    LoadScreenProjectionMatrix( engine.shaderPrograms[SHADER_CATEGORY_TEXT].id, (float) SCREEN_WIDTH, (float) SCREEN_HEIGHT );

    LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], &light_model_to_world, &world_to_camera, &camera_to_clip );
    LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], 0, &world_to_camera, &camera_to_clip );

    /*struct Font2D world_font;
    CreateHUDFont2D( FONT_TEXTURE_LOCATION, FONT_DATA_LOCATION, &world_font ); // TODO: Move to engine

    struct ShaderProgram text_world_shader = CreateShaderProgram("rsc/shaders/text_world.shader");
    LoadMatrices( &text_world_shader, 0, &world_to_camera, &camera_to_clip );*/

    struct ShaderProgram overlay_shader_program = CreateShaderProgram( "rsc/shaders/overlay.shader" );
    LoadScreenProjectionMatrix( overlay_shader_program.id, (float) SCREEN_WIDTH, (float) SCREEN_HEIGHT );

    struct Overlay overlay = CreateOverlay( 400, 50, 400, 400, "menu", (struct RGB) {0, 0, 255} );

    struct ShaderProgram widget_shader_program = CreateShaderProgram( "rsc/shaders/widget.shader" );
    LoadScreenProjectionMatrix( widget_shader_program.id, (float) SCREEN_WIDTH, (float) SCREEN_HEIGHT );

    CreateAndAddPushButtonToOverlay( 20, 20, 50, 100, (struct RGB) {0, 100, 20}, "Button", &overlay );

    CreateAndAddTextboxToOverlay( 100, 100, 50, 40, "Hey", (struct RGB) {0, 0, 0}, "Header", &overlay );

    CreateAndAddPushButtonToOverlay( 50, 50, 50, 50, (struct RGB) {0, 100, 20}, "Button2", &overlay );

    CreateAndAddCheckboxToOverlay( 150, 50, 20, 50, (struct RGB) {100, 100, 20}, "Textbox2", &overlay );

    //struct CallbackFunctions callbacks;
    //RegisterOverlayCallback( etMOUSE_MOVE_EVENT, OnMouseMove, &callbacks );
    // TODO: find a better way to pass this in. I don't want to have to create a struct and then pass in
    //          It there a way to just create a structure during call and cast that to void*?
    //struct MouseMoveData mouse_move_data = {5, 5};
    //HandleOverlayEvent( fMOUSE_MOVE, (void*) &mouse_move_data, &callbacks );

    while( engine.windowState.running )
    {
        ProcessInput( &engine.windowState, &overlay, 1, &engine.camera );

        RenderPrepare(); // Prepare screen for rendering
        
        engine.camera.target = AddVector3f( &engine.camera.position, &engine.camera.front );
        world_to_camera = CreateViewMatrix4f( &engine.camera.position, &engine.camera.target, &engine.camera.up );

        LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_LAMP], 0, &world_to_camera, 0 );
        LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], 0, &world_to_camera, 0 );

        // Handle Objects
        for( int i = 0; i < state.numGameObjects; i++ )
        {
            struct Matrix4f object_model_to_world = CreateTransformationMatrix4f( &state.gameObjects[i].position, 0, 0, 0, 1.0f );
            LoadMatrices( &engine.shaderPrograms[SHADER_CATEGORY_COLORS], &object_model_to_world, 0, 0 );
            Render( engine.shaderPrograms[SHADER_CATEGORY_COLORS].id, &state.gameObjects[i].texturedModel ); // Draw the model
        }

        // Handle Light Sources
        for( int i = 0; i < state.numLightObjects; i++ )
        {
            //Render( engine.shaderPrograms[SHADER_CATEGORY_LAMP].id, &state.lightObjects[i].texturedModel );
        }

        /*struct Vector3f text_position = { 0.0f, 0.0f, 0.0f };
        struct Matrix4f font_to_world = CreateTransformationMatrix4f( &text_position, 0, 0, 0, 1.0f );
        LoadMatrices( &text_world_shader, &font_to_world, 0, 0 );

        //PrintText2DInWorld( "3D YET MAN", 1.0f, (struct RGB) {255, 0, 0}, &world_font, &engine.shaderPrograms[SHADER_CATEGORY_COLORS] );
        PrintText2DInWorld( "3", 1.0f, (struct RGB) {255, 0, 0}, &world_font, &text_world_shader );*/

        DrawOverlay( &overlay, &overlay_shader_program, &widget_shader_program, &engine.shaderPrograms[SHADER_CATEGORY_TEXT] );

        PrintHUDText2D( "My Name is Bob", 250, 250, 1.0f, (struct RGB) {255, 255, 255},
                     &engine.font, &engine.shaderPrograms[SHADER_CATEGORY_TEXT] );
        PrintHUDText2D( "do you even know grow of agriculture!?", 400, 400, 2.0f, (struct RGB) {0, 0, 255},
                     &engine.font, &engine.shaderPrograms[SHADER_CATEGORY_TEXT] );


        UpdateWindowState( &engine.windowState );
    }
    //DestroyFont2D( &world_font );
    DestroyOverlay( &overlay );

    FreeGameState( &state );
    DestroyEngine( &engine );
    return 0;
}



