
INC=inc
SRC=src
OBJ=obj
LIB=lib

MK_LIB = ar rcs
LIB_NAME = fyengine.a

main = main.c

OUT=run_sandbox

CC=g++
#CFLAGS=-Wall -DGLEW_STATIC -Iinc -Idependencies/GLEW/include -I"C:\glfw-3.2.1.bin.WIN64\glfw-3.2.1.bin.WIN64\include"
#LINKFLAGS= -L C:\glfw-3.2.1.bin.WIN64\glfw-3.2.1.bin.WIN64\lib-mingw-w64 -L C:\Users\die20\Documents\Code\OpenGLPlay\dependencies\GLEW\lib\Release\x64
#LIBRARIES= -lglew32s -lopengl32 -lglfw3 -lgdi32

#CFLAGS=-Wall -DGLEW_STATIC -Iinc -I"C:\MinGW\include" -I"C:\MinGW\include\GLFW"
#LINKFLAGS= -L C:\MinGW\lib -L C:\Users\die20\Downloads\glew-2.1.0-win32.zip\glew-2.1.0\lib\Release\x64
#LIBRARIES= -lglew32s -lglfw3 -lopengl32 -lgdi32

CFLAGS=-Wall -DGLEW_STATIC -Iinc -Idependencies\GLEW\include -Idependencies\GLFW\include
LINKFLAGS= -L dependencies\GLEW\lib\Release\Win32 -L C:\MinGW\lib
LIBRARIES= -lglew32s -lglfw3 -lopengl32 -lgdi32


_DEPS = shaders.h renderer.h camera.h window.h loader.h model.h matrix.h lighting.h gamestate.h utilities.h common.h logger.h \
		font.h engine.h overlays.h events.h
DEPS = $(patsubst %, $(INC)/%, $(_DEPS))
SOURCES = $(SRC)/$(main) $(subst .h,.c, $(patsubst %, $(SRC)/%, $(_DEPS)))
OBJECTS = $(OBJ)/$(subst .c,.o,$(main)) $(subst .h,.o, $(patsubst %, $(OBJ)/%, $(_DEPS)))

$(OBJ)/%.o: $(SRC)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(OUT): $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(LINKFLAGS) $(LIBRARIES)


library: $(SOURCES) $(DEPS)
	$(MK_LIB) $(LIB)/lib$(LIB_NAME) $(OBJECTS)


help:
	echo sources = $(SOURCES)
	echo objects = $(OBJECTS)
	echo command = $(CC) $(CFLAGS) $(LINKFLAGS) $(SOURCES) $(LIBRARIES)

clean:
	del /q $(OBJ)\*


