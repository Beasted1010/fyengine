
@echo off

cd obj
echo COMPILING...
g++ -c -DGLEW_STATIC^
       -I../inc^
       -I../dependencies/GLEW/include^
       -I "C:\glfw-3.2.1.bin.WIN64\glfw-3.2.1.bin.WIN64\include"^
        ../src/old_tutorials/tut_lighting_6.c 

cd ..
echo LINKING...
g++ -L C:\glfw-3.2.1.bin.WIN64\glfw-3.2.1.bin.WIN64\lib-mingw-w64^
    -L C:\Users\die20\Documents\Code\OpenGLPlay\dependencies\GLEW\lib\Release\x64^
        obj/tut_lighting_6.o -lglew32s -lopengl32 -lglfw3 -lgdi32 

